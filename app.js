'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

let debug = require('debug')('server:server');

let http = require('http');
var https = require('https');


// var privateKey  = fs.readFileSync('sslcert/server.key', 'utf8');
// var certificate = fs.readFileSync('sslcert/server.crt', 'utf8');

let appzet = require('./src/appzet');

let logger;

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  let port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  let bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {


  let addr = AppZet.server.address();
  let bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}


/**
 * Get port from environment and store in Express.
 */

let port = normalizePort(process.env.PORT || '3000');

logger = AppZet.logger('App', true);

let createdServer;

appzet.loadAppZet()
  .then(function () {
    return appzet.setupHook();
  })
  .then(function () {
    return appzet.connectToDb();
  })
  .then(function () {
    return appzet.loadModels();
  })
  .then(function () {
    return appzet.loadRouters();
  })
  .then(function (app) {

    createdServer = app;

    // Health checker
    createdServer.get('/health', (req, res) => {
      res.status(200).send();
    });

    /**
     * Resolve hooks.
     */

    return new Promise(function (resolve, reject) {

      if (AppZet.hookRunning.length == 0)
        return resolve(app);

      AppZet.event.on("hookDone", function (identity, error) {
        if (error) {
          logger.log('error', error.stack);
          return process.exit();
        }

        logger.debug('%s-Hook Done', identity);

        if (AppZet.hookRunning.length == 0)
          return resolve(app);
      });
    });
  })
  .then(function (app) {
    logger.debug('Start server listening to port: ', port);

    app.set('port', port);

    /**
     * Create HTTP server.
     */


    AppZet.server = http.createServer(app);

    /**
     * Listen on provided port, on all network interfaces.
     */

    AppZet.server.listen(port);
    AppZet.server.on('error', onError);
    AppZet.server.on('listening', onListening);
    AppZet.server.timeout = AppZet.config.responses.requestTimeOut;

    AppZet.socket = io(AppZet.server, AppZet.config.socket.options);


    return app;
  })
  .then(function (app) {
    return appzet.setupSocket();
  })
  .then(function () {

    AppZet.config.bootstrap(function callback(error) {
      if (error) {
        logger.log('error', err.stack);
        return process.exit();
      }

      logger.debug('Bootstrap done');
    });
  })
  .catch(function (err) {
    logger.log('error', err.stack);
    process.exit();
  });



