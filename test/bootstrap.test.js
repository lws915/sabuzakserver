'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */


var http = require('http');
var appzet = require('../src/appzet');
var debug = require('debug')('server:server');
var testDbUrl = 'mongodb://localhost/test';
var port = normalizePort(process.env.PORT || '3000');

var logger = AppZet.logger('TestBoostrap');

module.exports = {
  testBefore: testBefore,
  testAfter: testAfter,
};


function testBefore() {
  return appzet.loadAppZet()
  // Setup hook
  // .then(function () {
  //   return appzet.setupHook();
  // })

  // Connect to db
    .then(function () {
      return appzet.connectToDb(testDbUrl)
    })

    // Drop db before starting test
    .then(function () {
      return new Promise(function (resolve, reject) {
        Mongoose.connection.db.dropDatabase(function (err, result) {
          if (err)
            reject(err);
          else
            resolve(result);
        });
      });
    })

    .then(function () {
      return appzet.loadModels();
    })
    .then(function () {
      return appzet.loadRouters();
    })
    .then(function (app) {
      return new Promise(function (resolve, reject) {

        function callback(err) {
          if (err) {
            logger.log('error', err);
            return process.exit();
          }

          logger.log('debug', 'Bootstrap done');
          resolve(app);
        }

        AppZet.config.bootstrap(callback);

      });
    })
    .then(function (app) {

      AppZet.hooks.Passport.initialise(() => {
      });

      AppZet.server = http.createServer(app);

      AppZet.server.listen(port);
      AppZet.server.on('error', onError);
      AppZet.server.on('listening', onListening);
      AppZet.server.timeout = AppZet.config.responses.requestTimeOut;

      return console.log("Start testing");
    })
    .catch(function (err) {
      console.error(err);
      process.exit();
    });
}


function testAfter() {
  return new Promise((resolve, reject) => {
    AppZet.server.close();
    resolve();
  });
}


/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {


  var addr = AppZet.server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}