'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

var assert = require('assert');
var DBError = require('../../../src/api/errors/DBError');


describe('DBError Error Test', function () {

  it('Generic error feature test', function (done) {
    function doSomethingBad() {
      throw new DBError();
    }

    try {
      doSomethingBad();
    } catch (err) {
      assert(err.name == 'DBError');

      assert(err.message == 'Internal Server Error');

      assert(err instanceof DBError);

      assert(err instanceof Error);

      assert(require('util').isError(err));

      assert(err.stack);

      assert.strictEqual(err.toString(),
        'DBError: Internal Server Error');

      assert.strictEqual(err.stack.split('\n')[0],
        'DBError: Internal Server Error');

      assert.strictEqual(err.stack.split('\n')[1].indexOf('doSomethingBad'), 7);
    }

    done();
  });


  it('Error derived creating test', function (done) {
    function doErrorSource() {
      throw new Error("source error");
    }


    try {
      doErrorSource();
    } catch (err) {

      var dbError = new DBError({error: err});

      assert(dbError.name == 'DBError');

      assert(dbError.message == 'source error');

      assert(dbError instanceof DBError);

      assert(dbError instanceof Error);

      assert(require('util').isError(dbError));

      assert(dbError.stack);

      assert.strictEqual(dbError.toString(),
        'DBError: source error');

      assert.strictEqual(dbError.stack.split('\n')[0],
        'DBError: Error: source error');

      assert.strictEqual(dbError.stack.split('\n')[1].indexOf('doErrorSource'), 7);
    }

    done();
  });

});
