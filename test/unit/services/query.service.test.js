'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

var assert = require('assert');

describe('Query Service', function () {

  it('should exist', function () {
    assert.ok(AppZet.services.QueryService);
    assert.ok(global.QueryService);
  });

  describe('#buildQuery()', function () {
    it('should return array when string array is passed', function (done) {

      var query = {
        populate: '["123", "456"]'
      };

      var params = AppZet.services.QueryService.buildQuery(query);

      assert.equal(Array.isArray(params.populate), true);

      done();
    });

  });

});
