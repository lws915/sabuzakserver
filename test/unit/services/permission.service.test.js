'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

var assert = require('assert');


describe('Permission Service', function () {

  before(function (done) {

    // Create test data before hand
    var config = {
      drop: true,
      roles: [
        {
          name: "관리자",
          identity: "ADMIN"
        },
        {
          name: "사용자",
          identity: "USER"
        }
      ],
      permissions: {
        ADMIN: [
          // User
          {"identity": 'ADMIN_GET_USER', "model": 'User', "action": 'read', "relation": 'role'},
          {"identity": 'ADMIN_POST_USER', "model": 'User', "action": 'create', "relation": 'role'},
          {"identity": 'ADMIN_PUT_USER', "model": 'User', "action": 'update', "relation": 'role'},
          {"identity": 'ADMIN_DELETE_USER', "model": 'User', "action": 'delete', "relation": 'role'},
        ],
        USER: [
          // User
          {"identity": 'USER_GET_USER', "model": 'User', "action": 'read', "relation": 'role'},
          {"identity": 'USER_PUT_USER', "model": 'User', "action": 'update', "relation": 'owner'},
        ]
      },

      users: {
        ADMIN: [
          {
            email: 'developer@applicat.co.kr',
            username: 'admin',
            nickname: '관리자',
            password: 'admin1234'
          }
        ],
        USER: [
          {
            email: 'user@applicat.co.kr',
            username: 'user',
            nickname: '사용자',
            password: 'user1234'
          }
        ],
      }
    };

    AppZet.hooks.Security.initialise(config, function (error) {
      console.error(error);
      done();
    });

  });

  after(function (done) {
    Promise.all([
      Model.remove(),
      Role.remove(),
      Permission.remove(),
      User.remove(),
      Passport.remove(),
    ])
      .spread(function () {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  it('should exist', function (done) {
    assert.ok(AppZet.services.PermissionService);
    assert.ok(PermissionService);
    done();
  });

  describe('findModelPermissions', function () {
    it('admin permission get should exist', function (done) {
      Promise.all([
        User.findOne({email: 'developer@applicat.co.kr'}),
        Model.findOne({identity: 'User'})])
        .spread(function (user, model) {
          var options = {
            model: model,
            method: 'GET',
            user: user,
          };

          return PermissionService.findModelPermissions(options)
        })
        .then(function (permissions) {
          assert.equal(permissions.length, 1);
          assert.equal(permissions[0].relation, 'role');
          done();
        })
        .catch(function (error) {
          console.error(error);
          assert.ok(false);
          done();
        });
    });

    it('admin permission post should exist', function (done) {
      Promise.all([
        User.findOne({email: 'developer@applicat.co.kr'}),
        Model.findOne({identity: 'User'})])
        .spread(function (user, model) {
          var options = {
            model: model,
            method: 'POST',
            user: user,
          };

          return PermissionService.findModelPermissions(options)
        })
        .then(function (permissions) {
          assert.equal(permissions.length, 1);
          assert.equal(permissions[0].relation, 'role');
          done();
        })
        .catch(function (error) {
          console.error(error);
          assert.ok(false);
          done();
        });
    });

    it('admin permission put should exist', function (done) {
      Promise.all([
        User.findOne({email: 'developer@applicat.co.kr'}),
        Model.findOne({identity: 'User'})])
        .spread(function (user, model) {
          var options = {
            model: model,
            method: 'PUT',
            user: user,
          };

          return PermissionService.findModelPermissions(options)
        })
        .then(function (permissions) {
          assert.equal(permissions.length, 1);
          assert.equal(permissions[0].relation, 'role');
          done();
        })
        .catch(function (error) {
          console.error(error);
          assert.ok(false);
          done();
        });
    });

    it('admin permission delete should exist', function (done) {
      Promise.all([
        User.findOne({email: 'developer@applicat.co.kr'}),
        Model.findOne({identity: 'User'})])
        .spread(function (user, model) {
          var options = {
            model: model,
            method: 'DELETE',
            user: user,
          };

          return PermissionService.findModelPermissions(options)
        })
        .then(function (permissions) {
          assert.equal(permissions.length, 1);
          assert.equal(permissions[0].relation, 'role');
          done();
        })
        .catch(function (error) {
          console.error(error);
          assert.ok(false);
          done();
        });
    });

    it('user permission get should exist', function (done) {
      Promise.all([
        User.findOne({email: 'user@applicat.co.kr'}),
        Model.findOne({identity: 'User'})])
        .spread(function (user, model) {
          var options = {
            model: model,
            method: 'GET',
            user: user,
          };

          return PermissionService.findModelPermissions(options)
        })
        .then(function (permissions) {
          assert.equal(permissions.length, 1);
          assert.equal(permissions[0].relation, 'role');
          done();
        })
        .catch(function (error) {
          console.error(error);
          assert.ok(false);
          done();
        });
    });

    it('user permission put should exist', function (done) {
      Promise.all([
        User.findOne({email: 'user@applicat.co.kr'}),
        Model.findOne({identity: 'User'})])
        .spread(function (user, model) {
          var options = {
            model: model,
            method: 'PUT',
            user: user,
          };

          return PermissionService.findModelPermissions(options)
        })
        .then(function (permissions) {
          assert.equal(permissions.length, 1);
          assert.equal(permissions[0].relation, 'owner');
          done();
        })
        .catch(function (error) {
          console.error(error);
          assert.ok(false);
          done();
        });
    });

    it('user permission post should not exist', function (done) {
      Promise.all([
        User.findOne({email: 'user@applicat.co.kr'}),
        Model.findOne({identity: 'User'})])
        .spread(function (user, model) {
          var options = {
            model: model,
            method: 'POST',
            user: user,
          };

          return PermissionService.findModelPermissions(options)
        })
        .then(function (permissions) {
          assert.equal(permissions.length, 0);
          done();
        })
        .catch(function (error) {
          console.error(error);
          assert.ok(false);
          done();
        });
    });

    it('user permission delete should not exist', function (done) {
      Promise.all([
        User.findOne({email: 'user@applicat.co.kr'}),
        Model.findOne({identity: 'User'})])
        .spread(function (user, model) {
          var options = {
            model: model,
            method: 'DELETE',
            user: user,
          };

          return PermissionService.findModelPermissions(options)
        })
        .then(function (permissions) {
          assert.equal(permissions.length, 0);
          done();
        })
        .catch(function (error) {
          console.error(error);
          assert.ok(false);
          done();
        });
    });

  });
});
