'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

var assert = require('assert');
var nodemailer = require("nodemailer");
var sgTransport = require('nodemailer-sendgrid-transport');

describe('Mail Service', function () {


  before(function (done) {
    // Create test data bef

    ///Users/andy/Workspaces/Projects/appzet/development/server/test/unit/services
    // console.log("__dirname: " + __dirname);
    MailService.init({
      templatePath: __dirname + "/../../../src/config/mail_template/",
      expiry: 60,
      transport: nodemailer.createTransport(sgTransport({
        auth: {
          api_key: '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'
        }
      }))
    });

    done();
  });


  it('should exist', function () {
    assert.ok(AppZet.services.MailService);
    assert.ok(global.MailService);
    assert.ok(MailService);
  });

  describe('#compileMail()', function () {
    it('should find 1 admin mail templates kr', function (done) {

      MailService.compileMail('admin', 'kr', {
        title: "관리자 상담 이메일",
        email: "email@email.co.kr",
        phone: "01049944159",
        content: "상담 내용",
      })
        .then(
          (email) => {
            assert.notEqual(email, null);
            assert.notEqual(email.subject, null);
            assert.notEqual(email.text, null);
            assert.notEqual(email.html, null);
            done();
          },
          (error) => {
            assert.equal(true, false);
            console.error(error);
            done();
          }
        );
    });

    it('should find 1 admin mail templates en', function (done) {

      MailService.compileMail('admin', 'en', {
        title: "관리자 상담 이메일",
        email: "email@email.co.kr",
        phone: "01049944159",
        content: "상담 내용",
      })
        .then(
          (email) => {
            assert.notEqual(email, null);
            assert.notEqual(email.subject, null);
            assert.notEqual(email.text, null);
            assert.notEqual(email.html, null);
            done();
          },
          (error) => {
            assert.equal(true, false);
            console.error(error);
            done();
          }
        );
    });

    it('should find 1 admin mail templates default', function (done) {

      MailService.compileMail('admin', '', {
        title: "관리자 상담 이메일",
        email: "email@email.co.kr",
        phone: "01049944159",
        content: "상담 내용",
      })
        .then(
          (email) => {
            assert.notEqual(email, null);
            assert.notEqual(email.subject, null);
            assert.notEqual(email.text, null);
            assert.notEqual(email.html, null);
            done();
          },
          (error) => {
            assert.equal(true, false);
            console.error(error);
            done();
          }
        );
    });

    it('should find 1 admin mail templates default', function (done) {

      MailService.compileMail('admin', 'asdf', {
        title: "관리자 상담 이메일",
        email: "email@email.co.kr",
        phone: "01049944159",
        content: "상담 내용",
      })
        .then(
          (email) => {
            assert.notEqual(email, null);
            assert.notEqual(email.subject, null);
            assert.notEqual(email.text, null);
            assert.notEqual(email.html, null);
            done();
          },
          (error) => {
            assert.equal(true, false);
            console.error(error);
            done();
          }
        );
    });


    it('should failed to find mail templates', function (done) {

      MailService.compileMail('asdfss', 'asdf', {
        title: "관리자 상담 이메일",
        email: "email@email.co.kr",
        phone: "01049944159",
        content: "상담 내용",
      })
        .then(
          (email) => {
            assert.equal(true, false);
            done();
          },
          (error) => {
            assert.ok(
              error instanceof AppZet.error.MailTemplateNotFoundError,
              true);
            done();
          }
        );
    });
  });

  //// Warning: enable when you want to test email
  // describe('#sendMail()', function () {
  //   it('should send mail to developer email', function (done) {
  //
  //     MailService.sendEmail(
  //       'admin',
  //       'kr',
  //       {
  //         title: "관리자 상담 이메일",
  //         email: "email@email.co.kr",
  //         phone: "01049944159",
  //         content: "상담 내용",
  //       },
  //       'admin@applicat.co.kr',
  //       'developer@applicat.co.kr')
  //       .then((email) => {
  //         done();
  //       })
  //       .catch((error) => {
  //         assert.equal(true, false);
  //         done();
  //       });
  //
  //   });
  // });

});
