let s3Service = require('../../../src/api/services/S3Service')

// s3Service.deleteObject({
//   Bucket: 'appzet-apps',
//   Key: 'userId/appId/index.html'
// })
//   .then(data=> {
//     console.log(data);
//   })
//   .catch(error => {
//     console.error(error);
//   });
//
// s3.listObjects(params, function(err, data) {
//   if (err) return callback(err);
//
//   if (data.Contents.length == 0) callback();
//
//   params = {Bucket: bucketName};
//   params.Delete = {Objects:[]};
//
//   data.Contents.forEach(function(content) {
//     params.Delete.Objects.push({Key: content.Key});
//   });
//
//   s3.deleteObjects(params, function(err, data) {
//     if (err) return callback(err);
//     if(data.Contents.length == 1000)emptyBucket(bucketName,callback);
//     else callback();
//   });
// });

var params = {
  Bucket: 'templates.appzet.com',
  Key: 'published/0/index.html',
  ACL: 'public-read',
  CopySource: 'templates.appzet.com/draft/0/index.html'
}

s3Service.copyObject(params)
  .then(data=> {
    console.log("done");
  })
  .catch(error => {
    console.error(error);
  });