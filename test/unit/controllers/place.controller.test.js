'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

var assert = require('assert');
var request = require('supertest')('http://localhost:3000');
var bootstrapper = require('../../bootstrap.test.js');

describe('Place Controller', function () {

  before(function (done) {
    // Create test data before hand


    var photos = [
      {
        tags: ["MAIN", "PROFILE"]
      },
      {
        tags: ["PROFILE"]
      },
      {
        tags: ["MAIN"]
      },
      {
        tags: ["GALLERY"]
      },
      {
        tags: ["MENU"]
      },
    ];

    bootstrapper.testBefore()
      .then(() => {
        return Photo.create(photos);
      })
      .then(function (photos) {

        var places = [
          {
            category: 'STATION',
            name: '강남역',
            address: '강남역 주소',
            geoJSON: [127.027642, 37.497959],
            photos: [photos[0], photos[1]]
          },
          {
            category: 'STATION',
            name: '역삼역',
            address: '역삼역 주소',
            geoJSON: [127.037393, 37.499907],
            photos: [photos[2], photos[3]]
          },
          {
            category: 'STATION',
            name: '교대역',
            address: '교대역 주소',
            geoJSON: [127.01464299999998, 37.49396600000001],
            photos: [photos[4]]
          },
          {
            category: 'STATION',
            name: '선릉역',
            address: '선릉역 주소',
            geoJSON: [127.048941, 37.504479]
          },
          {
            category: 'RANDOM',
            name: 'RANDOM1',
            address: '광화문',
            geoJSON: [126.976923, 37.575988]
          },
          {
            category: 'RANDOM',
            name: 'RANDOM2',
            address: '응암',
            geoJSON: [126.920521, 37.592665]
          }
        ];

        return Place.create(places);
      })
      .then(function (places) {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  after(function (done) {
    Promise.all([
        Photo.remove(),
        Place.remove(),
      ])
      .spread(function () {
        return bootstrapper.testAfter();
      })
      .then(function () {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  it('should exist', function (done) {
    assert.ok(AppZet.controllers.PlaceController);
    assert.ok(global.Place);
    done();
  });

  describe('GET /place/count', function () {

    it('should count all 6 places', function (done) {
      request
        .get('/place/count')
        .set('Accept', 'application/json')
        .expect(200, {count: 6}, done);
    });

    it('should count only 4 {"query": {"category":"STATION"}}', function (done) {
      request
        .get('/place/count')
        .set('Accept', 'application/json')
        .query({query: '{"category": "STATION" }'})
        .expect(200, {count: 4}, done);
    });

  });

  describe('GET /place/find', function () {

    it('should find all 6 places', function (done) {
      request
        .get('/place/find')
        .set('Accept', 'application/json')
        .expect(200)
        .expect(function (res) {

          assert.equal(res.body.places.length, 6);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 4 place in query({"category": {"$in": ["STATION", "RANDOM"] } })', function (done) {
      request
        .get('/place/find')
        .set('Accept', 'application/json')
        .query({query: '{"category": {"$in": ["STATION", "RANDOM"] } }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.places.length, 6);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find place with limit 3', function (done) {
      request
        .get('/place/find')
        .set('Accept', 'application/json')
        .query({limit: 3})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.places.length, 3);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, true);

        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with skip 2', function (done) {
      request
        .get('/place/find')
        .set('Accept', 'application/json')
        .query({skip: 2})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.places.length, 4);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with sort', function (done) {
      request
        .get('/place/find')
        .set('Accept', 'application/json')
        .query({sort: '{"name": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.places.length, 6);
          assert.equal(res.body.places[0].name, "RANDOM1");
          assert.equal(res.body.places[1].name, "RANDOM2");
          assert.equal(res.body.places[2].name, "강남역");
          assert.equal(res.body.places[3].name, "교대역");
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with populate', function (done) {
      request
        .get('/place/find')
        .set('Accept', 'application/json')
        .query({populate: 'photos', sort: '{"name": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.places.length, 6);
          assert.equal(res.body.places[0].name, "RANDOM1");
          assert.equal(res.body.places[0].photos.length, 0);
          assert.equal(res.body.places[1].name, "RANDOM2");
          assert.equal(res.body.places[1].photos.length, 0);
          assert.equal(res.body.places[2].name, "강남역");
          assert.equal(res.body.places[2].photos.length, 2);
          assert.equal(res.body.places[3].name, "교대역");
          assert.equal(res.body.places[3].photos.length, 1);
          assert.equal(res.body.places[3].photos[0] instanceof Object, true);

          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find place in order of nearest', function (done) {
      request
        .get('/place/find')
        .set('Accept', 'application/json')
        .query({query: '{ "geoJSON": { "$near": { "$geometry": { "type": "Point", "coordinates": [127.048941, 37.504479] } } } }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.places.length, 6);
          assert.equal(res.body.places[0].name, "선릉역");
          assert.equal(res.body.places[1].name, "역삼역");
          assert.equal(res.body.places[2].name, "강남역");
          assert.equal(res.body.places[3].name, "교대역");
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });


  describe('GET /place/findOne', function () {

    it('should find first 1 place matching ({"name": "역삼역"}) views++', function (done) {
      request
        .get('/place/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"name": "역삼역"}'})
        .expect(200)
        .expect(function (res) {
          assert.notEqual(res.body.place, null);
          assert.equal(res.body.isLikable, true);
          assert.equal(res.body.place.views, 1);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find first 1 place matching ({"name": "역삼역"}) views++', function (done) {
      request
        .get('/place/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"name": "역삼역"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.place.name, "역삼역");
          assert.equal(res.body.isLikable, true);
          assert.equal(res.body.place.views, 2);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 1 place matching ({"name": "역삼역"}) with populate', function (done) {
      request
        .get('/place/findOne')
        .set('Accept', 'application/json')
        .query({populate: 'photos', query: '{"name": "역삼역"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.place.name, "역삼역");
          assert.equal(res.body.place.photos.length, 2);
          assert.equal(res.body.place.photos[1] instanceof Object, true);
          assert.equal(res.body.isLikable, true);
          assert.equal(res.body.place.views, 3);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });


  });

  describe('POST /place', function () {
    it('should create 1 place ', function (done) {
      request
        .post('/place')
        .send({
          category: "NEW_CATEGORY",
          name: "New Title",
          address: "New Address",
        })
        .expect(200)
        .expect(function (res) {

          // Check new document
          assert.notEqual(res.body.place._id, null);
          assert.equal(res.body.place.category, "NEW_CATEGORY");
          assert.equal(res.body.place.name, "New Title");
          assert.equal(res.body.place.address, "New Address");
        })
        .end(function (err, res) {
          if (err) return done(err);
          // Check document count
          Place.find()
            .then(function (places) {
              assert.equal(places.length, 7);
              done();
            })
            .catch(function (err) {
              done(err);
            });

        });
    });
  });

  describe('PUT /place', function () {

    it('should update 1 place ', function (done) {
      Place.findOne({name: 'New Title'})
        .then(function (place) {
          request
            .put('/place')
            .send({
              _id: place._id,
              category: "UPDATED_CATEGORY",
              name: "Updated Title",
              address: "Updated Address",
            })
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Place.findOne({_id: place._id})
                .then(function (updatedPlace) {
                  assert.equal(updatedPlace._id, place._id);
                  assert.equal(updatedPlace.category, "UPDATED_CATEGORY");
                  assert.equal(updatedPlace.name, "Updated Title");
                  assert.equal(updatedPlace.address, "Updated Address");
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /place', function () {
    it('should soft delete 1 place ', function (done) {
      Place.findOne({name: 'Updated Title'})
        .then(function (place) {
          request
            .delete('/place')
            .query({_id: place._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Place.findOne({_id: place._id})
                .then(function (removedPlace) {
                  assert.equal(removedPlace.isDeleted, true);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /place/hard', function () {
    it('should hard delete 1 place ', function (done) {
      Place.findOne({name: 'Updated Title'})
        .then(function (place) {
          request
            .delete('/place/hard')
            .query({_id: place._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Place.findOne({_id: place._id})
                .then(function (removedPlace) {
                  assert.equal(removedPlace, null);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });


});
