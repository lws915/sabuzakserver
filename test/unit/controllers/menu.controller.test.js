'use strict';

/**
 * Created by Yunseok Han on 09/01/2016
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Yunseok Han - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yunseok Han <developer@applicat.co.kr>, 09/01/2016
 *
 */

var assert = require('assert');
var request = require('supertest')('http://localhost:3000');
var bootstrapper = require('../../bootstrap.test.js');

describe('Menu Controller', function () {

  before(function (done) {
    // Create test data before hand


    var photos = [
      {
        tags: ["MAIN"]
      },
      {
        tags: ["GALLERY"]
      },
      {
        tags: ["GALLERY"]
      },
      {
        tags: ["GALLERY"]
      },
      {
        tags: ["GALLERY"]
      },
      {
        tags: ["GALLERY"]
      }
    ];

    var tags = [
      {
        category: 'MENU',
        index: 0,
        name: '#차',
        modify: true,
      }, {
        category: 'MENU',
        index: 1,
        name: '#커피',
        modify: true,
      }, {
        category: 'MENU',
        index: 2,
        name: '#베이커리',
        modify: true,
      }, {
        category: 'MENU',
        index: 3,
        name: '#상품',
        modify: true,
      }, {
        category: 'MENU',
        index: 4,
        name: '#여름',
        modify: true,
      }, {
        category: 'MENU',
        index: 5,
        name: '#겨울',
        modify: true,
      }
    ];

    bootstrapper.testBefore()
      .then(() => {
        return Promise.all([Photo.create(photos),Tag.create(tags)]);
      })
      .spread(function (photos, tags) {

        var menus = [
          {
            category:'MAIN_MENU',
            name: 'RANDOM1',
            summary: '차가운 물에 장시간 동안 우려내어\n커피 본연의 단 맛을 느끼실 수 있습니다',
            description: '차가운 물에 장시간 동안 우려내어\n커피 본연의 단 맛을 느끼실 수 있습니다',
            prices:[{
              unit:'M',
              price:'4600'
            },{
              unit:'L',
              price:'5600'
            }],
            tags:[tags[0],tags[1],tags[2],tags[3],tags[4],tags[5]]
          },
          {
            category:'MAIN_MENU',
            name: 'RANDOM2',
            summary: '차가운 물에 장시간 동안 우려내어\n커피 본연의 단 맛을 느끼실 수 있습니다',
            description: '차가운 물에 장시간 동안 우려내어\n커피 본연의 단 맛을 느끼실 수 있습니다',
            prices:[{
              unit:'M',
              price:'4600'
            },{
              unit:'L',
              price:'5600'
            }],
          },
          {
            category:'MAIN_MENU',
            name: '가나블루 오리지널',
            summary: '차가운 물에 장시간 동안 우려내어\n커피 본연의 단 맛을 느끼실 수 있습니다',
            description: '차가운 물에 장시간 동안 우려내어\n커피 본연의 단 맛을 느끼실 수 있습니다',
            prices:[{
              unit:'M',
              price:'4600'
            },{
              unit:'L',
              price:'5600'
            }],
            photos: [photos[0],photos[1]]
          },
          {
            category:'MAIN_MENU',
            name: '다가블루 오리지널',
            summary: '차가운 물에 장시간 동안 우려내어\n커피 본연의 단 맛을 느끼실 수 있습니다',
            description: '차가운 물에 장시간 동안 우려내어\n커피 본연의 단 맛을 느끼실 수 있습니다',
            prices:[{
              unit:'M',
              price:'4600'
            },{
              unit:'L',
              price:'5600'
            }],
            photos: [photos[0]]
          },
          {
            category:'SIDE_MENU',
            name: '다다블루 오리지널',
            summary: '차가운 물에 장시간 동안 우려내어\n커피 본연의 단 맛을 느끼실 수 있습니다',
            description: '차가운 물에 장시간 동안 우려내어\n커피 본연의 단 맛을 느끼실 수 있습니다',
            prices:[{
              unit:'M',
              price:'4600'
            },{
              unit:'L',
              price:'5600'
            }],
            photos: [photos[0], photos[1], photos[2], photos[3], photos[4], photos[5]]
          },
          {
            category:'BEVERAGE',
            name: '콜드블루 오리지널',
            summary: '차가운 물에 장시간 동안 우려내어\n커피 본연의 단 맛을 느끼실 수 있습니다',
            description: '차가운 물에 장시간 동안 우려내어\n커피 본연의 단 맛을 느끼실 수 있습니다',
            prices:[{
              unit:'M',
              price:'4600'
            },{
              unit:'L',
              price:'5600'
            }],
            photos: [photos[0], photos[1], photos[2], photos[3], photos[4], photos[5]]
          }
        ];

        return Menu.create(menus);
      })
      .then(function (menus) {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  after(function (done) {
    Promise.all([
      Photo.remove(),
      Menu.remove()
    ])
      .spread(function () {
        return bootstrapper.testAfter();
      })
      .then(function (){
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  it('should exist', function (done) {
    assert.ok(AppZet.controllers.MenuController);
    assert.ok(global.Menu);
    done();
  });

  describe('GET /menu/count', function () {

    it('should count all 6 menus', function (done) {
      request
        .get('/menu/count')
        .set('Accept', 'application/json')
        .expect(200, {count: 6}, done);
    });

  });

  describe('GET /menu/find', function () {

    it('should find all 6 menus', function (done) {
      request
        .get('/menu/find')
        .set('Accept', 'application/json')
        .expect(200)
        .expect(function (res) {

          assert.equal(res.body.menus.length, 6);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find menu with limit 3', function (done) {
      request
        .get('/menu/find')
        .set('Accept', 'application/json')
        .query({limit: 3})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.menus.length, 3);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, true);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with skip 2', function (done) {
      request
        .get('/menu/find')
        .set('Accept', 'application/json')
        .query({skip: 2})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.menus.length, 4);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with sort', function (done) {
      request
        .get('/menu/find')
        .set('Accept', 'application/json')
        .query({sort: '{"name": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.menus.length, 6);
          assert.equal(res.body.menus[0].name, "RANDOM1");
          assert.equal(res.body.menus[1].name, "RANDOM2");
          assert.equal(res.body.menus[2].name, "가나블루 오리지널");
          assert.equal(res.body.menus[3].name, "다가블루 오리지널");
          assert.equal(res.body.menus[4].name, "다다블루 오리지널");
          assert.equal(res.body.menus[5].name, "콜드블루 오리지널");
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with populate', function (done) {
      request
        .get('/menu/find')
        .set('Accept', 'application/json')
        .query({populate: 'photos', sort: '{"name": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.menus.length, 6);
          assert.equal(res.body.menus[0].name, "RANDOM1");
          assert.equal(res.body.menus[0].photos.length, 0);
          assert.equal(res.body.menus[1].name, "RANDOM2");
          assert.equal(res.body.menus[1].photos.length, 0);
          assert.equal(res.body.menus[2].name, "가나블루 오리지널");
          assert.equal(res.body.menus[2].photos.length, 2);
          assert.equal(res.body.menus[3].name, "다가블루 오리지널");
          assert.equal(res.body.menus[3].photos.length, 1);
          assert.equal(res.body.menus[4].name, "다다블루 오리지널");
          assert.equal(res.body.menus[4].photos.length, 6);
          assert.equal(res.body.menus[4].photos[0] instanceof Object, true);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with populate', function (done) {
      request
        .get('/menu/find')
        .set('Accept', 'application/json')
        .query({populate: 'tags', sort: '{"name": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.menus.length, 6);
          assert.equal(res.body.menus[0].name, "RANDOM1");
          assert.equal(res.body.menus[0].tags.length, 6);
          assert.equal(res.body.menus[0].tags[0] instanceof Object, true);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });
  describe('GET /menu/findOne', function () {

    it('should find first 1 menu matching ({"name": "가나블루 오리지널"}) views++', function (done) {
      request
        .get('/menu/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"name": "가나블루 오리지널"}'})
        .expect(200)
        .expect(function (res) {
          assert.notEqual(res.body.menu, null);
          assert.equal(res.body.isLikable, true);
          assert.equal(res.body.menu.views, 1);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find first 1 menu matching ({"name": "가나블루 오리지널"}) views++', function (done) {
      request
        .get('/menu/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"name": "가나블루 오리지널"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.menu.name, "가나블루 오리지널");
          assert.equal(res.body.isLikable, true);
          assert.equal(res.body.menu.views, 2);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 1 menu matching ({"name": "가나블루 오리지널"}) with populate', function (done) {
      request
        .get('/menu/findOne')
        .set('Accept', 'application/json')
        .query({populate: 'photos', query: '{"name": "가나블루 오리지널"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.menu.name, "가나블루 오리지널");
          assert.equal(res.body.menu.photos.length, 2);
          assert.equal(res.body.menu.photos[1] instanceof Object, true);
          assert.equal(res.body.isLikable, true);
          assert.equal(res.body.menu.views, 3);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });


  });

  describe('POST /menu', function () {
    it('should create 1 menu ', function (done) {
      request
        .post('/menu')
        .send({
          category:'NEW_CATEGORY',
          name: '콜드퍼플 오리지널',
          summary: '차가운 물에 장시간 동안 우려내어\n커피 본연의 단 맛을 느끼실 수 있습니다',
          description: '차가운 물에 장시간 동안 우려내어\n커피 본연의 단 맛을 느끼실 수 있습니다',
          prices:[{
            unit:'M',
            price:'4600'
          },{
            unit:'L',
            price:'5600'
          }],
          tags:['콜드브루'],
        })
        .expect(200)
        .expect(function (res) {

          // Check new document
          assert.notEqual(res.body.menu._id, null);
          assert.equal(res.body.menu.category, "NEW_CATEGORY");
          assert.equal(res.body.menu.name, "콜드퍼플 오리지널");
        })
        .end(function (err, res) {
          if (err) return done(err);
          // Check document count
          Menu.find()
            .then(function (menus) {
              assert.equal(menus.length, 7);
              done();
            })
            .catch(function (err) {
              done(err);
            });

        });
    });
  });

  describe('PUT /menu', function () {

    it('should update 1 menu ', function (done) {
      Menu.findOne({name: 'RANDOM1'})
        .then(function (menu) {
          request
            .put('/menu')
            .send({
              _id: menu._id,
              category: "UPDATED_CATEGORY",
              name: "Updated Title",
              description: "Updated Description",
            })
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Menu.findOne({_id: menu._id})
                .then(function (updatedMenu) {
                  assert.equal(updatedMenu._id, menu._id);
                  assert.equal(updatedMenu.category, "UPDATED_CATEGORY");
                  assert.equal(updatedMenu.name, "Updated Title");
                  assert.equal(updatedMenu.description, "Updated Description");
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /menu', function () {
    it('should soft delete 1 menu ', function (done) {
      Menu.findOne({name: 'Updated Title'})
        .then(function (menu) {
          request
            .delete('/menu')
            .query({_id: menu._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Menu.findOne({_id: menu._id})
                .then(function (removedMenu) {
                  assert.equal(removedMenu.isDeleted, true);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /menu/hard', function () {
    it('should hard delete 1 menu ', function (done) {
      Menu.findOne({name: 'Updated Title'})
        .then(function (menu) {
          request
            .delete('/menu/hard')
            .query({_id: menu._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Menu.findOne({_id: menu._id})
                .then(function (removedMenu) {
                  assert.equal(removedMenu, null);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });


});
