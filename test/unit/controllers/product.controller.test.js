'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

var assert = require('assert');
var request = require('supertest')('http://localhost:3000');
var bootstrapper = require('../../bootstrap.test.js');


describe('Product Controller', function () {

  before(function (done) {
    // Create test data before hand


    var photos = [
      {
        tags: ["MAIN", "PROFILE"]
      },
      {
        tags: ["PROFILE"]
      },
      {
        tags: ["MAIN"]
      },
      {
        tags: ["GALLERY"]
      },
      {
        tags: ["MENU"]
      },
    ];

    bootstrapper.testBefore()
      .then(() => {
        return Photo.create(photos);
      })
      .then(function (photos) {

        var products = [
          {
            category: 'ELECTRONICS',
            name: '세탁기',
            description: '세탁기 상품',
            photos: [photos[0], photos[1]]
          },
          {
            category: 'ELECTRONICS',
            name: '냉장고',
            description: '냉장고 상품',
            photos: [photos[2], photos[3]]
          },
          {
            category: 'ELECTRONICS',
            name: '선풍기',
            description: '선풍기 상품',
            photos: [photos[4]]
          },
          {
            category: 'ELECTRONICS',
            name: '에어콘',
            description: '에어콘 상품',
          },
          {
            category: 'TICKET',
            name: '콘서트',
            description: '콘서트 티켓',
          },
          {
            category: 'TICKET',
            name: '뮤지컬',
            description: '뮤지컬 티켓',
          }
        ];

        return Product.create(products);
      })
      .then(function (products) {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  after(function (done) {
    Promise.all([
      Photo.remove(),
      Product.remove(),
    ])
      .spread(function () {
        return bootstrapper.testAfter();
      })
      .then(function(){
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  it('should exist', function (done) {
    assert.ok(AppZet.controllers.ProductController);
    assert.ok(global.Product);
    done();
  });

  describe('GET /product/count', function () {

    it('should count all 6 products', function (done) {
      request
        .get('/product/count')
        .set('Accept', 'application/json')
        .expect(200, {count: 6}, done);
    });

    it('should count only 4 {"query": {"category":"ELECTRONICS"}}', function (done) {
      request
        .get('/product/count')
        .set('Accept', 'application/json')
        .query({query: '{"category": "ELECTRONICS" }'})
        .expect(200, {count: 4}, done);
    });

  });

  describe('GET /product/find', function () {

    it('should find all 6 products', function (done) {
      request
        .get('/product/find')
        .set('Accept', 'application/json')
        .expect(200)
        .expect(function (res) {

          assert.equal(res.body.products.length, 6);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 4 product in query({"category": {"$in": ["ELECTRONICS", "TICKET"] } })', function (done) {
      request
        .get('/product/find')
        .set('Accept', 'application/json')
        .query({query: '{"category": {"$in": ["ELECTRONICS", "TICKET"] } }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.products.length, 6);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find product with limit 3', function (done) {
      request
        .get('/product/find')
        .set('Accept', 'application/json')
        .query({limit: 3})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.products.length, 3);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, true);

        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with skip 2', function (done) {
      request
        .get('/product/find')
        .set('Accept', 'application/json')
        .query({skip: 2})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.products.length, 4);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with sort', function (done) {
      request
        .get('/product/find')
        .set('Accept', 'application/json')
        .query({sort: '{"name": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.products.length, 6);
          assert.equal(res.body.products[0].name, "냉장고");
          assert.equal(res.body.products[1].name, "뮤지컬");
          assert.equal(res.body.products[2].name, "선풍기");
          assert.equal(res.body.products[3].name, "세탁기");
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with populate', function (done) {
      request
        .get('/product/find')
        .set('Accept', 'application/json')
        .query({populate: 'photos', sort: '{"name": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.products.length, 6);
          assert.equal(res.body.products[0].name, "냉장고");
          assert.equal(res.body.products[0].photos.length, 2);
          assert.equal(res.body.products[1].name, "뮤지컬");
          assert.equal(res.body.products[1].photos.length, 0);
          assert.equal(res.body.products[2].name, "선풍기");
          assert.equal(res.body.products[2].photos.length, 1);
          assert.equal(res.body.products[3].name, "세탁기");
          assert.equal(res.body.products[3].photos.length, 2);
          assert.equal(res.body.products[3].photos[0] instanceof Object, true);

          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });


  describe('GET /product/findOne', function () {

    it('should find first 1 product matching ({"name": "냉장고"}) views++', function (done) {
      request
        .get('/product/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"name": "냉장고"}'})
        .expect(200)
        .expect(function (res) {
          assert.notEqual(res.body.product, null);
          assert.equal(res.body.isLikable, true);
          assert.equal(res.body.product.views, 1);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find first 1 product matching ({"name": "냉장고"}) views++', function (done) {
      request
        .get('/product/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"name": "냉장고"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.product.name, "냉장고");
          assert.equal(res.body.isLikable, true);
          assert.equal(res.body.product.views, 2);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 1 product matching ({"name": "냉장고"}) with populate', function (done) {
      request
        .get('/product/findOne')
        .set('Accept', 'application/json')
        .query({populate: 'photos', query: '{"name": "냉장고"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.product.name, "냉장고");
          assert.equal(res.body.product.photos.length, 2);
          assert.equal(res.body.product.photos[1] instanceof Object, true);
          assert.equal(res.body.isLikable, true);
          assert.equal(res.body.product.views, 3);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });


  });

  describe('POST /product', function () {
    it('should create 1 product ', function (done) {
      request
        .post('/product')
        .send({
          category: "NEW_CATEGORY",
          name: "New Name",
          description: "New Description",
        })
        .expect(200)
        .expect(function (res) {

          // Check new document
          assert.notEqual(res.body.product._id, null);
          assert.equal(res.body.product.category, "NEW_CATEGORY");
          assert.equal(res.body.product.name, "New Name");
          assert.equal(res.body.product.description, "New Description");
        })
        .end(function (err, res) {
          if (err) return done(err);
          // Check document count
          Product.find()
            .then(function (products) {
              assert.equal(products.length, 7);
              done();
            })
            .catch(function (err) {
              done(err);
            });

        });
    });
  });

  describe('PUT /product', function () {

    it('should update 1 product ', function (done) {
      Product.findOne({name: 'New Name'})
        .then(function (product) {
          request
            .put('/product')
            .send({
              _id: product._id,
              category: "UPDATED_CATEGORY",
              name: "Updated Title",
              description: "Updated Address",
            })
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Product.findOne({_id: product._id})
                .then(function (updatedProduct) {
                  assert.equal(updatedProduct._id, product._id);
                  assert.equal(updatedProduct.category, "UPDATED_CATEGORY");
                  assert.equal(updatedProduct.name, "Updated Title");
                  assert.equal(updatedProduct.description, "Updated Address");
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /product', function () {
    it('should soft delete 1 product ', function (done) {
      Product.findOne({name: 'Updated Title'})
        .then(function (product) {
          request
            .delete('/product')
            .query({_id: product._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Product.findOne({_id: product._id})
                .then(function (removedProduct) {
                  assert.equal(removedProduct.isDeleted, true);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /product/hard', function () {
    it('should hard delete 1 product ', function (done) {
      Product.findOne({name: 'Updated Title'})
        .then(function (product) {
          request
            .delete('/product/hard')
            .query({_id: product._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Product.findOne({_id: product._id})
                .then(function (removedProduct) {
                  assert.equal(removedProduct, null);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });


});
