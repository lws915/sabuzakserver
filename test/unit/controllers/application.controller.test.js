'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

var assert = require('assert');
var request = require('supertest')('http://localhost:3000');
var bootstrapper = require('../../bootstrap.test.js');

describe('Application Controller', function () {

  before(function (done) {
    // Create test data before hand


    var templates = [
      {
        name: "파란 카페",
        config: {},
      },
      {
        name: "붉은 회사",
        config: {},
      },
      {
        name: "노란 학원",
        config: {},
      },
      {
        name: "푸른 떡볶이",
        config: {},
      },
      {
        name: "회색 팸플릿",
        config: {},
      },
      {
        name: "정열의 이벤트",
        config: {},
      },
    ];

    bootstrapper.testBefore()
      .then(() => {
        return Template.create(templates)
      })
      .then(function (templates) {

        var applications = [
          {
            name: 'K-Town',
            domain: 'K_TOWN',
            config: {
              deliveryFee: 3000
            },
            template: templates[0]._id
          },
          {
            name: 'MyFitMate',
            domain: 'MY_FIT_MATE',
            config: {
              couponFee: 1200
            },
            template: templates[3]._id
          },
          {
            name: '진에어',
            domain: 'JIN_AIR',
            config: {
              ticketPrice: 130000,
              photo: 'http://www.google.co.kr'
            },
            template: templates[1]._id
          },
          {
            name: '학교장터',
            domain: 'SCHOOL_MARKET',
            config: {
              successFee: 500
            },
            template: templates[2]._id
          },
          {
            name: '북경통',
            domain: 'BEIJING_TONG',
            config: {
              adFee: 3000,
              schools: [
                '북경대',
                '상해대',
                '중국대',
                '북경외대',
              ]
            },
            template: templates[5]._id
          },
          {
            name: '골딕',
            domain: 'GOLF_DIC',
            config: {
              club: 100,
              field: 50,
              ball: 10,
              bool: true
            },
            template: templates[4]._id
          }
        ];

        return Application.create(applications);
      })
      .then(function (applications) {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  after(function (done) {
    Promise.all([Application.remove(), Template.remove()])
      .spread(function () {
        return bootstrapper.testAfter();
      })
      .then(function () {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  it('should exist', function (done) {
    assert.ok(AppZet.controllers.ApplicationController);
    assert.ok(global.Application);
    done();
  });

  describe('GET /application/count', function () {

    it('should find all 6 applications', function (done) {
      request
        .get('/application/count')
        .set('Accept', 'application/json')
        .expect(200, {count: 6}, done);
    });

    it('should find only 1 {"query": {"name":"북경통"}}', function (done) {
      request
        .get('/application/count')
        .set('Accept', 'application/json')
        .query({query: '{"name":"북경통"}'})
        .expect(200, {count: 1}, done);
    });

    it('should find only 1 {"config.club": 100 }', function (done) {
      request
        .get('/application/count')
        .set('Accept', 'application/json')
        .query({query: '{"config.club": 100 }'})
        .expect(200, {count: 1}, done);
    });

  });

  describe('GET /application/find', function () {

    it('should find all 6 applications', function (done) {
      request
        .get('/application/find')
        .set('Accept', 'application/json')
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.applications.length, 6);
          assert.equal(res.body.more, false);
          assert.equal(res.body.total, 6);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 3 applications in query({"name": {"$in": ["K-Town", "MyFitMate", "진에어"] } })', function (done) {
      request
        .get('/application/find')
        .set('Accept', 'application/json')
        .query({query: '{"name": {"$in": ["K-Town", "MyFitMate", "진에어"] } }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.applications.length, 3);
          assert.equal(res.body.more, false);
          assert.equal(res.body.total, 3);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find application with limit 3', function (done) {
      request
        .get('/application/find')
        .set('Accept', 'application/json')
        .query({limit: 3})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.applications.length, 3);
          assert.equal(res.body.more, true);
          assert.equal(res.body.total, 6);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with skip 2', function (done) {
      request
        .get('/application/find')
        .set('Accept', 'application/json')
        .query({skip: 2})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.applications.length, 4);
          assert.equal(res.body.more, false);
          assert.equal(res.body.total, 6);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with sort', function (done) {
      request
        .get('/application/find')
        .set('Accept', 'application/json')
        .query({sort: '{"name": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.applications.length, 6);
          assert.equal(res.body.applications[0].name, "K-Town");
          assert.equal(res.body.applications[1].name, "MyFitMate");
          assert.equal(res.body.applications[2].name, "골딕");
          assert.equal(res.body.applications[3].name, "북경통");
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with populate', function (done) {
      request
        .get('/application/find')
        .set('Accept', 'application/json')
        .query({populate: 'template', sort: '{"name": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.applications.length, 6);
          assert.equal(res.body.applications[0].name, "K-Town");
          assert.equal(res.body.applications[0].template.name, "파란 카페");
          assert.equal(res.body.applications[1].name, "MyFitMate");
          assert.equal(res.body.applications[1].template.name, "푸른 떡볶이");
          assert.equal(res.body.applications[2].name, "골딕");
          assert.equal(res.body.applications[2].template.name, "회색 팸플릿");
          assert.equal(res.body.applications[3].name, "북경통");
          assert.equal(res.body.applications[3].template.name, "정열의 이벤트");
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });


  describe('GET /application/findOne', function () {

    it('should find first 1 application', function (done) {
      request
        .get('/application/findOne')
        .set('Accept', 'application/json')
        .expect(200)
        .expect(function (res) {
          assert.notEqual(res.body.application, null);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find first 1 application matching ({"domain": "K_TOWN"})', function (done) {
      request
        .get('/application/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"domain": "K_TOWN"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.application.name, "K-Town");
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 1 application matching ({"domain": "K_TOWN"}) with populate', function (done) {
      request
        .get('/application/findOne')
        .set('Accept', 'application/json')
        .query({populate: 'template', query: '{"domain": "K_TOWN"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.application.name, "K-Town");
          assert.ok(res.body.application.template._id);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });

  describe('POST /application', function () {
    it('should create 1 application ', function (done) {
      request
        .post('/application')
        .send({
          name: 'New Application',
          domain: 'NEW_APPLICATION',
          config: {
            newConfig: "newConfig"
          }
        })
        .expect(200)
        .expect(function (res) {

          // Check new document
          assert.notEqual(res.body.application._id, null);
          assert.equal(res.body.application.name, "New Application");
          assert.equal(res.body.application.domain, "NEW_APPLICATION");
          assert.equal(res.body.application.config.newConfig, "newConfig");
        })
        .end(function (err, res) {
          if (err) return done(err);
          // Check document count
          Application.find()
            .then(function (applications) {
              assert.equal(applications.length, 7);
              done();
            })
            .catch(function (err) {
              done(err);
            });

        });
    });
  });

  describe('PUT /application', function () {

    it('should update 1 application ', function (done) {
      Application.findOne({name: 'New Application'})
        .then(function (application) {
          request
            .put('/application')
            .send({
              _id: application._id,
              name: 'Updated Application',
              domain: 'UPDATED_APPLICATION',
              config: {
                updatedConfig: "updatedConfig"
              }
            })
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Application.findOne({_id: application._id})
                .then(function (updatedApplication) {
                  assert.equal(updatedApplication._id, application._id);
                  assert.equal(updatedApplication.name, "Updated Application");
                  assert.equal(updatedApplication.domain, "UPDATED_APPLICATION");
                  assert.equal(updatedApplication.config.updatedConfig, "updatedConfig");
                  assert.equal(updatedApplication.config.newConfig, null);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /application', function () {
    it('should soft delete 1 application ', function (done) {
      Application.findOne({name: 'Updated Application'})
        .then(function (application) {
          request
            .delete('/application')
            .query({_id: application._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Application.findOne({_id: application._id})
                .then(function (removedApplication) {
                  assert.equal(removedApplication.isDeleted, true);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /application/hard', function () {
    it('should soft delete 1 application ', function (done) {
      Application.findOne({name: 'Updated Application'})
        .then(function (application) {
          request
            .delete('/application/hard')
            .query({_id: application._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Application.findOne({_id: application._id})
                .then(function (removedApplication) {
                  assert.equal(removedApplication, null);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });
});
