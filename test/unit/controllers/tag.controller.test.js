'user strict';
/**
 * Created by Yunseok Han on 09/26/2016
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Yunseok Han - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yunseok Han <developer@applicat.co.kr>, 09/26/2016
 *
 */
var assert = require('assert');
var request = require('supertest')('http://localhost:3000');
var bootstrapper = require('../../bootstrap.test.js');

describe('Tag Controller', function () {

  // 테스트 시작전 임시 데이터를 만듭니다.
  before(function (done) {
    var tags = [
      {
        category: 'MENU',
        index: 0,
        name: '#차',
        server: 2
      }, {
        category: 'MENU',
        index: 1,
        name: '#커피',
        server: 2
      }, {
        category: 'MENU',
        index: 2,
        name: '#베이커리',
        server: 2
      }, {
        category: 'MENU',
        index: 3,
        name: '#상품',
        server: 2
      }, {
        category: 'MENU',
        index: 4,
        name: '#여름',
        server: 2
      }, {
        category: 'MENU',
        index: 5,
        name: '#겨울',
        server: 2
      }
    ];

    bootstrapper.testBefore()
      .then(() => {
        return Tag.create(tags);
      })
      .then(function (tags) {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  after(function (done) {
    Promise.all([
      Tag.remove(),
    ])
      .spread(function () {
        return bootstrapper.testAfter();
      })
      .then(function () {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  describe('GET /tag/count', function () {

    it('should count all 6 tags', function (done) {
      request
        .get('/server/2/tag/count')
        .set('Accept', 'application/json')
        .expect(200, {count: 6}, done);
    });

  });

  describe('GET /tag/find', function () {

    it('should find all 6 tags', function (done) {
      request
        .get('/tag/find')
        .set('Accept', 'application/json')
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.tags.length, 6);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with sort', function (done) {
      request
        .get('/tag/find')
        .set('Accept', 'application/json')
        .query({sort: '{"index": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.tags.length, 6);
          assert.equal(res.body.tags[0].name, "#차");
          assert.equal(res.body.tags[1].name, "#커피");
          assert.equal(res.body.tags[2].name, "#베이커리");
          assert.equal(res.body.tags[3].name, "#상품");
          assert.equal(res.body.tags[4].name, "#여름");
          assert.equal(res.body.tags[5].name, "#겨울");
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });

  describe('GET /tag/findOne', function () {

    it('should find first 1 tag matching ({"name": "#커피"})', function (done) {
      request
        .get('/tag/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"name": "#커피"}'})
        .expect(200)
        .expect(function (res) {
          assert.notEqual(res.body.tag, null);
          assert.equal(res.body.tag.name, '#커피');
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });

  describe('PUT /tag', function () {

    it('should update 1 tag ', function (done) {
      Tag.findOne({name: '#커피'})
        .then(function (tag) {
          request
            .put('/tag')
            .send({
              _id: tag._id,
              name: "#아메리카노"
            })
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Tag.findOne({_id: tag._id})
                .then(function (updatedTag) {
                  assert.equal(updatedTag._id, tag._id);
                  assert.equal(updatedTag.name, "#아메리카노");
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /tag/hard', function () {
    it('should hard delete 1 tag ', function (done) {
      Tag.findOne({name: '#아메리카노'})
        .then(function (tag) {
          request
            .delete('/tag/hard')
            .query({_id: tag._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Tag.findOne({_id: tag._id})
                .then(function (removedTag) {
                  assert.equal(removedTag, null);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

});