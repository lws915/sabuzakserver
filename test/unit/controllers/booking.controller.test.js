'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

var assert = require('assert');
var request = require('supertest')('http://localhost:3000');
var bootstrapper = require('../../bootstrap.test.js');

describe('Booking Controller', function () {

  before(function (done) {
    // Create test data before hand


    var places = [
      {
        category: 'STATION',
        name: '강남역',
        address: '강남역 주소',
        geoJSON: [127.027642, 37.497959],
      },
      {
        category: 'STATION',
        name: '역삼역',
        address: '역삼역 주소',
        geoJSON: [127.037393, 37.499907],
      },
      {
        category: 'STATION',
        name: '교대역',
        address: '교대역 주소',
        geoJSON: [127.01464299999998, 37.49396600000001],
      },
      {
        category: 'STATION',
        name: '선릉역',
        address: '선릉역 주소',
        geoJSON: [127.048941, 37.504479]
      },
      {
        category: 'RANDOM',
        name: 'RANDOM1',
        address: '광화문',
        geoJSON: [126.976923, 37.575988]
      },
      {
        category: 'RANDOM',
        name: 'RANDOM2',
        address: '응암',
        geoJSON: [126.920521, 37.592665]
      }
    ];

    bootstrapper.testBefore()
      .then(() => {
      })
      .then(() =>{
        return Place.create(places);
      })
      .then(function (places) {

        var date = new Date();

        var bookings = [];
        bookings.push({category: 'SERVICE', datetime: date.setHours(1), place: places[0]._id});
        bookings.push({category: 'SERVICE', datetime: date.setHours(2), place: places[1]._id});
        bookings.push({category: 'SERVICE', datetime: date.setHours(3), place: places[2]._id});
        bookings.push({category: 'DINING', datetime: date.setHours(4), place: places[3]._id});
        bookings.push({category: 'DINING', datetime: date.setHours(5), place: places[4]._id});
        bookings.push({category: 'DINING', datetime: date.setHours(6), place: places[5]._id});

        return Booking.create(bookings);
      })
      .then(function (bookings) {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  after(function (done) {
    Promise.all([Place.remove(), Booking.remove()])
      .spread(function () {
        return bootstrapper.testAfter();
      })
      .then(function(){
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  it('should exist', function (done) {
    assert.ok(AppZet.controllers.BookingController);
    assert.ok(global.Booking);
    done();
  });

  describe('GET /booking/count', function () {

    it('should count all 6 bookings', function (done) {
      request
        .get('/booking/count')
        .set('Accept', 'application/json')
        .expect(200, {count: 6}, done);
    });

    it('should count only 3 {"query": {"category":"DINING"}}', function (done) {
      request
        .get('/booking/count')
        .set('Accept', 'application/json')
        .query({query: '{"category": "DINING" }'})
        .expect(200, {count: 3}, done);
    });

  });

  describe('GET /booking/find', function () {

    it('should find all 6 bookings', function (done) {
      request
        .get('/booking/find')
        .set('Accept', 'application/json')
        .expect(200)
        .expect(function (res) {

          assert.equal(res.body.bookings.length, 6);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 4 booking in query({"category": {"$in": ["SERVICE", "DINING"] } })', function (done) {
      request
        .get('/booking/find')
        .set('Accept', 'application/json')
        .query({query: '{"category": {"$in": ["SERVICE", "DINING"] } }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.bookings.length, 6);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find booking with limit 3', function (done) {
      request
        .get('/booking/find')
        .set('Accept', 'application/json')
        .query({limit: 3})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.bookings.length, 3);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, true);

        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with skip 2', function (done) {
      request
        .get('/booking/find')
        .set('Accept', 'application/json')
        .query({skip: 2})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.bookings.length, 4);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with sort', function (done) {
      request
        .get('/booking/find')
        .set('Accept', 'application/json')
        .query({sort: '{"datetime": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.bookings.length, 6);
          assert.equal(res.body.bookings[0].category, "SERVICE");
          assert.equal(res.body.bookings[1].category, "SERVICE");
          assert.equal(res.body.bookings[2].category, "SERVICE");
          assert.equal(res.body.bookings[3].category, "DINING");
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with populate', function (done) {
      request
        .get('/booking/find')
        .set('Accept', 'application/json')
        .query({populate: 'place', sort: '{"datetime": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.bookings.length, 6);
          assert.equal(res.body.bookings[0].category, "SERVICE");
          assert.equal(res.body.bookings[0].place.name, "강남역");
          assert.equal(res.body.bookings[1].category, "SERVICE");
          assert.equal(res.body.bookings[1].place.name, "역삼역");
          assert.equal(res.body.bookings[2].category, "SERVICE");
          assert.equal(res.body.bookings[2].place.name, "교대역");
          assert.equal(res.body.bookings[3].category, "DINING");
          assert.equal(res.body.bookings[3].place.name, "선릉역");
          assert.equal(res.body.bookings[3].place instanceof Object, true);

          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });
  });


  describe('GET /booking/findOne', function () {

    it('should find first 1 booking matching ({"category": "SERVICE"}) views++', function (done) {
      request
        .get('/booking/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"category": "SERVICE"}'})
        .expect(200)
        .expect(function (res) {
          assert.notEqual(res.body.booking, null);
          assert.equal(res.body.booking.views, 1);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find first 1 booking matching ({"category": "SERVICE"}) views++', function (done) {
      request
        .get('/booking/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"category": "SERVICE"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.booking.category, "SERVICE");
          assert.equal(res.body.booking.views, 2);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 1 booking matching ({"category": "SERVICE"}) with populate', function (done) {
      request
        .get('/booking/findOne')
        .set('Accept', 'application/json')
        .query({populate: 'place', query: '{"category": "SERVICE"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.booking.category, "SERVICE");
          assert.equal(res.body.booking.place.category, "STATION");
          assert.equal(res.body.booking.place instanceof Object, true);
          assert.equal(res.body.booking.views, 3);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });
  });

  describe('POST /booking', function () {
    it('should create 1 booking ', function (done) {
      request
        .post('/booking')
        .send({
          category: "NEW_CATEGORY",
          datetime: new Date(),
          status: "CONFIRMED",
        })
        .expect(200)
        .expect(function (res) {

          // Check new document
          assert.notEqual(res.body.booking._id, null);
          assert.equal(res.body.booking.category, "NEW_CATEGORY");
          assert.notEqual(res.body.booking.datetime, null);
          assert.equal(res.body.booking.status, "CONFIRMED");
        })
        .end(function (err, res) {
          if (err) return done(err);
          // Check document count
          Booking.find()
            .then(function (bookings) {
              assert.equal(bookings.length, 7);
              done();
            })
            .catch(function (err) {
              done(err);
            });

        });
    });
  });

  describe('PUT /booking', function () {
    it('should update 1 booking ', function (done) {
      Booking.findOne({category: 'NEW_CATEGORY'})
        .then(function (booking) {
          request
            .put('/booking')
            .send({
              _id: booking._id,
              category: "UPDATED_CATEGORY",
              datetime: new Date(),
              status: "DONE",
            })
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Booking.findOne({_id: booking._id})
                .then(function (updatedBooking) {
                  assert.equal(updatedBooking._id, booking._id);
                  assert.equal(updatedBooking.category, "UPDATED_CATEGORY");
                  assert.notEqual(updatedBooking.datetime, null);
                  assert.equal(updatedBooking.status, "DONE");
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /booking', function () {
    it('should soft delete 1 booking ', function (done) {
      Booking.findOne({category: 'UPDATED_CATEGORY'})
        .then(function (booking) {
          request
            .delete('/booking')
            .query({_id: booking._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Booking.findOne({_id: booking._id})
                .then(function (removedBooking) {
                  assert.equal(removedBooking.isDeleted, true);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /booking/hard', function () {
    it('should hard delete 1 booking ', function (done) {
      Booking.findOne({category: 'UPDATED_CATEGORY'})
        .then(function (booking) {
          request
            .delete('/booking/hard')
            .query({_id: booking._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Booking.findOne({_id: booking._id})
                .then(function (removedBooking) {
                  assert.equal(removedBooking, null);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });
});
