'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

var assert = require('assert');
var request = require('supertest')('http://localhost:3000');
var nodemailer = require("nodemailer");
var sgTransport = require('nodemailer-sendgrid-transport');
var bootstrapper = require('../../bootstrap.test.js');

describe('Auth Controller', function () {

  before(function (done) {
    // Create test data before hand

    var users = [
      {
        email: 'developer@applicat.co.kr',
        username: 'admin',
        nickname: '관리자',
        password: 'admin1234'
      },
      {
        email: 'developer1@applicat.co.kr',
        username: 'user1',
        nickname: '사용자1',
        password: 'admin1234'
      },
      {
        email: 'developer2@applicat.co.kr',
        username: 'user2',
        nickname: '사용자2',
        password: 'admin1234'
      },
      {
        email: 'developer3@applicat.co.kr',
        username: 'user3',
        nickname: '사용자3',
        password: 'admin1234'
      }
    ];

    bootstrapper.testBefore()
      .then(() => {

        // initialise mailservice for password reset
        MailService.init({
          templatePath: __dirname + "/../../../src/config/mail_template/",
          expiry: 60,
          transport: nodemailer.createTransport(sgTransport({
            auth: {
              api_key: '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'
            }
          }))
        });

        var promises = [];

        _.forEach(users, function (user) {
          promises.push(User.register(user));
        });

        return Promise.all(promises);
      })
      .spread(function (users) {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  after(function (done) {
    Promise.all([Passport.remove(), User.remove()])
      .spread(function () {
        return bootstrapper.testAfter();
      })
      .then(function () {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  it('should exist', function (done) {
    assert.ok(AppZet.controllers.AuthController);
    done();
  });

  describe('GET /checkEmail', function () {

    it('should be not available', function (done) {
      request
        .get('/checkEmail')
        .set('Accept', 'application/json')
        .query({
          email: 'developer@applicat.co.kr'
        })
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.isAvailable, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should be available', function (done) {
      request
        .get('/checkEmail')
        .set('Accept', 'application/json')
        .query({
          email: 'developer10@applicat.co.kr'
        })
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.isAvailable, true);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });
  });

  describe('GET /checkUsername', function () {

    it('should be not available', function (done) {
      request
        .get('/checkUsername')
        .set('Accept', 'application/json')
        .query({
          username: 'admin'
        })
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.isAvailable, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should be available', function (done) {
      request
        .get('/checkUsername')
        .set('Accept', 'application/json')
        .query({
          username: 'admin1'
        })
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.isAvailable, true);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });
  });


  describe('GET /checkNickname', function () {

    it('should be not available', function (done) {
      request
        .get('/checkNickname')
        .set('Accept', 'application/json')
        .query({
          nickname: '관리자'
        })
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.isAvailable, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should be available', function (done) {
      request
        .get('/checkNickname')
        .set('Accept', 'application/json')
        .query({
          nickname: '관리자1'
        })
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.isAvailable, true);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });
  });


  describe('POST /login', function () {

    it('should login', function (done) {
      request
        .post('/login')
        .send({
          identifier: 'developer@applicat.co.kr',
          password: 'admin1234'
        })
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.user.email, "developer@applicat.co.kr");
          protocols.bearer({}, res.body.token, function (error, user) {
            assert.equal(user.email, "developer@applicat.co.kr");
          });

        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('fail login bad credential', function (done) {
      request
        .post('/login')
        .send({
          identifier: 'developer@applicat.co.kr',
          password: 'admin12341'
        })
        .expect(401)
        .expect(function (res) {
          assert.equal(res.body.error.location, "client");
          assert.equal(res.body.error.domain, "security");
          assert.equal(res.body.error.subject, "authentication");
          assert.equal(res.body.error.code, 401);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });
  });


  describe('GET /logout', function () {

    it('should logout', function (done) {
      request
        .get('/logout')
        .expect(200)
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });
  });


  describe('POST /register', function () {

    it('should register', function (done) {
      request
        .post('/register')
        .send({
          email: 'developer4@applicat.co.kr',
          username: 'user4',
          nickname: '사용자4',
          password: 'admin1234'
        })
        .expect(200)
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should throw bad request error', function (done) {
      request
        .post('/register')
        .send({
          username: 'user4',
          nickname: '사용자4',
          password: 'admin1234'
        })
        .expect(400)
        .expect(function (res) {
          assert.equal(res.body.error.location, "client");
          assert.equal(res.body.error.domain, "api");
          assert.equal(res.body.error.code, 400);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });
  });


  describe('GET /me success', function () {
    var token;

    before(function (done) {
      request
        .post('/login')
        .send({
          identifier: 'developer@applicat.co.kr',
          password: 'admin1234'
        })
        .end(function (err, res) {
          if (err) return done(err);
          token = res.body.token;
          done();
        });
    });

    it('get user info with assigned token', function (done) {
      request
        .get('/me')
        .set('Authorization', 'Bearer ' + token)
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.user.email, "developer@applicat.co.kr");
          assert.equal(res.body.user.username, "admin");
          assert.equal(res.body.user.nickname, "관리자");
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });
  });

  describe('GET /me authorization fail', function () {
    before(function (done) {
      request
        .get('/logout')
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('reject request with forbiddenError', function (done) {
      request
        .get('/me')
        .expect(403)
        .expect(function (res) {
          assert.equal(res.body.error.location, "client");
          assert.equal(res.body.error.domain, "security");
          assert.equal(res.body.error.subject, "authorization");
          assert.equal(res.body.error.code, 403);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });
  });


  describe('PUT /me success', function () {
    var token;

    before(function (done) {
      request
        .post('/login')
        .send({
          identifier: 'developer@applicat.co.kr',
          password: 'admin1234'
        })
        .end(function (err, res) {
          if (err) return done(err);
          token = res.body.token;
          done();
        });
    });

    it('update user info with assigned token', function (done) {
      request
        .put('/me')
        .set('Authorization', 'Bearer ' + token)
        .send({
          nickname: 'testNickname'
        })
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.user.nickname, "testNickname");
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });
  });


  describe('PUT /me authorization fail', function () {
    before(function (done) {
      request
        .get('/logout')
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('reject request with forbiddenError', function (done) {
      request
        .put('/me')
        .expect(403)
        .expect(function (res) {
          assert.equal(res.body.error.location, "client");
          assert.equal(res.body.error.domain, "security");
          assert.equal(res.body.error.subject, "authorization");
          assert.equal(res.body.error.code, 403);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });
  });

  //// Warning: enable when you want to password reset
  // describe('#passwordreset cycle', function () {
  //   var agent;
  //   before(function (done) {
  //     agent = require('supertest').agent(AppZet.server);
  //     done();
  //   });
  //
  //   it('passwordResetStart', function (done) {
  //     agent
  //       .post('/forgotPassword')
  //       .send({'email': 'developer@applicat.co.kr'})
  //       .expect(200)
  //       .expect(function (res) {
  //         User.findOne({email: 'developer@applicat.co.kr'})
  //           .then(function (user) {
  //             assert.ok(user.password_reset_code);
  //             assert.ok(user.password_reset_time);
  //             assert.ok(user.password_reset_counter);
  //             assert.ok(user.password_reset_request_time);
  //           });
  //       })
  //       .end(function (err, res) {
  //         if (err) return done(err);
  //         done();
  //       });
  //   });
  //
  //   it('passwordResetCheck', function (done) {
  //
  //     User.findOne({email: 'developer@applicat.co.kr'})
  //       .then(function (user) {
  //
  //         agent
  //           .post('/forgotPasswordCheck')
  //           .send({'email': 'developer@applicat.co.kr', 'code': user.password_reset_code})
  //           .expect(200)
  //           .expect(function (res) {
  //             User.findOne({email: 'developer@applicat.co.kr'})
  //               .then(function (user) {
  //                 assert.equal(user.password_reset_code, "X");
  //                 assert.equal(user.password_reset_time, 0);
  //                 assert.equal(user.password_reset_counter, 0);
  //                 assert.equal(user.password_reset_request_time, 0);
  //               });
  //           })
  //           .end(function (err, res) {
  //             if (err) return done(err);
  //             done();
  //           });
  //       });
  //   });
  //
  //   it('passwordResetComplete', function (done) {
  //
  //     agent
  //       .put('/forgotPasswordComplete')
  //       .send({'newPassword': 'test1234'})
  //       .expect(200)
  //       .expect(function (res) {
  //
  //         request
  //           .post('/login')
  //           .send({
  //             identifier: 'developer@applicat.co.kr',
  //             password: 'test1234'
  //           })
  //           .expect(200);
  //
  //       })
  //       .end(function (err, res) {
  //         if (err) return done(err);
  //         done();
  //       });
  //
  //   });
  //
  // });

  //// Warning: enable when you want to password reset
  describe('#changePassword()', function () {
    var agent;

    before(function (done) {
      agent = require('supertest').agent(AppZet.server);

      agent
        .post('/login')
        .send({
          identifier: 'developer3@applicat.co.kr',
          password: 'admin1234'
        })
        .end(function (err, res) {
          done();
        });
    });

    // startPasswordReset: startPasswordReset,
    it('should fail change password', function (done) {
      agent
        .put('/changePassword')
        .send({'oldPassword': 'admin12341', 'newPassword': 'test1234'})
        .expect(400)
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should changing password', function (done) {
      agent
        .put('/changePassword')
        .send({'oldPassword': 'admin1234', 'newPassword': 'test1234'})
        .expect(200)
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });
  });

});
