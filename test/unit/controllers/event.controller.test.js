/**
 * Created by Philip on 11/08/2016
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Philip MaengKwan Seo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Philip MaengKwan Seo <developer@applicat.co.kr>, 11/08/2016
 *
 */

'use strict';

var assert = require('assert');
var request = require('supertest')('http://localhost:3000');
var bootstrapper = require('../../bootstrap.test.js');

describe('Event Controller', function () {

  before(function (done) {
    // Create test data before hand

    var photos = [
      {
        tags: ["MAIN"]
      },
      {
        tags: ["DETAIL"]
      }
    ];

    bootstrapper.testBefore()
      .then(()=>{
        return Photo.create(photos);
      })
      .then(function (photos) {

        var events = [
          {
            category: "NORMAL",
            title: "일반 이벤트1",
            content: "이벤트 내용1",
            photos: [photos[0], photos[1]],
            startDate: new Date(),
            endDate: new Date(),
            winners: []
          },
          {
            category: "NORMAL",
            title: "일반 이벤트2",
            content: "이벤트 내용2",
            photos: [photos[0]],
            startDate: new Date(),
            endDate: new Date(),
            winners: []
          },
          {
            category: "NORMAL",
            title: "일반 이벤트3",
            content: "일반 이벤트 내용3",
            startDate: new Date(),
            endDate: new Date(),
            winners: []
          },
          {
            category: "ADMISSION",
            title: "응모 이벤트1",
            content: "응모 이벤트 내용1",
            photos: [photos[0], photos[1]],
            startDate: new Date(),
            endDate: new Date(),
            winners: []
          },
          {
            category: "ADMISSION",
            title: "응모 이벤트2",
            content: "응모 이벤트 내용2",
            photos: [photos[0]],
            startDate: new Date(),
            endDate: new Date(),
            winners: []
          },
          {
            category: "ADMISSION",
            title: "응모 이벤트3",
            content: "응모 이벤트 내용3",
            startDate: new Date(),
            endDate: new Date(),
            winners: []
          }
        ];

        // var eventAdmissions = [
        //   {
        //     name: "응모자1",
        //     phone: "01012345678",
        //     email: "admission@test.co.kr",
        //     event: events[0]
        //   },
        //   {
        //     name: "응모자2",
        //     phone: "01012345678",
        //     email: "admission@test.co.kr",
        //     event: events[1]
        //   },
        //   {
        //     name: "응모자3",
        //     phone: "01012345678",
        //     email: "admission@test.co.kr",
        //     event: events[2]
        //   },
        //   {
        //     name: "응모자4",
        //     phone: "01012345678",
        //     email: "admission@test.co.kr",
        //     event: events[3]
        //   },
        //   {
        //     name: "응모자5",
        //     phone: "01012345678",
        //     email: "admission@test.co.kr",
        //     event: events[4]
        //   }
        // ];

        return Event.create(events);
      })
      .then(function (events) {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  after(function (done) {
    Promise.all([
      Photo.remove(),
      Event.remove(),
    ])
      .spread(function () {
        return bootstrapper.testAfter();
      })
      .then(function(){
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  it('should exist', function (done) {
    assert.ok(AppZet.controllers.EventController);
    assert.ok(global.Event);
    done();
  });

  describe('GET /event/count', function () {

    it('should find all 6 events', function (done) {
      request
        .get('/event/count')
        .set('Accept', 'application/json')
        .expect(200, {count: 6}, done);
    });

  });

  describe('GET /event/find', function () {

    it('should find all 6 events', function (done) {
      request
        .get('/event/find')
        .set('Accept', 'application/json')
        .expect(200)
        .expect(function (res) {

          assert.equal(res.body.events.length, 6);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 3 event in query({"category": {"$in": ["ADMISSION"] } })', function (done) {
      request
        .get('/event/find')
        .set('Accept', 'application/json')
        .query({query: '{"category": {"$in": ["ADMISSION"] } }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.events.length, 3);
          assert.equal(res.body.total, 3);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find event with limit 3', function (done) {
      request
        .get('/event/find')
        .set('Accept', 'application/json')
        .query({limit: 3})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.events.length, 3);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, true);

        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with skip 2', function (done) {
      request
        .get('/event/find')
        .set('Accept', 'application/json')
        .query({skip: 2})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.events.length, 4);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with sort', function (done) {
      request
        .get('/event/find')
        .set('Accept', 'application/json')
        .query({sort: '{"title": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.events.length, 6);
          assert.equal(res.body.events[0].title, "응모 이벤트1");
          assert.equal(res.body.events[1].title, "응모 이벤트2");
          assert.equal(res.body.events[2].title, "응모 이벤트3");
          assert.equal(res.body.events[3].title, "일반 이벤트1");
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with populate', function (done) {
      request
        .get('/event/find')
        .set('Accept', 'application/json')
        .query({populate: 'photos', sort: '{"title": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.events.length, 6);
          assert.equal(res.body.events[0].title, "응모 이벤트1");
          assert.equal(res.body.events[0].photos.length, 2);
          assert.equal(res.body.events[1].title, "응모 이벤트2");
          assert.equal(res.body.events[1].photos.length, 1);
          assert.equal(res.body.events[1].photos[0] instanceof Object, true);
          assert.equal(res.body.events[2].title, "응모 이벤트3");
          assert.equal(res.body.events[2].photos.length, 0);
          assert.equal(res.body.events[3].title, "일반 이벤트1");
          assert.equal(res.body.events[3].photos.length, 2);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });

  describe('GET /event/findOne', function () {

    it('should find first 1 event matching ({"title": "일반 이벤트1"}) views++', function (done) {
      request
        .get('/event/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"title": "일반 이벤트1"}'})
        .expect(200)
        .expect(function (res) {
          assert.notEqual(res.body.event, null);
          assert.equal(res.body.event.views, 1);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find first 1 event matching ({"title": "일반 이벤트1"}) views++', function (done) {
      request
        .get('/event/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"title": "일반 이벤트1"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.event.title, "일반 이벤트1");
          assert.equal(res.body.event.views, 2);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 1 event matching ({"title": "일반 이벤트1"}) with populate', function (done) {
      request
        .get('/event/findOne')
        .set('Accept', 'application/json')
        .query({populate: 'photos', query: '{"title": "일반 이벤트1"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.event.title, "일반 이벤트1");
          assert.equal(res.body.event.photos.length, 2);
          assert.equal(res.body.event.views, 3);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });

  describe('POST /event', function () {
    it('should create 1 event ', function (done) {
      request
        .post('/event')
        .send({
          category: "NEW_CATEGORY",
          title: "New Title",
          content: "New Content",
          winners: []
        })
        .expect(200)
        .expect(function (res) {

          // Check new document
          assert.notEqual(res.body.event._id, null);
          assert.equal(res.body.event.category, "NEW_CATEGORY");
          assert.equal(res.body.event.title, "New Title");
          assert.equal(res.body.event.content, "New Content");
        })
        .end(function (err, res) {
          if (err) return done(err);
          // Check document count
          Event.find()
            .then(function (events) {
              assert.equal(events.length, 7);
              done();
            })
            .catch(function (err) {
              done(err);
            });

        });
    });
  });

  describe('PUT /event', function () {

    it('should update 1 event ', function (done) {
      Event.findOne({title: 'New Title'})
        .then(function (event) {
          request
            .put('/event')
            .send({
              _id: event._id,
              category: "UPDATED_CATEGORY",
              title: "Updated Title",
              content: "Updated Content",
            })
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Event.findOne({_id: event._id})
                .then(function (updatedEvent) {
                  assert.equal(updatedEvent._id, event._id);
                  assert.equal(updatedEvent.category, "UPDATED_CATEGORY");
                  assert.equal(updatedEvent.title, "Updated Title");
                  assert.equal(updatedEvent.content, "Updated Content");
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });


  // TODO: make event winners -> 이벤트 당첨자 어디서 생성해야 하는가.
  // describe('PUT /event', function () {
  //
  //   it('should make winners ', function (done) {
  //     Event.findOne({title: 'New Title'})
  //       .then(function (event) {
  //         request
  //           .put('/event')
  //           .send({
  //             _id: event._id,
  //             winners: []
  //           })
  //           .expect(200)
  //           .end(function (err, res) {
  //             if (err) return done(err);
  //             // Check document count
  //             Event.findOne({_id: event._id})
  //               .then(function (updatedEvent) {
  //                 assert.equal(updatedEvent._id, event._id);
  //                 assert.equal(updatedEvent.category, "UPDATED_CATEGORY");
  //                 assert.equal(updatedEvent.title, "Updated Title");
  //                 assert.equal(updatedEvent.content, "Updated Content");
  //                 done();
  //               })
  //               .catch(function (err) {
  //                 done(err);
  //               });
  //           });
  //       })
  //       .catch(function (err) {
  //         done(err);
  //       });
  //   });
  // });

  describe('DELETE /event', function () {
    it('should soft delete 1 event ', function (done) {
      Event.findOne({title: 'Updated Title'})
        .then(function (event) {
          request
            .delete('/event')
            .query({_id: event._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Event.findOne({_id: event._id})
                .then(function (removedEvent) {
                  assert.equal(removedEvent.isDeleted, true);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /event/hard', function () {
    it('should hard delete 1 event ', function (done) {
      Event.findOne({title: 'Updated Title'})
        .then(function (event) {
           request
            .delete('/event/hard')
            .query({_id: event._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Event.findOne({_id: event._id})
                .then(function (removedEvent) {
                  assert.equal(removedEvent, null);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

});