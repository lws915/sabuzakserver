'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

var assert = require('assert');
var request = require('supertest')('http://localhost:3000');
var bootstrapper = require('../../bootstrap.test.js');


describe('User Controller', function () {
  before(function (done) {
    var photos = [
      {
        tags: ["MAIN", "PROFILE"]
      },
      {
        tags: ["PROFILE"]
      },
      {
        tags: ["MAIN"]
      },
      {
        tags: ["GALLERY"]
      },
      {
        tags: ["MENU"]
      },
    ];

    bootstrapper.testBefore()
      .then(() => {
        return Photo.create(photos);
      })
      .then((photos) => {


        var users = [
          {
            username: "user1",
            email: "user1@applicat.co.kr",
            nickname: "user1",
            password: "1234",
            photos: [photos[0], photos[1]]
          },
          {
            username: "user2",
            email: "user2@applicat.co.kr",
            nickname: "user2",
            password: "12345",
            photos: [photos[2], photos[3]]
          },
          {
            username: "user3",
            email: "user3@applicat.co.kr",
            nickname: "user3",
            password: "123456",
            photos: [photos[4]]
          },
          {
            username: "user4",
            email: "user4@applicat.co.kr",
            nickname: "user4",
            password: "1234567"
          },
          {
            username: "user5",
            email: "user5@applicat.co.kr",
            nickname: "user5",
            password: "12345678"
          },
          {
            username: "user6",
            email: "user6@applicat.co.kr",
            nickname: "user6",
            password: "123456789"
          },
        ];
        var promises = [];
        _.forEach(users, function (user) {
          promises.push(User.register(user));
        });
        return promises;
      })
      .spread(function () {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  after(function (done) {
    Promise.all([
      User.remove(),
    ])
      .spread(function () {
        return bootstrapper.testAfter();
      })
      .then(function () {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  it('should exist', function (done) {
    assert.ok(AppZet.controllers.UserController);
    assert.ok(global.User);
    done();
  });

  describe('GET /user/count', function () {
    it('should find all 6 users', function (done) {
      request
        .get('/user/count')
        .set('Accept', 'application/json')
        .expect(200, {count: 6}, done);
    });

    it('should find only 1 {"query": {"nickname":"user2"}}', function (done) {
      request
        .get('/user/count')
        .set('Accept', 'application/json')
        .query({query: '{"nickname": "user2" }'})
        .expect(200, {count: 1}, done);
    });
  });

  describe('GET /user/find', function () {
    it('should find all 6 users', function (done) {
      request
        .get('/user/find')
        .set('Accept', 'application/json')
        .expect(200)
        .expect(function (res) {

          assert.equal(res.body.users.length, 6);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 4 user in query({"nickname": {"$in": ["user1", "user5", "user7"] } })', function (done) {
      request
        .get('/user/find')
        .set('Accept', 'application/json')
        .query({query: '{"nickname":  {"$in": ["user1", "user5", "user7"] } }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.users.length, 2);
          assert.equal(res.body.total, 2);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find user with limit 3', function (done) {
      request
        .get('/user/find')
        .set('Accept', 'application/json')
        .query({limit: 3})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.users.length, 3);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, true);

        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find user with skip 2', function (done) {
      request
        .get('/user/find')
        .set('Accept', 'application/json')
        .query({skip: 2})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.users.length, 4);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find user with sort', function (done) {
      request
        .get('/user/find')
        .set('Accept', 'application/json')
        .query({sort: '{"nickname": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.users.length, 6);
          assert.equal(res.body.users[0].nickname, "user1");
          assert.equal(res.body.users[1].nickname, "user2");
          assert.equal(res.body.users[2].nickname, "user3");
          assert.equal(res.body.users[3].nickname, "user4");
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find user with populate', function (done) {
      request
        .get('/user/find')
        .set('Accept', 'application/json')
        .query({populate: 'photos', sort: '{"nickname": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.users.length, 6);
          assert.equal(res.body.users[0].nickname, "user1");
          assert.equal(res.body.users[0].photos.length, 2);
          assert.equal(res.body.users[1].nickname, "user2");
          assert.equal(res.body.users[1].photos.length, 2);
          assert.equal(res.body.users[1].photos[0] instanceof Object, true);
          assert.equal(res.body.users[2].nickname, "user3");
          assert.equal(res.body.users[2].photos.length, 1);
          assert.equal(res.body.users[3].nickname, "user4");
          assert.equal(res.body.users[3].photos.length, 0);
          assert.equal(res.body.total, 6);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });


  describe('GET /user/findOne', function () {


    it('should find first 1 user matching ({"nickname": "user2"})', function (done) {
      request
        .get('/user/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"nickname": "user2"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.user.email, "user2@applicat.co.kr");
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 1 user matching ({"nickname": "user2"}) with populate', function (done) {
      request
        .get('/user/findOne')
        .set('Accept', 'application/json')
        .query({populate: 'photos', query: '{"nickname": "user2"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.user.email, "user2@applicat.co.kr");
          assert.equal(res.body.user.photos.length, 2);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });

  describe('POST /user', function () {
    it('should create 1 user ', function (done) {
      request
        .post('/user')
        .send({
          username: "user7",
          email: "user7@applicat.co.kr",
          nickname: "user7",
          password: "12345678910"
        })
        .expect(200)
        .expect(function (res) {

          // Check new document
          assert.notEqual(res.body.user._id, null);
          assert.equal(res.body.user.username, "user7");
          assert.equal(res.body.user.email, "user7@applicat.co.kr");
          assert.equal(res.body.user.nickname, "user7");
        })
        .end(function (err, res) {
          if (err) return done(err);
          // Check document count
          User.count()
            .then(function (count) {
              assert.equal(count, 7);
              done();
            })
            .catch(function (err) {
              done(err);
            });

        });
    });
  });

  describe('PUT /user', function () {

    it('should update 1 user ', function (done) {
      User.findOne({nickname: 'user7'})
        .then(function (user) {
          request
            .put('/user')
            .send({
              _id: user._id,
              nickname: "uppdatedUser7",
            })
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              User.findOne({_id: user._id})
                .then(function (updatedPost) {
                  assert.equal(updatedPost._id, user._id);
                  assert.equal(updatedPost.nickname, "uppdatedUser7");
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /user', function () {
    it('should soft delete 1 user ', function (done) {
      User.findOne({nickname: 'uppdatedUser7'})
        .then(function (user) {
          request
            .delete('/user')
            .query({_id: user._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              User.findOne({_id: user._id})
                .then(function (removedUser) {
                  assert.equal(removedUser.isDeleted, true);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /user/hard', function () {
    it('should hard delete 1 user ', function (done) {
      User.findOne({nickname: 'user1'})
        .then(function (user) {
          request
            .delete('/user/hard')
            .query({_id: user._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);

              // Check documents removed
              Promise.all([User.findOne({_id: user._id}), Photo.find({_id: {$in: user.photos}})])
                .spread(function (removedUser, removedPhotos) {
                  assert.equal(removedUser, null);
                  assert.equal(removedPhotos.length, 0);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });
});
