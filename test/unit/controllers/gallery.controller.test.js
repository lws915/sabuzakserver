var assert = require('assert');
var request = require('supertest')('http://localhost:3000');
var bootstrapper = require('../../bootstrap.test.js');

describe('Gallery Controller', function () {

  before(function (done) {
    // Create test data before hand

    var photos = [
      {
        url: 'naver.com'
      }, {
        url: 'google.com'
      }
    ];

    bootstrapper.testBefore()
      .then(() => {
        return Promise.all([Photo.create(photos)]);
      })
      .spread(function (photos) {

        var galleries = [
          {
            description: '1안녕하세요. 갤러리 첫번째 사진입니다.',
            instagram: false,
          },
          {
            description: '2안녕하세요. 갤러리 두번째 사진입니다.',
            instagram: true
          },
          {
            description: '3안녕하세요. 세번째 갤러리 사진입니다.',
            instagram: false,
            photos: photos[0]
          },
          {
            description: '4안녕하세요. 네번째 갤러리 사진입니다.',
            instagram: true,
            photos: photos[1]
          },
        ];

        return Gallery.create(galleries);
      })
      .then(function (galleries) {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  after(function (done) {
    Promise.all([
      Photo.remove(),
      Gallery.remove()
    ])
      .spread(function () {
        return bootstrapper.testAfter();
      })
      .then(function () {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  it('should exist', function (done) {
    assert.ok(AppZet.controllers.GalleryController);
    assert.ok(global.Gallery);
    done();
  });

  describe('GET /gallery/count', function () {

    it('should count all 4 gallery', function (done) {
      request
        .get('/gallery/count')
        .set('Accept', 'application/json')
        .expect(200, {count: 4}, done);
    });

  });
  describe('GET /gallery/find', function () {

    it('should find all 4 galleries', function (done) {
      request
        .get('/gallery/find')
        .set('Accept', 'application/json')
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.galleries.length, 4);
          assert.equal(res.body.total, 4);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find gallery with limit 3', function (done) {
      request
        .get('/gallery/find')
        .set('Accept', 'application/json')
        .query({limit: 3})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.galleries.length, 3);
          assert.equal(res.body.total, 4);
          assert.equal(res.body.more, true);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find galleries with skip 2', function (done) {
      request
        .get('/gallery/find')
        .set('Accept', 'application/json')
        .query({skip: 2})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.galleries.length, 2);
          assert.equal(res.body.total, 4);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find galleries with populate', function (done) {
      request
        .get('/gallery/find')
        .set('Accept', 'application/json')
        .query({populate: 'photos', sort: {description: 1}})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.galleries.length, 4);
          assert.equal(res.body.galleries[0].instagram, false);
          assert.equal(res.body.galleries[0].photos.length, 0);
          assert.equal(res.body.galleries[1].instagram, true);
          assert.equal(res.body.galleries[1].photos.length, 0);
          assert.equal(res.body.galleries[2].instagram, false);
          assert.equal(res.body.galleries[2].photos.length, 1);
          assert.equal(res.body.galleries[3].instagram, true);
          assert.equal(res.body.galleries[3].photos[0] instanceof Object, true);
          assert.equal(res.body.total, 4);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });


  describe('GET /gallery/findOne', function () {

    it('should find first 1 gallery matching ({"description": "1안녕하세요. 갤러리 첫번째 사진입니다."}) views++', function (done) {
      request
        .get('/gallery/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"description": "1안녕하세요. 갤러리 첫번째 사진입니다."}'})
        .expect(200)
        .expect(function (res) {
          assert.notEqual(res.body.gallery, null);
          assert.equal(res.body.gallery.views, 1);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find first 1 gallery matching ({"description": "1안녕하세요. 갤러리 첫번째 사진입니다."}) views++', function (done) {
      request
        .get('/gallery/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"description": "1안녕하세요. 갤러리 첫번째 사진입니다."}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.gallery.description, "1안녕하세요. 갤러리 첫번째 사진입니다.");
          assert.equal(res.body.gallery.views, 2);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 1 gallery matching ({"description": "3안녕하세요. 세번째 갤러리 사진입니다."}) with populate', function (done) {
      request
        .get('/gallery/findOne')
        .set('Accept', 'application/json')
        .query({populate: 'photos', query: '{"description": "3안녕하세요. 세번째 갤러리 사진입니다."}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.gallery.description, "3안녕하세요. 세번째 갤러리 사진입니다.");
          assert.equal(res.body.gallery.photos.length, 1);
          assert.equal(res.body.gallery.photos[0] instanceof Object, true);
          assert.equal(res.body.gallery.views, 1);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });


  });

  describe('POST /gallery', function () {
    it('should create 1 gallery ', function (done) {
      request
        .post('/gallery')
        .send({
          description: '5안녕하세요. 다섯번째 갤러리 입니다.',
          instagram: true
        })
        .expect(200)
        .expect(function (res) {
          assert.notEqual(res.body.gallery._id, null);
          assert.equal(res.body.gallery.description, "5안녕하세요. 다섯번째 갤러리 입니다.");
          assert.equal(res.body.gallery.instagram, true);
        })
        .end(function (err, res) {
          if (err) return done(err);
          // Check document count
          Gallery.find()
            .then(function (galleries) {
              assert.equal(galleries.length, 5);
              done();
            })
            .catch(function (err) {
              done(err);
            });

        });
    });
  });
  describe('PUT /gallery', function () {
    it('should update 1 gallery ', function (done) {
      var galleryId = '';
      request
        .get('/gallery/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"description": "3안녕하세요. 세번째 갤러리 사진입니다."}'})
        .expect(200)
        .end(function(err,res){
          galleryId = res.body.gallery._id;
          request
            .put('/gallery')
            .send({
              _id: res.body.gallery._id,
              description: "3안녕하세요. 수정된 세번째 갤러리 사진입니다.",
              instagram: true,
              photos: null
            })
            .expect(200)
            .end(function(err, res){
              request
                .get('/gallery/findOne')
                .set('Accept', 'application/json')
                .query({
                  query: '{"_id": '+galleryId+'}',
                  populate:'photos'
                })
                .expect(200)
                .end(function(err,res){
                  assert.equal(res.body.gallery._id, galleryId);
                  assert.equal(res.body.gallery.description, "3안녕하세요. 수정된 세번째 갤러리 사진입니다.");
                  assert.equal(res.body.gallery.instagram, true);
                  done();
                })
            })
        });
    })
  });

  describe('DELETE /gallery/hard', function () {
    it('should hard delete 1 gallery ', function (done) {
      Gallery.findOne({description: '3안녕하세요. 수정된 세번째 갤러리 사진입니다.'})
        .then(function (gallery) {
          request
            .delete('/gallery/hard')
            .send({_id:gallery._id})
            // .query({query: '{"_id": '+gallery._id+'}'})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Gallery.findOne({_id: gallery._id})
                .then(function (removedGallery) {
                  assert.equal(removedGallery, null);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });


});
