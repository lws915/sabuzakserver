'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

var assert = require('assert');
var request = require('supertest')('http://localhost:3000');
var bootstrapper = require('../../bootstrap.test.js');


describe('Post Controller', function () {

  before(function (done) {
    // Create test data before hand


    var photos = [
      {
        tags: ["MAIN", "PROFILE"]
      },
      {
        tags: ["PROFILE"]
      },
      {
        tags: ["MAIN"]
      },
      {
        tags: ["GALLERY"]
      },
      {
        tags: ["MENU"]
      },
    ];

    bootstrapper.testBefore()
      .then(() => {
        return Photo.create(photos);
      })
      .then(function (photos) {

        var posts = [
          {
            category: "NOTICE",
            title: "공지사항",
            content: "공지사항 내용",
            photos: [photos[0], photos[1]]
          },
          {
            category: "EVENT",
            title: "이벤트",
            content: "이벤트 내용",
            photos: [photos[2], photos[3]]
          },
          {
            category: "NOTICE",
            title: "공지사항2",
            content: "공지사항2 내용",
            photos: [photos[4]]
          },
          {
            category: "QUICK",
            title: "번개톡",
            content: "번개톡 내용",
          },
          {
            category: "NOTICE",
            title: "공지사항3",
            content: "공지사항3 내용",
          },
        ];

        return Post.create(posts);
      })
      .then(function (posts) {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  after(function (done) {
    Promise.all([
        Photo.remove(),
        Post.remove(),
      ])
      .spread(function () {
        return bootstrapper.testAfter();
      })
      .then(function () {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  it('should exist', function (done) {
    assert.ok(AppZet.controllers.PostController);
    assert.ok(global.Post);
    done();
  });

  describe('GET /post/count', function () {
    it('should find all 5 posts', function (done) {
      request
        .get('/post/count')
        .set('Accept', 'application/json')
        .expect(200, {count: 5}, done);
    });

    it('should find only 1 {"query": {"category":"NOTICE"}}', function (done) {
      request
        .get('/post/count')
        .set('Accept', 'application/json')
        .query({query: '{"category": "NOTICE" }'})
        .expect(200, {count: 3}, done);
    });
  });

  describe('GET /post/find', function () {
    it('should find all 5 posts', function (done) {
      request
        .get('/post/find')
        .set('Accept', 'application/json')
        .expect(200)
        .expect(function (res) {

          assert.equal(res.body.posts.length, 5);
          assert.equal(res.body.total, 5);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 4 post in query({"category": {"$in": ["NOTICE", "EVENT"] } })', function (done) {
      request
        .get('/post/find')
        .set('Accept', 'application/json')
        .query({query: '{"category": {"$in": ["NOTICE", "EVENT"] } }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.posts.length, 4);
          assert.equal(res.body.total, 4);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find post with limit 3', function (done) {
      request
        .get('/post/find')
        .set('Accept', 'application/json')
        .query({limit: 3})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.posts.length, 3);
          assert.equal(res.body.total, 5);
          assert.equal(res.body.more, true);

        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with skip 2', function (done) {
      request
        .get('/post/find')
        .set('Accept', 'application/json')
        .query({skip: 2})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.posts.length, 3);
          assert.equal(res.body.total, 5);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with sort', function (done) {
      request
        .get('/post/find')
        .set('Accept', 'application/json')
        .query({sort: '{"title": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.posts.length, 5);
          assert.equal(res.body.posts[0].title, "공지사항");
          assert.equal(res.body.posts[1].title, "공지사항2");
          assert.equal(res.body.posts[2].title, "공지사항3");
          assert.equal(res.body.posts[3].title, "번개톡");
          assert.equal(res.body.total, 5);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with populate', function (done) {
      request
        .get('/post/find')
        .set('Accept', 'application/json')
        .query({populate: 'photos', sort: '{"title": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.posts.length, 5);
          assert.equal(res.body.posts[0].title, "공지사항");
          assert.equal(res.body.posts[0].photos.length, 2);
          assert.equal(res.body.posts[1].title, "공지사항2");
          assert.equal(res.body.posts[1].photos.length, 1);
          assert.equal(res.body.posts[1].photos[0] instanceof Object, true);
          assert.equal(res.body.posts[2].title, "공지사항3");
          assert.equal(res.body.posts[2].photos.length, 0);
          assert.equal(res.body.posts[3].title, "번개톡");
          assert.equal(res.body.posts[3].photos.length, 0);
          assert.equal(res.body.total, 5);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });


  describe('GET /post/findOne', function () {

    it('should find first 1 post matching ({"title": "공지사항"}) views++', function (done) {
      request
        .get('/post/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"title": "공지사항"}'})
        .expect(200)
        .expect(function (res) {
          assert.notEqual(res.body.post, null);
          assert.equal(res.body.isLikable, true);
          assert.equal(res.body.post.views, 1);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find first 1 post matching ({"title": "공지사항"}) views++', function (done) {
      request
        .get('/post/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"title": "공지사항"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.post.title, "공지사항");
          assert.equal(res.body.isLikable, true);
          assert.equal(res.body.post.views, 2);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 1 post matching ({"title": "공지사항"}) with populate', function (done) {
      request
        .get('/post/findOne')
        .set('Accept', 'application/json')
        .query({populate: 'photos', query: '{"title": "공지사항"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.post.title, "공지사항");
          assert.equal(res.body.post.photos.length, 2);
          assert.equal(res.body.isLikable, true);
          assert.equal(res.body.post.views, 3);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });

  describe('POST /post', function () {
    it('should create 1 post ', function (done) {
      request
        .post('/post')
        .send({
          category: "NEW_CATEGORY",
          title: "New Title",
          content: "New Content",
        })
        .expect(200)
        .expect(function (res) {

          // Check new document
          assert.notEqual(res.body.post._id, null);
          assert.equal(res.body.post.category, "NEW_CATEGORY");
          assert.equal(res.body.post.title, "New Title");
          assert.equal(res.body.post.content, "New Content");
        })
        .end(function (err, res) {
          if (err) return done(err);
          // Check document count
          Post.find()
            .then(function (posts) {
              assert.equal(posts.length, 6);
              done();
            })
            .catch(function (err) {
              done(err);
            });

        });
    });
  });

  describe('PUT /post', function () {

    it('should update 1 post ', function (done) {
      Post.findOne({title: 'New Title'})
        .then(function (post) {
          request
            .put('/post')
            .send({
              _id: post._id,
              category: "UPDATED_CATEGORY",
              title: "Updated Title",
              content: "Updated Content",
            })
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Post.findOne({_id: post._id})
                .then(function (updatedPost) {
                  assert.equal(updatedPost._id, post._id);
                  assert.equal(updatedPost.category, "UPDATED_CATEGORY");
                  assert.equal(updatedPost.title, "Updated Title");
                  assert.equal(updatedPost.content, "Updated Content");
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /post', function () {
    it('should soft delete 1 post ', function (done) {
      Post.findOne({title: 'Updated Title'})
        .then(function (post) {
          request
            .delete('/post')
            .query({_id: post._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Post.findOne({_id: post._id})
                .then(function (removedPost) {
                  assert.equal(removedPost.isDeleted, true);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /post/hard', function () {
    it('should hard delete 1 post ', function (done) {
      Post.findOne({title: 'Updated Title'})
        .then(function (post) {
          request
            .delete('/post/hard')
            .query({_id: post._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Post.findOne({_id: post._id})
                .then(function (removedPost) {
                  assert.equal(removedPost, null);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });


});
