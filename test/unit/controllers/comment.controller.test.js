'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

var assert = require('assert');
var request = require('supertest')('http://localhost:3000');
var bootstrapper = require('../../bootstrap.test.js');

describe('Comment Controller', function () {

  before(function (done) {
    // Create test data before hand


    var posts = [
      {
        category: "NOTICE",
        title: "공지사항",
        content: "공지사항 내용",
      },
      {
        category: "EVENT",
        title: "이벤트",
        content: "이벤트 내용",
      },
      {
        category: "NOTICE",
        title: "공지사항2",
        content: "공지사항2 내용",
      },
      {
        category: "QUICK",
        title: "번개톡",
        content: "번개톡 내용",
      },
      {
        category: "NOTICE",
        title: "공지사항3",
        content: "공지사항3 내용",
      },
    ];

    var questionnaires = [
      {
        category: "QUESTIONNAIRE",
        name: "questionnaire name1"
      }
    ];

    var questionnaireGroups = [
      {
        category: "QUESTIONNAIRE_GROUP",
        name: "questionnaire group name1"
      }
    ];

    bootstrapper.testBefore()
      .then(() => {
        return Questionnaire.create(questionnaires);
      })
      .then(function (questionnaires) {
        return QuestionnaireGroup.create(questionnaireGroups)
      })
      .then(function (questionnaireGroups) {
        return Post.create(posts)
      })
      .then(function (posts) {

        var comments = [
          {
            content: "댓글 1",
            post: posts[0]
          },
          {
            content: "댓글 2",
            post: posts[1]
          },
          {
            content: "댓글 3",
            post: posts[2]
          },
          {
            content: "댓글 4",
            post: posts[3]
          },
          {
            content: "댓글 5",
            post: posts[4]
          },
        ];

        return Comment.create(comments);
      })
      .then(function (comments) {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  after(function (done) {
    Promise.all([
        Post.remove(),
        Questionnaire.remove(),
        QuestionnaireGroup.remove(),
        Comment.remove()
      ])
      .spread(function () {
        return bootstrapper.testAfter();
      })
      .then(function(){
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  it('should exist', function (done) {
    assert.ok(AppZet.controllers.CommentController);
    assert.ok(global.Comment);
    done();
  });

  describe('GET /comment/count', function () {

    it('should find all 5 comments', function (done) {
      request
        .get('/comment/count')
        .set('Accept', 'application/json')
        .expect(200, {count: 5}, done);
    });

    it('should find only 1 {"query": {"category":"NOTICE"}}', function (done) {
      request
        .get('/comment/count')
        .set('Accept', 'application/json')
        .query({query: '{"content": "댓글 3" }'})
        .expect(200, {count: 1}, done);
    });

  });

  describe('GET /comment/find', function () {

    it('should find all 5 comments', function (done) {
      request
        .get('/comment/find')
        .set('Accept', 'application/json')
        .expect(200)
        .expect(function (res) {

          assert.equal(res.body.comments.length, 5);
          assert.equal(res.body.total, 5);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 1 comment in query({"content": "댓글 1" })', function (done) {
      request
        .get('/comment/find')
        .set('Accept', 'application/json')
        .query({query: '{"content": "댓글 1" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.comments.length, 1);
          assert.equal(res.body.total, 1);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find comment with limit 3', function (done) {
      request
        .get('/comment/find')
        .set('Accept', 'application/json')
        .query({limit: 3})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.comments.length, 3);
          assert.equal(res.body.total, 5);
          assert.equal(res.body.more, true);

        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with skip 2', function (done) {
      request
        .get('/comment/find')
        .set('Accept', 'application/json')
        .query({skip: 2})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.comments.length, 3);
          assert.equal(res.body.total, 5);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with sort', function (done) {
      request
        .get('/comment/find')
        .set('Accept', 'application/json')
        .query({sort: '{"content": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.comments.length, 5);
          assert.equal(res.body.comments[0].content, "댓글 1");
          assert.equal(res.body.comments[1].content, "댓글 2");
          assert.equal(res.body.comments[2].content, "댓글 3");
          assert.equal(res.body.comments[3].content, "댓글 4");
          assert.equal(res.body.total, 5);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with populate', function (done) {
      request
        .get('/comment/find')
        .set('Accept', 'application/json')
        .query({populate: 'post', sort: '{"content": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.comments.length, 5);
          assert.equal(res.body.comments[0].content, "댓글 1");
          assert.equal(res.body.comments[0].post.title, "공지사항");
          assert.equal(res.body.comments[1].content, "댓글 2");
          assert.equal(res.body.comments[1].post.title, "이벤트");
          assert.equal(res.body.comments[2].content, "댓글 3");
          assert.equal(res.body.comments[2].post.title, "공지사항2");
          assert.equal(res.body.comments[3].content, "댓글 4");
          assert.equal(res.body.comments[3].post.title, "번개톡");
          assert.equal(res.body.total, 5);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });


  describe('GET /comment/findOne', function () {

    it('should find first 1 comment matching ({"content": "댓글 1"}) views++', function (done) {
      request
        .get('/comment/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"content": "댓글 1"}'})
        .expect(200)
        .expect(function (res) {
          assert.notEqual(res.body.comment, null);
          assert.equal(res.body.isLikable, true);
          assert.equal(res.body.comment.views, 1);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find first 1 comment matching ({"content": "댓글 1"}) views++', function (done) {
      request
        .get('/comment/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"content": "댓글 1"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.comment.content, "댓글 1");
          assert.equal(res.body.isLikable, true);
          assert.equal(res.body.comment.views, 2);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 1 comment matching ({"content": "댓글 1"}) with populate', function (done) {
      request
        .get('/comment/findOne')
        .set('Accept', 'application/json')
        .query({populate: 'post', query: '{"content": "댓글 1"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.comment.content, "댓글 1");
          assert.equal(res.body.comment.post.title, "공지사항");
          assert.equal(res.body.isLikable, true);
          assert.equal(res.body.comment.views, 3);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });

  describe('POST /comment/childComment', function () {
    it('should create 1 child comment ', function (done) {
      request
        .post('/comment/childComment')
        .send({
          content: "child comment",
          parent: 1,
        })
        .expect(200)
        .expect(function (res) {
          // Check new document
          assert.notEqual(res.body.comment._id, null);
          assert.equal(res.body.comment.content, "child comment");
          assert.equal(res.body.comment.parent, 1);

        })
        .end(function (err, res) {
          if (err) return done(err);

          Comment.findOne({_id: 1})
            .then(function (comment) {
              assert.equal(comment.commentsCount, 1);
              done();
            })
            .catch(function (err) {
              done(err);
            });
        });
    });
  });

  describe('POST /comment/post', function () {
    it('should create 1 post comment ', function (done) {

      Post.findOne()
        .then(function (post) {
          request
            .post('/comment/post')
            .send({
              content: "post comment",
              post: post._id,
            })
            .expect(200)
            .expect(function (res) {
              // Check new document
              assert.notEqual(res.body.comment._id, null);
              assert.equal(res.body.comment.content, "post comment");
              assert.equal(res.body.comment.post, post._id);

            })
            .end(function (err, res) {
              if (err) return done(err);

              Post.findOne({_id: post._id})
                .then(function (post) {
                  assert.equal(post.commentsCount, 1);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('POST /comment/questionnaire', function () {
    it('should create 1 questionnaire comment ', function (done) {

      Questionnaire.findOne()
        .then(function (questionnaire) {
          request
            .post('/comment/questionnaire')
            .send({
              content: "questionnaire comment",
              questionnaire: questionnaire._id,
            })
            .expect(200)
            .expect(function (res) {
              // Check new document
              assert.notEqual(res.body.comment._id, null);
              assert.equal(res.body.comment.content, "questionnaire comment");
              assert.equal(res.body.comment.questionnaire, questionnaire._id);

            })
            .end(function (err, res) {
              if (err) return done(err);

              Questionnaire.findOne({_id: questionnaire._id})
                .then(function (questionnaire) {
                  assert.equal(questionnaire.commentsCount, 1);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('POST /comment/questionnaireGroup', function () {
    it('should create 1 questionnaireGroup comment ', function (done) {

      QuestionnaireGroup.findOne()
        .then(function (questionnaireGroup) {
          request
            .post('/comment/questionnaireGroup')
            .send({
              content: "questionnaireGroup comment",
              questionnaireGroup: questionnaireGroup._id,
            })
            .expect(200)
            .expect(function (res) {
              // Check new document
              assert.notEqual(res.body.comment._id, null);
              assert.equal(res.body.comment.content, "questionnaireGroup comment");
              assert.equal(res.body.comment.questionnaireGroup, questionnaireGroup._id);

            })
            .end(function (err, res) {
              if (err) return done(err);

              QuestionnaireGroup.findOne({_id: questionnaireGroup._id})
                .then(function (questionnaireGroup) {
                  assert.equal(questionnaireGroup.commentsCount, 1);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('PUT /comment', function () {
    it('should update 1 comment ', function (done) {
      request
        .put('/comment')
        .send({
          _id: 1,
          content: "Updated Comment",
        })
        .expect(200)
        .end(function (err, res) {
          if (err) return done(err);

          Comment.findOne({_id: 1})
            .then(function (comment) {
              assert.equal(comment._id, 1);
              assert.equal(comment.content, "Updated Comment");
              done();
            })
            .catch(function (err) {
              done(err);
            });
        });
    });
  });

  describe('DELETE /comment', function () {
    it('should soft delete 1 comment ', function (done) {
      Comment.findOne({content: 'Updated Comment'})
        .then(function (comment) {
          request
            .delete('/comment')
            .query({_id: comment._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Comment.findOne({_id: comment._id})
                .then(function (removedComment) {
                  assert.equal(removedComment.isDeleted, true);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /comment/hard', function () {
    it('should hard delete 1 comment ', function (done) {
      Comment.findOne({content: 'child comment'})
        .then(function (comment) {
          request
            .delete('/comment/hard')
            .query({_id: comment._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Comment.findOne({_id: 1})
                .then(function (parentComment) {
                  assert.equal(parentComment.commentsCount, 0);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });
});
