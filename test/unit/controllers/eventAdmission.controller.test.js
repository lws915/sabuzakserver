/**
 * Created by Philip on 11/08/2016
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Philip MaengKwan Seo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Philip MaengKwan Seo <developer@applicat.co.kr>, 11/08/2016
 *
 */

'use strict';

var assert = require('assert');
var request = require('supertest')('http://localhost:3000');
var bootstrapper = require('../../bootstrap.test.js');

describe('EventAdmission Controller', function () {

  before(function (done) {
    // Create test data before hand
    var today = new Date();

    var events = [
      {
        category: "NORMAL",
        title: "일반 이벤트1",
        content: "이벤트 내용1",
        startDate: today,
        endDate: new Date(today.setDate(today.getDate() + 1))
      },
      {
        category: "NORMAL",
        title: "일반 이벤트2",
        content: "이벤트 내용2",
        startDate: today,
        endDate: new Date(today.setDate(today.getDate() + 1))
      },
      {
        category: "NORMAL",
        title: "일반 이벤트3",
        content: "이벤트 내용3",
        startDate: today,
        endDate: new Date(today.setDate(today.getDate() + 1))
      },
      {
        category: "NORMAL",
        title: "일반 이벤트4",
        content: "이벤트 내용4",
        startDate: today,
        endDate: new Date(today.setDate(today.getDate() + 1))
      },
      {
        category: "NORMAL",
        title: "일반 이벤트5",
        content: "이벤트 내용5",
        startDate: today,
        endDate: new Date(today.setDate(today.getDate() + 1))
      }
    ];

    bootstrapper.testBefore()
      .then(() => {
        return Event.create(events);
      })
      .then(function (events) {

        var eventAdmissions = [
          {
            name: "응모자1",
            phone: "01012345678",
            email: "admission@test.co.kr",
            event: events[0]
          },
          {
            name: "응모자2",
            phone: "01012345678",
            email: "admission@test.co.kr",
            event: events[1]
          },
          {
            name: "응모자3",
            phone: "01012345678",
            email: "admission@test.co.kr",
            event: events[2]
          },
          {
            name: "응모자4",
            phone: "01012345678",
            email: "admission@test.co.kr",
            event: events[3]
          },
          {
            name: "응모자5",
            phone: "01012345678",
            email: "admission@test.co.kr",
            event: events[4]
          }
        ];

        return EventAdmission.create(eventAdmissions);
      })
      .then(function (eventAdmissions) {
        done();
      })
      .catch(function (err) {
        done(err);
      });

  });

  after(function (done) {
    Promise.all([
      Event.remove(),
      EventAdmission.remove(),
    ])
      .spread(function () {
        return bootstrapper.testAfter();
      })
      .then(function () {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  it('should exist', function (done) {
    assert.ok(AppZet.controllers.EventAdmissionController);
    assert.ok(global.EventAdmission);
    done();
  });

  describe('GET /eventAdmission/count', function () {

    it('should find all 5 eventAdmissions', function (done) {
      request
        .get('/eventAdmission/count')
        .set('Accept', 'application/json')
        .expect(200, {count: 5}, done);
    });

  });

  describe('GET /eventAdmission/find', function () {

    it('should find all 5 eventAdmissions', function (done) {
      request
        .get('/eventAdmission/find')
        .set('Accept', 'application/json')
        .expect(200)
        .expect(function (res) {

          assert.equal(res.body.eventAdmissions.length, 5);
          assert.equal(res.body.total, 5);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find only 1 {"query": {"name": "응모자1"}}', function (done) {
      request
        .get('/eventAdmission/find')
        .set('Accept', 'application/json')
        .query({query: '{"name": "응모자1" }'})
        .expect(function (res) {
          assert.equal(res.body.eventAdmissions.length, 1);
          assert.equal(res.body.total, 1);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find event with limit 3', function (done) {
      request
        .get('/eventAdmission/find')
        .set('Accept', 'application/json')
        .query({limit: 3})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.eventAdmissions.length, 3);
          assert.equal(res.body.total, 5);
          assert.equal(res.body.more, true);

        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with skip 2', function (done) {
      request
        .get('/eventAdmission/find')
        .set('Accept', 'application/json')
        .query({skip: 2})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.eventAdmissions.length, 3);
          assert.equal(res.body.total, 5);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with sort', function (done) {
      request
        .get('/eventAdmission/find')
        .set('Accept', 'application/json')
        .query({sort: '{"name": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.eventAdmissions.length, 5);
          assert.equal(res.body.eventAdmissions[0].name, "응모자1");
          assert.equal(res.body.eventAdmissions[1].name, "응모자2");
          assert.equal(res.body.eventAdmissions[2].name, "응모자3");
          assert.equal(res.body.eventAdmissions[3].name, "응모자4");
          assert.equal(res.body.total, 5);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find applications with populate', function (done) {
      request
        .get('/eventAdmission/find')
        .set('Accept', 'application/json')
        .query({populate: 'event', sort: '{"name": "asc" }'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.eventAdmissions.length, 5);
          assert.equal(res.body.eventAdmissions[0].name, "응모자1");
          assert.equal(res.body.eventAdmissions[0].event.title, "일반 이벤트1");
          assert.equal(res.body.eventAdmissions[1].name, "응모자2");
          assert.equal(res.body.eventAdmissions[1].event.title, "일반 이벤트2");
          assert.equal(res.body.eventAdmissions[2].name, "응모자3");
          assert.equal(res.body.eventAdmissions[2].event.title, "일반 이벤트3");
          assert.equal(res.body.eventAdmissions[3].name, "응모자4");
          assert.equal(res.body.eventAdmissions[3].event.title, "일반 이벤트4");
          assert.equal(res.body.total, 5);
          assert.equal(res.body.more, false);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });

  describe('GET /eventAdmission/findOne', function () {

    it('should find first 1 eventAdmission matching ({"name": "응모자1"}) views++', function (done) {
      request
        .get('/eventAdmission/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"name": "응모자1"}'})
        .expect(200)
        .expect(function (res) {
          assert.notEqual(res.body.eventAdmission, null);
          assert.equal(res.body.eventAdmission.views, 1);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find first 1 eventAdmission matching ({"name": "응모자1"}) views++', function (done) {
      request
        .get('/eventAdmission/findOne')
        .set('Accept', 'application/json')
        .query({query: '{"name": "응모자1"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.eventAdmission.name, "응모자1");
          assert.equal(res.body.eventAdmission.views, 2);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 1 eventAdmission matching ({"name": "응모자1"}) with populate', function (done) {
      request
        .get('/eventAdmission/findOne')
        .set('Accept', 'application/json')
        .query({populate: 'event', query: '{"name": "응모자1"}'})
        .expect(200)
        .expect(function (res) {
          assert.equal(res.body.eventAdmission.name, "응모자1");
          assert.equal(res.body.eventAdmission.event.title, "일반 이벤트1");
          assert.equal(res.body.eventAdmission.views, 3);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

  });

  describe('POST /eventAdmission', function () {
    it('should create 1 eventAdmission ', function (done) {
      Event.findOne()
        .then(function (event) {
          request
            .post('/eventAdmission')
            .send({
              name: "New Name",
              phone: "01012345678",
              email: "newMail@test.co.kr",
              event: event
            })
            .expect(200)
            .expect(function (res) {

              // Check new document
              assert.notEqual(res.body.eventAdmission._id, null);
              assert.equal(res.body.eventAdmission.name, "New Name");
              assert.equal(res.body.eventAdmission.phone, "01012345678");
              assert.equal(res.body.eventAdmission.email, "newMail@test.co.kr");
            })
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              EventAdmission.find()
                .then(function (eventAdmissions) {
                  assert.equal(eventAdmissions.length, 6);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });

            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('PUT /eventAdmission', function () {

    it('should update 1 eventAdmission ', function (done) {
      EventAdmission.findOne({name: 'New Name'})
        .then(function (eventAdmission) {
          request
            .put('/eventAdmission')
            .send({
              _id: eventAdmission._id,
              name: "Updated Name",
              phone: "01087654321",
              email: "updatedMail@test.co.kr",
              event: eventAdmission.event
            })
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              EventAdmission.findOne({_id: eventAdmission._id})
                .then(function (updatedEventAdmission) {
                  assert.equal(updatedEventAdmission._id, eventAdmission._id);
                  assert.equal(updatedEventAdmission.name, "Updated Name");
                  assert.equal(updatedEventAdmission.phone, "01087654321");
                  assert.equal(updatedEventAdmission.email, "updatedMail@test.co.kr");
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /eventAdmission', function () {
    it('should soft delete 1 eventAdmission ', function (done) {
      EventAdmission.findOne({name: 'Updated Name'})
        .then(function (eventAdmission) {
          request
            .delete('/eventAdmission')
            .query({_id: eventAdmission._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              EventAdmission.findOne({_id: eventAdmission._id})
                .then(function (removedEventAdmission) {
                  assert.equal(removedEventAdmission.isDeleted, true);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /eventAdmission/hard', function () {
    it('should hard delete 1 eventAdmission ', function (done) {
      EventAdmission.findOne({name: 'Updated Name'})
        .then(function (eventAdmission) {
          request
            .delete('/eventAdmission/hard')
            .query({_id: eventAdmission._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              EventAdmission.findOne({_id: eventAdmission._id})
                .then(function (removedEventAdmission) {
                  assert.equal(removedEventAdmission, null);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

});