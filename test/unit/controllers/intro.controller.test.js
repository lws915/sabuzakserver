'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

var assert = require('assert');
var request = require('supertest')('http://localhost:3000');
var bootstrapper = require('../../bootstrap.test.js');


describe('Intro Controller', function () {
  before(function (done) {
    var photos = [
      {
        tags: ["DETAIL"]
      },
      {
        tags: ["DETAIL"]
      },
      {
        tags: ["DETAIL"]
      },
      {
        tags: ["DETAIL"]
      },
      {
        tags: ["DETAIL"]
      },
      {
        tags: ["DETAIL"]
      }
    ];

    bootstrapper.testBefore()
      .then(() => {
        return Photo.create(photos);
      })
      .then((photos) => {
        var intro =
          {
            contentHtml: "<h2>마이치치스</h2><br /><p>저희 식당의 모든 음식의 재료들은 직접 키우고 재배한 유기농으로 안심하고 드셔도 됩니다. 더욱 건강하고 신선한 음식을 선보이겠습니다.</p>",
            phone: "0234438876",
            photos: [photos[0], photos[1], photos[2], photos[3], photos[4], photos[5]]
          }

        return Intro.create(intro);
      })
      .then(function (intros) {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  after(function (done) {
    Promise.all([
        Photo.remove(),
        Intro.remove(),
      ])
      .spread(function () {
        return bootstrapper.testAfter();
      })
      .then(function () {
        done();
      })
      .catch(function (err) {
        done(err);
      });
  });

  it('should exist', function (done) {
    assert.ok(AppZet.controllers.IntroController);
    assert.ok(global.Intro);
    done();
  });

  describe('GET /intro/findOne', function () {

    it('should findOne 1 Intro per App', function (done) {
      request
        .get('/intro/findOne')
        .set('Accept', 'application/json')
        .expect(200)
        .expect(function (res) {
          assert.notEqual(res.body.intro, null);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 1 Intro that contains "마이치치스" in contentHtml attr', function (done) {
      request
        .get('/intro/findOne')
        .set('Accept', 'application/json')
        .query({query: {contentHtml: new RegExp('마이치치스', 'i')}})
        .expect(200)
        .expect(function (res) {
          assert.notEqual(res.body.intro, null);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 1 Intro that matching phone number "0234438876"', function (done) {
      request
        .get('/intro/findOne')
        .set('Accept', 'application/json')
        .query({query: {phone: '0234438876'}})
        .expect(200)
        .expect(function (res) {
          assert.notEqual(res.body.intro, null);
          assert.equal(res.body.intro.phone, "0234438876");
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });

    it('should find 1 populated Intro that contains "마이치치스" in contentHtml attr', function (done) {
      request
        .get('/intro/findOne')
        .set('Accept', 'application/json')
        .query({populate: 'photos', query: {contentHtml: new RegExp('마이치치스', 'i')}})
        .expect(200)
        .expect(function (res) {
          assert.notEqual(res.body.intro, null);
          assert.equal(res.body.intro.photos.length, 6);
        })
        .end(function (err, res) {
          if (err) return done(err);
          done();
        });
    });
  });

  describe('POST /intro', function () {
    it('should create 1 intro ', function (done) {
      request
        .post('/intro')
        .send({
          contentHtml: "<h3>New Content</h3><p>This is test about creating</p>",
          phone: "01012345678"
        })
        .expect(200)
        .expect(function (res) {

          // Check new document
          assert.notEqual(res.body.intro._id, null);
          assert.notEqual(res.body.intro, null);
          assert.equal(res.body.intro.phone, "01012345678");
        })
        .end(function (err, res) {
          if (err) return done(err);
          // Check document count
          Intro.find()
            .then(function (intros) {
              assert.equal(intros.length, 2);
              done();
            })
            .catch(function (err) {
              done(err);
            });
        });
    });
  });

  describe('PUT /intro', function () {

    it('should update 1 intro ', function (done) {
      Intro.findOne()
        .then(function (intro) {
          request
            .put('/intro')
            .send({
              _id: intro._id,
              contentHtml: "<h3>Updated Content</h3><p>This is test about updating</p>",
              phone: "01098765432"
            })
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Intro.findOne({_id: intro._id})
                .then(function (updatedIntro) {
                  assert.equal(updatedIntro._id, intro._id);
                  assert.equal(updatedIntro.phone, "01098765432");
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /intro', function () {
    it('should soft delete 1 intro ', function (done) {
      Intro.findOne()
        .then(function (intro) {
          request
            .delete('/intro')
            .query({_id: intro._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Intro.findOne({_id: intro._id})
                .then(function (removedIntro) {
                  assert.equal(removedIntro.isDeleted, true);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

  describe('DELETE /intro/hard', function () {
    it('should hard delete 1 intro ', function (done) {
      Intro.findOne()
        .then(function (intro) {
          request
            .delete('/intro/hard')
            .query({_id: intro._id})
            .expect(200)
            .end(function (err, res) {
              if (err) return done(err);
              // Check document count
              Intro.findOne({_id: intro._id})
                .then(function (removedIntro) {
                  assert.equal(removedIntro, null);
                  done();
                })
                .catch(function (err) {
                  done(err);
                });
            });
        })
        .catch(function (err) {
          done(err);
        });
    });
  });

});
