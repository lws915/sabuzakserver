'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

var assert = require('assert');


describe('User Model', function () {

  it('should exist', function (done) {
    assert.ok(AppZet.models.User);
    assert.ok(global.User);
    done();
  });

  // TODO: need to create duplicated validation during model life cycle
  // it('should not allow same email', function (done) {
  //
  //   User.create([
  //       {
  //         email: "test@test.com",
  //         username: "test1",
  //       },
  //       {
  //         email: "test@test.com",
  //         username: "test2",
  //       }
  //     ])
  //     .then(function (users) {
  //       done();
  //     })
  //     .catch(function (err) {
  //       done(err);
  //     });
  //
  //
  // });

});
