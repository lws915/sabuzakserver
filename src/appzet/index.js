'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 *
 *
 * Script description:
 *
 *  Order of instantiation
 *    1.  AppZet - Create and Initialise server And Initialise Globals
 *               - Setup API
 *               - Setting Log
 *               - Initialise Service
 *               - Initialise Model
 *               - Initialise Policy
 *               - Initialise Response
 *               - Initialise Controller
 *               - Initialise Hook
 *               - Setup Config
 *               - Set-up Hook
 *
 *    2. Mongoose - Connect
 *        - Setup AppZet Model
 *        - Setup DB Connect
 *
 *    3. Express - Initialise
 *        - Setup Middleware
 *
 *
 *    4. AppZet - Start
 *              - Bootstrap
 *
 */


/**********************************************************************
 *              AppZet - Create and Initialise server
 **********************************************************************/

const EventEmitter = require('events');
const helmet = require('helmet');

let sources = require('./globals');

for (let key in sources) {
  global[key] = sources[key];
}

require('mongoose-big-decimal')(Mongoose);

const configPath = "src/config/";
const servicePath = "src/api/services/";
const hookPath = "src/api/hooks/";
const modelPath = "src/api/models/";
const policyPath = "src/api/policies/";
const responsePath = "src/api/responses/";
const controllerPath = "src/api/controllers/";
const errorPath = "src/api/errors";
const configLoadOrder = [
  'bootstrap.js',
  'connections.js',
  'hooks.js',
  'sessions.js',
  'models.js',
  'responses.js',
  'security.js',
  'passport.js',
  'cors.js',
  'routes',
  'http.js',
  'socket.js'
];

global.AppZet = {
  config: {},
  models: {},
  modelDefinitions: {},
  path: Path.join(__dirname, '../../'),
  event: new EventEmitter(),
  hookRunning: [],
  loader: {},
  socket: null
};


/**********************************************************************
 *                        - Setup API
 **********************************************************************/


AppZet.loader.setupRouters = function () {
  let appZetrouter = Express.Router();

  _.forEach(AppZet.config.routes.routers, function (routerConfig) {
    logger.silly("Router:", routerConfig.baseUrl);

    if (routerConfig.middlewares && Array.isArray(routerConfig.middlewares) && routerConfig.middlewares.length > 0)
      appZetrouter.use(routerConfig.baseUrl, routerConfig.middlewares);

    appZetrouter.use(routerConfig.baseUrl, AppZet.loader.getApiRouter(routerConfig, routerConfig.baseUrl));
  });
  return appZetrouter;
};


AppZet.loader.getApiRouter = function (routerConfig, baseUrl) {
  let route, resolution, method, url, controller, api;
  let router = Express.Router();

  _.forEach(routerConfig.routes, function (value, key) {
    route = key.split(' ');
    resolution = value.split('.');
    method = route[0].toLowerCase();
    url = route[1];

    // Controller not exist
    if (!AppZet.controllers[resolution[0]])
      return logger.log('warn', "Controller does not exist Route: ", key, " does not exist");

    controller = AppZet.controllers[resolution[0]];

    if (!controller[resolution[1]])
      return logger.log('warn', "baseUrl:", baseUrl, "Api doest not exist Route: ", key, " does not exist");

    api = controller[resolution[1]];


    if (api) {
      logger.silly('Express route: %s - %s resolved to: %s - %s', route[0], route[1], resolution[0], resolution[1]);

      if (routerConfig.policies[resolution[0]]) {
        if (routerConfig.policies[resolution[0]][resolution[1]]) {
          AppZet.loader.policyConfigSetup(AppZet.express, router, AppZet.policies, method, url, api, routerConfig.policies[resolution[0]][resolution[1]]);
        } else if (routerConfig.policies[resolution[0]]['*']) {
          AppZet.loader.policyConfigSetup(AppZet.express, router, AppZet.policies, method, url, api, routerConfig.policies[resolution[0]]['*']);
        } else {
          AppZet.loader.policyConfigSetup(AppZet.express, router, AppZet.policies, method, url, api, routerConfig.policies[resolution[0]]);
        }
      } else if (routerConfig.policies['*']) {
        AppZet.loader.policyConfigSetup(AppZet.express, router, AppZet.policies, method, url, api, routerConfig.policies['*']);
      }
    }
  });

  return router;
};

AppZet.loader.policyConfigSetup = function (express, router, policies, method, url, api, config) {

  if (typeof config === 'boolean') {
    if (config)
      router[method](url, api);
  }

  if (typeof config === 'string') {
    if (policies[config]) {
      router[method](url, policies[config]);
      router[method](url, api);
    }
  }

  if (Array.isArray(config)) {
    _.forEach(config, function (identity) {
      if (typeof identity === 'string')
        if (policies[identity])
          router[method](url, policies[identity]);
    });
    router[method](url, api);
  }
};

/**********************************************************************
 *                         Setting Log Level
 **********************************************************************/

/**
 *  Usage:
 *        let logger = AppZet.logger('AppZet');
 *        logger.debug('AppZet Initialisation Done');
 *
 */

_.assignIn(AppZet.config, require(AppZet.path + configPath + 'log.js'));
_.assignIn(AppZet.config, require(AppZet.path + configPath + 'models.js'));

Winston.transports.DailyRotateFile = require('winston-daily-rotate-file');

let folders = ['logs'];

folders.forEach((folder) => {
  if (!FileSystem.existsSync(folder)) {
    FileSystem.mkdirSync(folder);
  }
});

AppZet.logger = function (tag, errorHandle) {

  let config = {
    level: AppZet.config.log.level,
    exitOnError: true,
    transports: [
      new (Winston.transports.Console)(
        {
          colorize: true,
          timestamp: true,
          prettyPrint: true
        }
      ),
      new (Winston.transports.DailyRotateFile)({
        name: 'debug-file',
        filename: AppZet.config.log.logFilePath || 'logs/appzet.debug',
        level: 'debug',
        datePattern: '.yyyy-MM-dd.log',
        maxsize: 10000000,
        json: false
      })
    ]
  };

  if (errorHandle)
    config.exceptionHandlers = [
      new Winston.transports.Console({
        colorize: true,
        handleExceptions: false,
        humanReadableUnhandledException: true
      }),
      new (Winston.transports.DailyRotateFile)({
        name: 'debug-file',
        filename: AppZet.config.log.exceptionFilePath || 'logs/appzet.exceptions',
        level: 'debug',
        datePattern: '.yyyy-MM-dd.log',
        maxsize: 10000000,
        json: false
      })
    ];

  let logger = new (Winston.Logger)(config);

  logger.log = function () {
    let args = arguments;
    if (tag)
      args[1] = '[' + tag + ']' + ' - ' + args[1];
    Winston.Logger.prototype.log.apply(this, args);
  }

  return logger;
};

let logger = AppZet.logger('AppZet');
AppZet.event.emit('loggerLoaded');
logger.debug('AppZet Initialisation Done');


function loadAppZet() {
  return new Promise(function (resolve, reject) {

    /**********************************************************************
     *                      AppZet - Initialise Service
     **********************************************************************/

    AppZet.services = {};
    FileSystem.readdirSync(AppZet.path + servicePath).forEach(function (file) {
      if (file.indexOf('DS_Store') > -1)return;


      let service = require(AppZet.path + servicePath + file);
      let serviceName = file.split('.')[0];
      AppZet.services[serviceName] = service;
      global[serviceName] = service;
    });

    AppZet.event.emit('serviceLoaded');
    logger.debug('AppZet Initialise Services Done');
    logger.silly('AppZet Services:', AppZet.services);


    /**********************************************************************
     *                      AppZet - Initialise Model
     **********************************************************************/

    AppZet.modelDefinitions = {};
    FileSystem.readdirSync(AppZet.path + modelPath).forEach(function (file) {
      let model = require(AppZet.path + modelPath + file);
      let modelName = file.split('.')[0];

      if (!model.disableGlobalSchema)
        model.schema = _.assign({}, AppZet.config.models.schema, model.schema);

      AppZet.modelDefinitions[modelName] = model;
    });

    AppZet.event.emit('modelLoaded');
    logger.debug('AppZet Initialise Models Done');
    logger.silly('AppZet Models:', AppZet.modelDefinitions);


    /**********************************************************************
     *                      AppZet - Initialise Policy
     **********************************************************************/

    AppZet.policies = {};
    FileSystem.readdirSync(AppZet.path + policyPath).forEach(function (file) {
      let policy = require(AppZet.path + policyPath + file);
      let policyName = file.split('.')[0];
      AppZet.policies[policyName] = policy;
      AppZet.policies[policyName.toLowerCase()] = policy;
    });

    AppZet.event.emit('policyLoaded');
    logger.debug('AppZet Initialise policies Done');
    logger.silly('AppZet policies:', AppZet.policies);


    /**********************************************************************
     *                      AppZet - Initialise Response
     **********************************************************************/

    AppZet.responses = {};
    FileSystem.readdirSync(AppZet.path + responsePath).forEach(function (file) {
      let response = require(AppZet.path + responsePath + file);
      let responseName = file.split('.')[0];
      AppZet.responses[responseName] = response;
    });

    AppZet.event.emit('responseLoaded');
    logger.debug('AppZet Initialise responses Done');
    logger.silly('AppZet responses:', AppZet.responses);


    /**********************************************************************
     *                      AppZet - Initialise Controller
     **********************************************************************/

    AppZet.controllers = {};
    FileSystem.readdirSync(AppZet.path + controllerPath).forEach(function (file) {
      let controller = require(AppZet.path + controllerPath + file);
      let controllerName = file.split('.')[0];
      AppZet.controllers[controllerName] = controller;
    });

    AppZet.event.emit('controllerLoaded');
    logger.debug('AppZet Initialise Controllers Done');
    logger.silly('AppZet Controllers:', AppZet.controllers);

    /**********************************************************************
     *                      AppZet - Initialise Hook
     **********************************************************************/

    AppZet.hooks = {};
    FileSystem.readdirSync(AppZet.path + hookPath).forEach(function (file) {
      let hook = require(AppZet.path + hookPath + file);
      // let hookName = file.split('.')[0];
      AppZet.hooks[hook.identity] = hook;
    });

    AppZet.event.emit('hookLoaded');
    logger.debug('AppZet Initialise Hooks Done');
    logger.silly('AppZet Hooks:', AppZet.hooks);


    /**********************************************************************
     *                        AppZet - Setup Config
     **********************************************************************/

    // Load policies first

    AppZet.config.policies = {};

    FileSystem.readdirSync(AppZet.path + configPath + 'policies').forEach(function (file) {
      let policyConfig = require(AppZet.path + configPath + 'policies/' + file);
      let policyConfigName = file.split('.')[0];
      AppZet.config.policies[policyConfigName] = policyConfig;
      AppZet.config.policies[policyConfigName.toLowerCase()] = policyConfig;
    });

    _.forEach(configLoadOrder, function (file) {
      _.assignIn(AppZet.config, require(AppZet.path + configPath + file));
    });


    resolve();
  });
}

/**********************************************************************
 *                        AppZet - Set-up Hook
 **********************************************************************/

function setupHook() {
  return new Promise(function (resolve, reject) {
    if (AppZet.config.hooks.inactive)
      return resolve();

    _.forEach(AppZet.hooks, function (hook) {

      if (!hook.identity instanceof String || !hook.afterEvent instanceof String || !hook.initialise instanceof Function)
        return logger.log("error", "hook must have identity:String, afterEvent:String, initialise:Function");

      if (AppZet.hookRunning.indexOf(hook.identity) !== -1)
        return logger.log("error", "hook identity:String must be unique");

      if (!hook.inactive) {
        AppZet.hookRunning.push(hook.identity);
        return AppZet.event.once(hook.afterEvent, function () {
          hook.initialise(hookComplete.bind(null, hook.identity));
        });
      }
    });

    return resolve();
  });
}

function hookComplete(identity, error) {
  let index = AppZet.hookRunning.indexOf(identity);
  AppZet.hookRunning.splice(index, 1);
  AppZet.event.emit('hookDone', identity, error);
}

/**********************************************************************
 *                        Mongoose - connect
 **********************************************************************/

function connectToDb(url) {
  return new Promise(function (resolve, reject) {
    Mongoose.Promise = Promise;
    let dbUrl = url || AppZet.config.connections.mongoDb.url;
    let option = AppZet.config.connections.mongoDb.option || {};
    logger.debug('Mongoose connecting to MongoDBb: %s', dbUrl);
    Mongoose.connect(dbUrl, option);

    Mongoose.connection.on('error', function (err) {
      logger.debug('Mongoose connection to MongoDBb Failed');
      reject(err);
    });

    Mongoose.connection.once('open', function () {
      AppZet.event.emit('dbConnected');
      logger.debug('Mongoose connected to MongoDBb');

      resolve();
    });
  });
}


function loadModels() {
  return new Promise(function (resolve, reject) {
    AutoIncrement.initialize(Mongoose.connection);
    logger.debug('Mongoose plugin loaded');

    _.forEach(AppZet.modelDefinitions, function (model, modelName) {
      logger.silly('Loading mongoose schema:', modelName);

      let schema = model.schema;

      let options = _.assign({}, AppZet.config.models.options, model.options);

      let modelSchema = Schema(schema, options);

      // indexing model
      if (model.indexes) {
        _.forEach(model.indexes, index => {
          modelSchema.index(index.field, index.option);
        });
      }

      logger.silly('Applying mongoose plugin to schema: %s', modelName);
      if (!model.disabledPlugin || !model.disabledPlugin.AutoIncrement) {
        if (model.plugins && model.plugins.AutoIncrement && model.plugins.AutoIncrement.options) {
          modelSchema.plugin(AutoIncrement.plugin, _.assign({}, {model: modelName}, model.plugins.AutoIncrement.options));
        } else
          modelSchema.plugin(AutoIncrement.plugin, {model: modelName, startAt: 1});
      }


      modelSchema.plugin(FindOrCreate);

      // Create life-cycle
      _.forEach(model.cycles, (val, key) => {
        if (_.isArray(val)) {
          _.forEach(val, function (sequence) {
            assignMongooseMiddleware(modelSchema, key, sequence);
          });
        } else if (_.isFunction(val)) {
          assignMongooseMiddleware(modelSchema, key, val);
        }
      });

      // Create static methods
      _.assign(modelSchema.statics, model.statics);

      // Create instance methods
      _.assign(modelSchema.methods, model.methods);

      _.forEach(model.virtuals, (value, key) => {
        modelSchema.virtual(key, value);
      });

      global[modelName] = AppZet.models[modelName] = Mongoose.model(modelName, modelSchema);
    });

    AppZet.event.emit('mongoModelLoaded');
    logger.debug('Mongo Initialisation Done');
    resolve();
  });
}

function assignMongooseMiddleware(schema, key, fn) {
  let operation = key.replace('before', '');
  operation = operation.replace('after', '');
  operation = operation.charAt(0).toLowerCase() + operation.slice(1);
  if (key.indexOf('before') == 0)
    schema['pre'](operation, fn);
  else
    schema['post'](operation, fn);
}


/**********************************************************************
 *                        Express - Initialise
 **********************************************************************/

function loadRouters() {


  return new Promise(function (resolve, reject) {
    logger.debug('Express Initialisation Start');

    AppZet.express = Express();

    // Security framework
    AppZet.express.use(helmet());


    /**********************************************************************
     *                      - Setup Middleware
     **********************************************************************/

    _.forEach(AppZet.config.http.middlewares, function (item) {
      AppZet.express.use(item.path, item.middleware);
    });

    AppZet.event.emit('expressLoaded');
    logger.debug('Express Initialisation Done');
    resolve(AppZet.express);
  });
}


function setupSocket() {

  if (AppZet.config.socket.adapter)
    AppZet.socket.adapter(AppZet.config.socket.adapter);

  _.forEach(AppZet.config.socket.middlewares, function (item) {
    AppZet.socket.use(item);
  });
}

module.exports = {
  loadAppZet: loadAppZet,
  setupHook: setupHook,
  connectToDb: connectToDb,
  loadModels: loadModels,
  loadRouters: loadRouters,
  setupSocket: setupSocket
};




