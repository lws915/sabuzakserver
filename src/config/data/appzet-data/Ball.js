module.exports = [
  {
    vId: 1,
    document: {
      owner: 1,
      amount: 30,
      usage:'이벤트 지급',
    }
  },
  {
    vId: 2,
    document: {
      owner: 1,
      amount: -10,
      usage:'이성 프로필 추가 확인',
    }
  },
  {
    vId: 3,
    document: {
      owner: 1,
      amount: 30,
      usage:'앱 내 결제에 의한 충전',
    }
  },
  {
    vId: 4,
    document: {
      owner: 2,
      amount: 10,
      usage:'평가에 의한 구슬 지급',
    }
  },
  {
    vId: 5,
    document: {
      owner: 2,
      amount: -5,
      usage:'이성 프로필 추가 확인',
    }
  },
  {
    vId: 6,
    document: {
      owner: 2,
      amount: 50,
      usage:'최초 가입시 50개 지급',
    }
  }
];