'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */


let admin = ['AuditPolicy', 'BearerAuth', 'Admin'];

let user = ['AuditPolicy', 'BearerAuth', 'User'];

// let student = ['AuditPolicy', 'BearerAuth', 'User'];

// let regularTeacher = ['AuditPolicy', 'BearerAuth', 'User'];

// let associateTeacher = ['AuditPolicy', 'BearerAuth', 'User'];

// let postFindPolicy = ['AuditPolicy', 'BearerAuth', 'PostFindPolicy'];

let owner = ['AuditPolicy', 'BearerAuth', 'User', 'OwnerPolicy'];

let anyone = ['AuditPolicy'];

let anyoneOrUser = ['AuditPolicy', 'BearerAuth'];

module.exports = {
  '*': false,

  AnnouncementController: {
    // Public
    count: anyone,
    find: anyone,
    findOne: anyone,

    // 관리자
    create: anyone,
    update: anyone,
    remove: anyone
  },

  AuthController: {
    // Public
    checkEmail: anyone,
    checkUsername: anyone,
    checkNickname: anyone,
    register: anyone,
    login: anyone,
    socialLogin: anyone,
    posLogin: anyone,
    posRenewSession: anyone,
    OAuthConnect: anyone,
    confirmActivation: anyone,
    forgotPasswordStart: anyone,
    forgotPasswordCheck: anyone,
    forgotPasswordComplete: anyone,
    forgotPasswordStartSMS: anyone,
    forgotPasswordCheckSMS: anyone,
    forgotPasswordCompleteSMS: anyone,
    support: anyone,
    reportError: anyone,
    logout: anyone,

    // 사용자
    getMyUserInfo: owner,
    updateMyInfo: owner,

    changeSafeNumber: owner,

    // Only 사용자 (Block 체험 사용자)
    connectLocal: anyone,
    OAuthDisconnect: anyone,
    changePassword: anyone,
    sendActivationEmail: anyone,

    checkMobileNumber: anyone,
    completeMobileNumber: anyone,

    // POS 사용자
    posLogout: anyone,
  },

  DeviceController: {
    // 앱 사용자 Data주인
    mine: anyone,
    pushOn: anyone,
    pushOff: anyone,
    update: anyone,

    // 사용자 서버주인
    count: anyone,
    find: anyone,
    findOne: anyone,

    // 관리자
    // pushToTopicSomething: anyone,
    remove: anyone,
  },

  FileController: {
    // Public
    create: anyone,
    createOnlyUrl: anyone,
    update: anyone,
    remove: anyone
  },

  UserController: {
    // 사용자 (서버)
    getCustomerStat: anyoneOrUser,

    // 사용자 (서버) or 관리자
    count: anyoneOrUser,
    find: anyoneOrUser,
    findOne: anyoneOrUser,
    findRandom: anyoneOrUser,
    profileOpen: anyoneOrUser,
    getUsersByPop: anyoneOrUser,

    // 관리자
    resetLoginLimit: anyoneOrUser,
    resetEmailConfirmLimit: anyoneOrUser,
    resetPasswordResetLimit: anyoneOrUser,
		update: anyoneOrUser,
		classUpdate: anyoneOrUser,
    remove: anyoneOrUser,
  },

  VersionController: {
    // 관리자
    count: admin,
    find: admin,
    findOne: admin,

    create: admin,
    update: admin,
    remove: admin,
  },
  PostController: {
    count: anyoneOrUser,
    find: anyoneOrUser,
    findOne: anyoneOrUser,

    create: anyoneOrUser,
    update: anyoneOrUser,
    remove: anyoneOrUser,
  },
  NoticeController: {
    count: anyoneOrUser,
    find: anyoneOrUser,
    findOne: anyoneOrUser,

    create: anyoneOrUser,
    update: anyoneOrUser,
    remove: anyoneOrUser,
  },
  BallController: {
    count: anyoneOrUser,
    find: anyoneOrUser,
    findOne: anyoneOrUser,

    create: anyoneOrUser,
    update: anyoneOrUser,
    remove: anyoneOrUser,
  },
  BallWalletController: {
    count: anyoneOrUser,
    find: anyoneOrUser,
    findOne: anyoneOrUser,

    create: anyoneOrUser,
    update: anyoneOrUser,
    remove: anyoneOrUser,
  },
  LikeController: {
    count: anyoneOrUser,
    find: anyoneOrUser,
    findOne: anyoneOrUser,

    create: anyoneOrUser,
    remove: anyoneOrUser,
  },
  RankController: {
    count: anyoneOrUser,
    find: anyoneOrUser,
    findOne: anyoneOrUser,
    recommend: anyoneOrUser,

    create: anyoneOrUser,
    remove: anyoneOrUser,
  },
	
};





