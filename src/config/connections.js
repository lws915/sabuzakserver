'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */
let nodemailer = require("nodemailer");

module.exports.connections = {

  /********************************
   *             DEV
   ********************************/

  mongoDb: {
    url: 'mongodb://localhost/sabuzak',
  },

  mailTemplates: [
    'support',
    'activate',
    'passwordreset',
    'refund'
  ],

  email: '',

  iamport: {
    impKey: '8470584791374682',
    impSecret: '9lQrfho4Ga4dYzWTLjBb0spU6yKiAZjJkaazrzD8JHmIhiB2CdeCFDxMbh4LgouAyFa0fN9YvvHMNiPq'
  },

  oauthBaseUrl: 'sabuzak',

  google: {
    clientId: '',
    clientSecret: '',
    accessType: 'offline',
    scope: "email",
    response_type: 'code',
  },

  facebook: {
    clientId: '',
    clientSecret: '',
    scope: "email",
    response_type: 'code',
  },

  // TODO: need to change this later
  sms: {
    id: 'baton4972',
    pwd: 'NUI9797FHR5459XMZPP2',
    sender: '01095943306'
  },

  safeNumber: {
    agent_code: "LOPE",
    comm_code: 102,
    url: 'http://api.mybilling.co.kr/JOIN_050/join.php'
  },

  s3: {
    images: {
      bucket: 'images.applicat.co.kr',
    },
  },


  /********************************
   *             PRODUCTION
   ********************************/
}
;