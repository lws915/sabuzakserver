'use strict';

/**
 * Created by wonseok Lee on 30/08/2017
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 30/08/2017
 *
 */

module.exports = {


  /**************************************
   *               Auth
   *************************************/
  'GET /checkEmail': 'AuthController.checkEmail',
  'POST /register': 'AuthController.register',
  'POST /login': 'AuthController.login',
  'get /socialLogin': 'AuthController.socialLogin',
  'PUT /activate/confirm': 'AuthController.confirmActivation',
  'POST /forgotPassword': 'AuthController.forgotPasswordStart',
  'POST /forgotPasswordCheck': 'AuthController.forgotPasswordCheck',
  'PUT /forgotPasswordComplete': 'AuthController.forgotPasswordComplete',

  'POST /forgotPasswordSMS': 'AuthController.forgotPasswordStartSMS',
  'POST /forgotPasswordCheckSMS': 'AuthController.forgotPasswordCheckSMS',
  'PUT /forgotPasswordCompleteSMS': 'AuthController.forgotPasswordCompleteSMS',

  'POST /support': 'AuthController.support',
  'GET /logout': 'AuthController.logout',
  'PUT /changePassword': 'AuthController.changePassword',
  'GET /me': 'AuthController.getMyUserInfo',
  'PUT /me': 'AuthController.updateMyInfo',
  'PUT /changeSafeNumber': 'AuthController.changeSafeNumber',
  'POST /activate/send': 'AuthController.sendActivationEmail',
  'POST /reportError': 'AuthController.reportError',

  'POST /checkMobileNumber': 'AuthController.checkMobileNumber',
  'POST /completeMobileNumber': 'AuthController.completeMobileNumber',

  /**************************************
   *               Device
   *************************************/
  'GET /device/count': 'DeviceController.count',
  'GET /device/find': 'DeviceController.find',
  'GET /device/findOne': 'DeviceController.findOne',

  'GET /device/mine': 'DeviceController.mine',
  'PUT /device/pushOn': 'DeviceController.pushOn',
  'PUT /device/pushOff': 'DeviceController.pushOff',
  'PUT /device': 'DeviceController.update',


  /**************************************
   *               File
   *************************************/
  'POST /file': 'FileController.create',
  'POST /file/createOnlyUrl': 'FileController.createOnlyUrl',

  'PUT /file': 'FileController.update',
  'DELETE /file': 'FileController.remove',


  /**************************************
   *               RequestLog
   *************************************/
  'GET /requestLog/count': 'RequestLogController.count',
  'GET /requestLog/find': 'RequestLogController.find',
  'GET /requestLog/findOne': 'RequestLogController.findOne',
  'GET /requestLog/countIp': 'RequestLogController.countIp',
  'GET /requestLog/countMonth': 'RequestLogController.countMonth',

  /**************************************
   *               User
   *************************************/
  'GET /user/count': 'UserController.count',
	'GET /user/find': 'UserController.find',
  'GET /user/findOne': 'UserController.findOne',
  'GET /user/findRandom': 'UserController.findRandom',
  'GET /user/getUsersByPop': 'UserController.getUsersByPop',

  'GET /user/stat': 'UserController.getCustomerStat',
  'POST /user/profileOpen': 'UserController.profileOpen',

  'PUT /user/login/reset': 'UserController.resetLoginLimit',
  'PUT /user/emailConfirm/reset': 'UserController.resetEmailConfirmLimit',
  'PUT /user/passwordChange/reset': 'UserController.resetPasswordResetLimit',
	'PUT /user': 'UserController.update',
  'DELETE /user': 'UserController.remove',


  /**************************************
   *               Version
   *************************************/
  'GET /version/count': 'VersionController.count',
  'GET /version/find': 'VersionController.find',
  'GET /version/findOne': 'VersionController.findOne',
  'POST /version': 'VersionController.create',
  'PUT /version': 'VersionController.update',
  'DELETE /version': 'VersionController.remove',

  /**************************************
   *               Post
   *************************************/
  'GET /post/count': 'PostController.count',
  'GET /post/find': 'PostController.find',
  'GET /post/findOne': 'PostController.findOne',
  'POST /post': 'PostController.create',
  'PUT /post': 'PostController.update',
  'DELETE /post': 'PostController.remove',

  /**************************************
   *               Notice
   *************************************/
  'GET /notice/count': 'NoticeController.count',
  'GET /notice/find': 'NoticeController.find',
  'GET /notice/findOne': 'NoticeController.findOne',
  'POST /notice': 'NoticeController.create',
  'PUT /notice': 'NoticeController.update',
  'DELETE /notice': 'NoticeController.remove',

  /**************************************
   *               Ball
   *************************************/
  'GET /ball/count': 'BallController.count',
  'GET /ball/find': 'BallController.find',
  'GET /ball/findOne': 'BallController.findOne',
  'POST /ball': 'BallController.create',
  'PUT /ball': 'BallController.update',
  'DELETE /ball': 'BallController.remove',

  /**************************************
   *               BallWallet
   *************************************/
  'GET /ballWallet/count': 'BallWalletController.count',
  'GET /ballWallet/find': 'BallWalletController.find',
  'GET /ballWallet/findOne': 'BallWalletController.findOne',
  'POST /ballWallet': 'BallWalletController.create',
  'PUT /ballWallet': 'BallWalletController.update',
  'DELETE /ballWallet': 'BallWalletController.remove',

  /**************************************
   *               Like
   *************************************/
  'GET /like/count': 'LikeController.count',
  'GET /like/find': 'LikeController.find',
  'GET /like/findOne': 'LikeController.findOne',
  'POST /like': 'LikeController.create',
  'DELETE /like': 'LikeController.remove',

  /**************************************
   *               Rank
   *************************************/
  'GET /rank/count': 'RankController.count',
  'GET /rank/find': 'RankController.find',
  'GET /rank/findOne': 'RankController.findOne',
  'GET /rank/recommend': 'RankController.recommend',
  'POST /rank': 'RankController.create',
  'DELETE /rank': 'RankController.remove',

};