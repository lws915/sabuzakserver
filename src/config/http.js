'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */


let notFound = require("../appzet/middlewares/notFound");
let errorHandler = require("../appzet/middlewares/errorHandler");
let bind = require("../appzet/middlewares/Bind");
let cookieParser = require('cookie-parser');

module.exports.http = {
  middlewares: [
    {path: '/', middleware: require("../appzet/middlewares/RequestLogger")},
    {path: '/', middleware: cookieParser()},
    {path: '/', middleware: bind}, // AppZet middleware
    {path: '/', middleware: AppZet.loader.setupRouters()},
    {path: '/', middleware: notFound},
    {path: '/', middleware: errorHandler},
  ]
};

