'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

let logger = AppZet.logger('SubscriptionService');

module.exports = {
  subscribe: subscribe,

  changeCard: changeCard,
  changePlan: changePlan,

  renewOn: renewOn,
  renewOff: renewOff,

  synchronize: synchronize,
  renew: renew,

  cancel: cancel,
};

/******************
 * Public Methods
 ******************/

/**
 *
 * 구독을 시작한다
 * - 만들기
 *
 * @param req
 * @param res
 */
function subscribe(clientId, invoice) {
  return new Promise((resolve, reject) => {
    if (!invoice.plans
      || !Array.isArray(invoice.plans)
      || !invoice.plans.length > 0
      || !invoice.payments
      || !invoice.payments[0])
      return reject(new Error("InvalidInvoice"));

    let planId, creditCardId;

    if (typeof invoice.plans[0] === 'object')
      planId = invoice.plans[0]._id;
    else
      planId = invoice.plans[0];

    if (typeof invoice.payments[0].creditCard === 'object')
      creditCardId = invoice.payments[0].creditCard._id;
    else
      creditCardId = invoice.payments[0].creditCard;

    Plan.findOne({_id: planId})
      .then((plan) => {
        if (!plan) throw new Error("PlanNotFound");

        let startTime = Moment().startOf('day');
        let endTime = Moment().startOf('day').month(startTime.month() + 1);

        let subscription = {
          client: clientId,

          invoice: invoice._id,
          price: plan.salePrice || plan.price,
          purchasedTime: invoice.createdAt,
          startTime: startTime,
          endTime: endTime,
          currentPlan: plan.toObject(),
          plan: planId,
          creditCard: creditCardId,

          server: invoice.server,
          owner: invoice.owner
          // Syncronize 때 저장됨
          // recurringPlan: {type: Object},
        };

        return Subscription.create(subscription);
      })
      .then((subscription) => {
        resolve(subscription);
      })
      .catch(err => {
        reject(err);
      })
  });
}


function changeCard(subscriptionId, creditCard) {

  let foundSubscription, createdCreditCard;

  return Subscription.findOne({_id: subscriptionId, status: 'Start'})
    .populate('creditCard')
    .then((subscription) => {
      if (!subscription)throw new Error('SubscriptionNotFound');
      foundSubscription = subscription;
      return TransactionService.registerCreditCard(creditCard,
        foundSubscription.creditCard.owner,
        foundSubscription.creditCard.server);
    })
    .then((creditCard) => {
      createdCreditCard = creditCard;
      TransactionService.removeCreditCard(foundSubscription.creditCard.customer_uid);
      return foundSubscription.creditCard.remove();
    })
    .then(() => {
      foundSubscription.creditCard = createdCreditCard._id;
      return foundSubscription.save();
    });
}

function changePlan(clientId, planId, creditCard) {

  let foundSubscription, foundPlan, planOption;

  const PlanOption = {
    UPGRADE: 0,
    DOWNGRADE: 1,
    CANCEL_DOWNGRADE: 2,
  };

  let today = Moment();

  return Promise.all([
    Subscription
      .findOne({client: clientId, status: 'Start', endTime: {$gte: today}})
      .populate('plan')
      .populate('owner'),
    Plan.findOne({_id: planId})
  ])
    .spread((subscription, plan) => {
      if (!subscription) throw new Error('SubscriptionNotFound');
      if (!plan) throw new Error('PlanNotFound');

      foundSubscription = subscription;
      foundPlan = plan;

      if (foundSubscription.plan.price > foundPlan.price) {
        planOption = PlanOption.DOWNGRADE;
      } else if (foundSubscription.plan.price < foundPlan.price) {
        planOption = PlanOption.UPGRADE;
      } else {
        planOption = PlanOption.CANCEL_DOWNGRADE;
      }

      switch (planOption) {
        case PlanOption.UPGRADE:
          return upgrade(foundSubscription, foundPlan, creditCard);
          break;
        case PlanOption.DOWNGRADE:
          return downgrade(foundSubscription, foundPlan);
          break;
        case PlanOption.CANCEL_DOWNGRADE:
          return cancelDowngrade(foundSubscription, subscription.plan);
          break;
      }
    })

}


function upgrade(subscription, plan, creditCard) {

  let today = Moment().startOf('day');
  let startTime = Moment(subscription.startTime);
  let endTime = Moment(subscription.endTime);
  let totalDays = endTime.diff(startTime, 'days');
  let daysLeft = endTime.diff(today, 'days');
  let daysUsed = today.diff(startTime, 'days');


  let newPlanPrice = plan.salePrice || plan.price;

  let willUsePrice = plan.salePrice ? plan.salePrice : plan.price;
  willUsePrice = Math.floor(willUsePrice / totalDays * daysLeft);

  let currentPlanPrice = subscription.plan.salePrice || subscription.plan.price;


  let usedPrice = subscription.plan.salePrice ? subscription.plan.salePrice : subscription.plan.price;
  usedPrice = Math.floor(usedPrice / totalDays * daysUsed);

  let totalPrice = willUsePrice - currentPlanPrice + usedPrice;

  let invoice = {
    type: 'Subscription',

    // 상품정보
    totalPrice: totalPrice,
    plans: [plan._id],
    itemList: {
      plans: [plan.toObject()],
    },

    notes: "플랜 업그레이드",

    // 개인 정보
    email: subscription.owner.identifier,
    fullName: subscription.owner.fullName,
    phone: subscription.owner.phone,
    address: subscription.owner.address,
    addressDetail: subscription.owner.addressDetail,
    companyName: subscription.owner.companyName,

    client: subscription.client,

    owner: subscription.owner._id,
    server: subscription.owner.server
  };

  let createdInvoice;

  return Invoice.create(invoice)
    .then((invoice) => {
      createdInvoice = invoice;

      if (!creditCard)
        return TransactionService.payWithExistingCard(invoice, subscription.creditCard);
      else
        return TransactionService.transact(invoice, creditCard, true);
    })
    .then((payment) => {
      subscription.status = 'Stop';

      let newSubscription = {
        client: subscription.client,
        invoice: createdInvoice._id,
        price: newPlanPrice,
        purchasedTime: createdInvoice.createdAt,
        startTime: subscription.startTime,
        endTime: subscription.endTime,
        currentPlan: plan.toObject(),
        plan: plan._id,

        server: createdInvoice.server,
        owner: createdInvoice.owner,

        // Syncronize 때 저장됨
        recurringPlan: subscription.recurringPlan,

        previousSubscription: subscription._id,
      };

      if (!creditCard)
        newSubscription.creditCard = subscription.creditCard;
      else
        newSubscription.creditCard = payment.creditCard;


      return [
        Subscription.create(newSubscription),
        subscription.save()
      ]
    });
}

function downgrade(subscription, plan) {
  subscription.recurringPlan = plan.toObject();
  return subscription.save();
}

function cancelDowngrade(subscription, plan) {
  subscription.recurringPlan = plan.toObject();
  return subscription.save();
}


function renewOn(id) {

  // 연체료 물은 구독은 중간에 못 멈춤
  let today = Moment();
  return Subscription.findOne({_id: id, status: "Start", endTime: {$gte: today}})
    .then(subscription => {
      if (!subscription)
        throw new Error("SubscriptionNotFound");

      subscription.renew = 'On';
      return subscription.save();
    });
}

/**
 * 1. Subscription update subscription canceled = true;
 */
function renewOff(id) {

  // 연체료 물은 구독은 중간에 못 멈춤
  let today = Moment();
  return Subscription.findOne({_id: id, status: "Start", endTime: {$gte: today}})
    .then(subscription => {
      if (!subscription)
        throw new Error("SubscriptionNotFound");

      let today = Moment();

      let diffDays = Moment(subscription.endTime).diff(today, 'days');

      if (diffDays > 13)
        subscription.renew = 'Off';
      else
        subscription.renew = 'Schedule Cancel';

      return subscription.save();
    });
}

/**
 *
 * - 구독 기간이 지낫는지 체크 하고 지낫으면 경고 메일 보내기
 * - 구독 기간이 15일전 다음 결제 가격 지정
 * - 경고 기간 이후 (15일) 앱 정지
 *
 *
 *  구독을 전체적으로 확인 하는 Batch process
 *    - 구독 만료 15일전 현재 Plan의 가격을 측정 및 다음 구독 정보 미리저장
 *
 *    - 만료된 구독이 잇는지 확인
 *      - 자동 결제 On 또는 Schedule Cancel 일 경우
 *        - 결제 시도
 *          - 실패 → 1,3,5,7,12,15일 간격 경고 이메일 15일날 중단
 *          - 성공 → 다음 구독
 *            - On일 경우 구독 renew = On
 *            - Schedule Cancel일 경우 구독 renew = Off
 *
 *      - 자동 결제 Off일 경우
 *        구독 중지
 *
 * @param req
 * @param res
 */
function synchronize() {

  // - 구독 만료 15일전 현재 Plan의 가격을 측정 및 다음 구독 정보 미리저장
  let today = Moment();
  let lastDay = Moment().date(today.date() + 15);

  let recurringPlanPromise =
    Subscription.find({
      endTime: {$gte: today, $lt: lastDay},
      recurringPlan: {$exists: false},
      status: "Start"
    })
      .populate('plan')
      .populate('owner')
      .then(subscriptions => {
        _.forEach(subscriptions, (subscription) => {
          subscription.recurringPlan = subscription.plan.toObject();
          subscription.save();
        });
      });

  let stopPromise =
    Subscription.find({
      endTime: {$lte: today},
      status: "Start",
      renew: 'Off'
    })
      .then(subscriptions => {
        _.forEach(subscriptions, (subscription) => {
          subscription.status = 'Stop';
          subscription.save();
        });
      });

  // - 만료된 구독이 잇는지 확인
  let paymentPromise =
    Subscription.find({
      endTime: {$lte: today},
      status: "Start",
      renew: {$in: ['On', 'Schedule Cancel']}
    })
      .populate('plan')
      .populate('owner')
      .then((subscriptions) => {
        let promises = [];
        _.forEach(subscriptions, (subscription) => {
          promises.push(renew(subscription));
        });

        return promises;
      });

  return Promise.all([recurringPlanPromise, stopPromise, paymentPromise])
    .spread(() => {
      console.log("done");
    })
    .catch(err => {
      logger.log('error', err);
    });
}


/**
 * - 구독 재계
 * @param invoice
 */
function renew(subscription, newPlan, creditCard) {
  return new Promise((resolve, reject) => {

    let plan = newPlan || subscription.recurringPlan || subscription.plan;
    let priceToPay = plan.salePrice || plan.price;

    let invoice = {
      type: 'Subscription',

      // 상품정보
      totalPrice: priceToPay,
      plans: [plan._id],
      itemList: {
        plans: [plan.toObject()],
      },

      notes: "구독 결제",

      // 개인 정보
      email: subscription.owner.identifier,
      fullName: subscription.owner.fullName,
      phone: subscription.owner.phone,
      address: subscription.owner.address,
      addressDetail: subscription.owner.addressDetail,
      companyName: subscription.owner.companyName,

      client: subscription.client,

      owner: subscription.owner._id,
      server: subscription.owner.server,
    };

    let createdInvoice;

    Invoice.create(invoice)
      .then((invoice) => {
        createdInvoice = invoice;

        if (creditCard)
          return TransactionService.transact(invoice, creditCard, true);
        else
          return TransactionService.payWithExistingCard(invoice, subscription.creditCard);
      })
      .then(payment => {

        let startTime = Moment().startOf('day');
        let endTime = Moment().startOf('day').month(startTime.month() + 1);

        // 연체료
        if (subscription.overDueDate) {
          startTime.date(startTime.date() - subscription.overDueDate);
          endTime.date(endTime.date() - subscription.overDueDate);
        }


        let newSubscription = {
          client: subscription.client,
          invoice: createdInvoice._id,
          price: priceToPay,
          purchasedTime: createdInvoice.createdAt,
          startTime: startTime,
          endTime: endTime,
          currentPlan: plan.toObject(),
          plan: plan._id,

          creditCard: subscription.creditCard,

          owner: subscription.owner._id,
          previousSubscription: subscription._id
        };

        if (subscription.renew === 'Schedule Cancel')
          newSubscription.renew = 'Off';

        if (creditCard)
          newSubscription.creditCard = payment.creditCard;

        return Subscription.create(newSubscription);
      })
      .then(subscription => {
        resolve(subscription);
      })
      .catch(err => {
        if (err.name === 'MongoError' || err instanceof Mongoose.Error)
          reject(err);

        if (newPlan)
          reject(new Error("F112"));

        // TODO need to send mail
        if (subscription.overDueDate == 0
          || subscription.overDueDate == 2
          || subscription.overDueDate == 4
          || subscription.overDueDate == 6
          || subscription.overDueDate == 8
          || subscription.overDueDate == 11) {

        }


        if (subscription.overDueDate == 14) {
          subscription.status = 'Stop';
        }

        subscription.overDueDate++;
        subscription.save()
          .then((subscription) => {
            resolve(subscription);
          });
      });
  });
}


/**
 *
 * 구독을 중지한다
 * - 중지로 업데이트
 *
 * @param req
 * @param res
 */
function cancel(id) {
  return Subscription.findOne({_id: id, status: "Start"})
    .then(subscription => {
      if (!subscription)
        throw new Error("SubscriptionNotFound");

      subscription.status = 'Stop';
      return subscription.save();
    });
}
