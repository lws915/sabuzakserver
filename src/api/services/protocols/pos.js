'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

//(?=.*[a-z]) The string must contain at least 1 lowercase alphabetical character
//(?=.*[A-Z]) The string must contain at least 1 uppercase alphabetical character
//(?=.*[0-9]) The string must contain at least 1 numeric character
//(?=.*[!@#\$%\^&\*]) The string must contain at least one special character, but we are escaping reserved RegEx characters to avoid conflict
//(?=.{8,}) The string must be eight characters or longer
//(?=.*[a-zA-Z]) One alphabet
let passwordReg = new RegExp("^\\d{8,}$");



function protocols() {
}

let logger = AppZet.logger('POSStrategy');

protocols.prototype = Object.create({
  register: register,
  login: login,
  createUser: createUser,
});

module.exports = new protocols();


/**
 * Local Authentication Protocol
 *
 * The most widely used way for websites to authenticate users is via a username
 * and/or email as well as a password. This module provides functions both for
 * registering entirely new users, assigning passwords to already registered
 * users and validating login requesting.
 *
 * For more information on local authentication in Passport.js, check out:
 * http://passportjs.org/guide/username-password/
 */

/**
 * @param {Object}   req
 * @param {Object}   res
 * @param {Function} next
 */
function register(req, res, next) {
  let user = {
    identifier: req.body.identifier,
    email: req.body.identifier,
    password: req.body.password,
    server: req.server
  };

  createUser(user)
    .then((createdUser)=> {
      next(null, createdUser);
    })
    .catch((err)=> {
      next(err);
    });
}


/**
 * Validate a login request
 *
 * Looks up a user using the supplied identifier (email or username) and then
 * attempts to find a local Passport associated with the user. If a Passport is
 * found, its password is checked against the password supplied in the form.
 *
 * @param {Object}   req
 * @param {string}   identifier
 * @param {string}   password
 * @param {Function} next
 */
function login(req, identifier, password, next) {
  let query = {};
  let serverId = parseInt(identifier.substring(0, 5));



  if (serverId === undefined
    || serverId === null
    || !Number.isInteger(serverId)) return next(null, false);
  else query.server = serverId;

  query.identifier = identifier;

  User.findOne(query, function (err, user) {
    if (err) return next(err);

    if (!user) {
      return next(null, false, new Error('Error.Passport.identifier.NotFound'));
    }

    Passport.findOne({owner: user._id, server: user.server, provider: 'appzet'},
      function (err, passport) {
        if (passport) {
          passport.validatePassword(password, function (err, res) {
            if (err) return next(err);

            if (!res) return next(null, false, new Error('Error.Passport.Password.Wrong'));
            else return next(null, user, passport);

          });
        } else return next(null, false, new Error('Error.Passport.Password.NotSet'));

      });
  });
}


/**
 * Register a new user
 *
 * This method creates a new user from a specified email, username and password
 * and assign the newly created user a local Passport.
 *
 * @param {String}   username
 * @param {String}   email
 * @param {String}   password
 * @param {Function} next
 */

function createUser(_user) {
  return new Promise((resolve, reject)=> {

    let password = _user.password;
    delete _user.password;

    _user.role = "포스 사용자";

    if (!password) {
      logger.log('silly', 'no password');
      return reject(new Error("password"));
    }

    if (!passwordReg.test(password)) {
      logger.log('silly', 'invalid password');
      return reject(new Error("password"));
    }

    let createdUser, createdPassport;

    User.create(_user)
      .then(user=> {
        createdUser = user;

        return Passport.create({
          protocol: 'pos',
          password: password,
          owner: createdUser._id,
          server: createdUser.server
        });
      })
      .then(passport=> {
        createdPassport = passport;
        resolve(createdUser);
      })
      .catch(err=> {
        if (createdUser && createdUser._id) User.remove(createdUser._id, ()=> {}).exec();
        reject(err);
      });
  });
}