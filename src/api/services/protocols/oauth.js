'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */


module.exports = function (req, token, tokenSecret, profile, next) {
  let query    = {
      identifier : profile._id
    , protocol   : 'oauth'
    , tokens     : { token: token }
    };

  if (tokenSecret !== undefined) {
    query.tokens.tokenSecret = tokenSecret;
  }

  sails.services.passport.connect(req, query, profile, next);
};
