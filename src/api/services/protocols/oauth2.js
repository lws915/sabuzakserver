'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */


module.exports = function (req, accessToken, refreshToken, profile, next) {
  let query    = {
      identifier : profile._id
    , protocol   : 'oauth2'
    , tokens     : { accessToken: accessToken }
    };

  if (refreshToken !== undefined) {
    query.tokens.refreshToken = refreshToken;
  }

  PassportService.connect(req, query, profile, next);
};
