'use strict';

/**
 * Created by sungwookim on 01/31/2017
 * As part of appzetserver
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by sungwookim <ksw1652@gmail.com>, 01/31/2017
 *
 */

let Promise = require('bluebird');

let logger = AppZet.logger('CouponService');

module.exports = {
  issueWithPasscode: issueWithPasscode,
  issueWithBarcode: issueWithBarcode,
  use: use,


};

function issueWithPasscode(userId, couponId, passcode, serverId) {
  return new Promise((resolve, reject) => {
    if (!couponId)
      throw new Error("NotMatchCoupon");

    if (!passcode)
      throw new Error("NotExistPasscode");

    return ServerService.checkPasscode(serverId, passcode, userId)
      .then(checkWrapper => {
        return Promise.all([
          Coupon.findOne({_id: couponId}),
          Device.findOne({owner: userId}),
          Client.findOne({server: serverId})
        ]);
      })
      .spread((coupon, device, client) => {
        let issuedCoupon = {};

        issuedCoupon.coupon = couponId;
        issuedCoupon.server = serverId;
        issuedCoupon.couponSnapshot = coupon;

        issuedCoupon.createdBy = userId;
        issuedCoupon.updatedBy = userId;
        issuedCoupon.owner = userId;

        let devices = [];
        devices.push(device);

        coupon.issued++;
        coupon.save();

        return Promise.all([
          CouponIssued.create(issuedCoupon),
          PushService.sendToDevices(client, devices, "사용 완료", "쿠폰 사용이 완료되었습니다.", {type: "COUPON_USED_PASSCODE"})
        ]);
      })
      .spread((couponIssued, pushResult) => {
        resolve(couponIssued);
      })
      .catch((error) => {
        reject(error);
      })
  });
}

function issueWithBarcode(getBarcode, serverId) {
  return new Promise((resolve, reject) => {
    if (!getBarcode)
      throw new Error("NotExistBarcode");

    let barcode = getBarcode.split(' ');
    let deviceIndex = _.toInteger(barcode[3]);

    barcode.splice(barcode.length - 1, 1);
    barcode = _.join(barcode, " ");

    let queryPromise = Device.findOne({_id: deviceIndex});

    queryPromise
      .populate('owner')
      .then((device) => {
        return Promise.all([
          device,
          Coupon.findOne({barcode: barcode}),
          Client.findOne({server: serverId})
        ]);
      })
      .spread((device, coupon, client) => {
        if (!device)
          throw new Error("DeviceNotFound");
        if (!device.owner.identifier == "web-virtual-device")
          throw new Error("WebUserNotAllow");


        let issuedCoupon = {};

        issuedCoupon.coupon = coupon._id;
        issuedCoupon.server = serverId;
        issuedCoupon.couponSnapshot = coupon;

        issuedCoupon.createdBy = device.owner._id;
        issuedCoupon.updatedBy = device.owner._id;
        issuedCoupon.owner = device.owner._id;

        coupon.issued++;
        coupon.save();

        let devices = [];
        devices.push(device);

        return Promise.all([
          CouponIssued.create(issuedCoupon),
          PushService.sendToDevices(client, devices, "사용 완료", "쿠폰 사용이 완료되었습니다.", {type: "COUPON_USED_BARCODE"})
        ]);
      })
      .spread((couponIssued, pushResult) => {
        resolve(couponIssued);
      })
      .catch((error) => {
        reject(error);
      })
  });
}

function use(couponIssued) {
  return new Promise((resolve, reject) => {
    if (!couponIssued) {
      return reject({message: 'NotMatchCouponIssued'});
    } else {
      CouponIssued.findOne({_id: couponIssued._id})
        .populate({path: 'owner', populate: {path: 'devices'}})
        .populate({path: 'coupon'})
        .then((couponIssued => {
          if (!couponIssued)
            return reject({message: 'NotMatchCouponIssued'});
          if (!couponIssued.coupon)
            return reject({message: 'NotMatchCoupon'});

          couponIssued.status = 'Used';
          couponIssued.usedDate = new Date();
          couponIssued.save();

          couponIssued.coupon.used++;
          couponIssued.coupon.save();

          return resolve(couponIssued);
        }));
    }
  });
}

