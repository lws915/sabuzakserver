'use strict';

/**
 * Created by sungwookim on 09/08/2017
 * As part of welaserver
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by sungwookim <ksw1652@gmail.com>, 09/08/2017
 *
 * Updater    수정자 - 작성자이름 09/08/2017
 */


module.exports = {
  checkPostAvailability: checkPostAvailability,
  checkReplyAvailability: checkReplyAvailability,
};


function checkPostAvailability(post, user) {

  return new Promise((resolve, reject) => {
    // if (typeof post.owner === 'object') ownerId = post.owner._id;
    // else ownerId = post.owner;

    post.populate('owner').execPopulate()
      .then(post => {
        // 자신 게시글
        if (user._id == post.owner._id) return resolve();
        // 관리자 권한
        if (user.role == "관리자") return resolve();

        if ((user.role == "정회원 선생님" || user.role == "준회원 선생님") && post.owner.role == "학생") {
          if (post.owner.campus == user.campus) return resolve();
          else return reject({message: 'NoPermission'});
        } else
          return reject({message: 'NoPermission'});
      });
  });
}

function checkReplyAvailability(reply, user) {
  return new Promise((resolve, reject) => {
    reply
      .populate('post')
      .populate('owner')
      .execPopulate()
      .then(reply => {
        // 자신 게시글
        if (user._id == reply.owner._id) return resolve();

        // 관리자 권한
        if (user.role == "관리자") return resolve();

        if ((user.role == "정회원 선생님" || user.role == "준회원 선생님") && reply.owner.role == "학생") {
          if (reply.owner.campus == user.campus) return resolve();
        }

        if (user.role == "학생" && reply.post.owner == user._id) return resolve();
        else return reject({message: 'NoPermission'});
      });
  });
}