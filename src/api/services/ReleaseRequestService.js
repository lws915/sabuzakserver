'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */


const uuidV1 = require('uuid/v1');

module.exports = {
  makePayment: makePayment,
  createApplication: createApplication,

  validateStatus: validateStatus,
};

function makePayment(req, releaseRequestId) {

  let amount = req.body.amount;
  let planId = req.body.planId;
  let templateId = req.body.templateId;
  let discountCode = req.body.discountCode;

  // 카드 정보
  let creditCard = {
    card_number: req.body.card_number,
    expiry: req.body.expiry,
    birth: req.body.birth,
    pwd_2digit: req.body.pwd_2digit
  };

  let validationPromises = [
    ReleaseRequest.findOne({_id: releaseRequestId}),
    Template.findOne({_id: templateId,  status: "Published"} ),
    Plan.findOne({_id: planId})
  ];

  if (discountCode)
    validationPromises.push(Discount.findOne({
      code: discountCode,
      status: 'Unused',
      isDeleted: false,
    }));

  let createdInvoice, createdPayment, foundDiscount,
    foundReleaseRequest, foundTemplate, foundPlan;

  return Promise.all(validationPromises)
    .spread((releaseRequest, template, plan, discount) => {
      if (!releaseRequest) throw new Error('ReleaseRequestNotFound');
      if (!template) throw new Error('TemplateNotFound');
      if (!plan) throw new Error('PlanNotFound');
      if (discountCode && !discount)throw new Error('DiscountNotFound');

      foundReleaseRequest = releaseRequest;
      foundTemplate = template;
      foundPlan = plan;

      // TODO: 결제 후 도메인 만들기

      let templatePrice = foundTemplate.salePrice || foundTemplate.price;
      let planPrice = foundPlan.salePrice || foundPlan.price;

      let originalPrice = (templatePrice + planPrice);

      let discountPrice = 0;

      foundDiscount = discount;
      if (foundDiscount) {

        if (discount.percentOff) {
          discountPrice = originalPrice / 100 * discount.percentOff;
        }
        else {
          discountPrice = discount.costOff;
        }
      }

      let totalPrice = originalPrice - discountPrice;

      if (totalPrice !== amount) throw new Error('AmountMissMatch');

      let invoice = {
        type: 'Normal',

        // 상품정보
        totalPrice: totalPrice,
        originalPrice: originalPrice,
        discountPrice: discountPrice,
        templates: [foundTemplate._id],
        plans: [foundPlan._id],

        itemList: {
          templates: [foundTemplate.toObject()],
          plans: [foundPlan.toObject()],
        },

        // 개인 정보
        email: req.body.email,
        fullName: req.body.fullName,
        phone: req.body.phone,
        address: req.body.address,
        addressDetail: req.body.addressDetail,
        companyName: req.body.companyName,

        owner: req.user._id,
        server: req.user.server
      };

      if (foundDiscount) {
        invoice.itemList.discounts = [discount.toObject()];
      }


      return Invoice.create(invoice);
    })
    .then(invoice => {
      createdInvoice = invoice;
      return TransactionService.transact(createdInvoice, creditCard, true);
    })
    .then(payment => {
      foundReleaseRequest.invoice = createdInvoice._id;
      foundReleaseRequest.status = 'Payment Done';

      foundTemplate.bought++;

      let promises = [
        foundReleaseRequest.save(),
        foundTemplate.save()
      ];

      if (foundDiscount) {
        foundDiscount.status = 'Used';
        foundDiscount.invoice = createdInvoice._id;
        foundDiscount.owner = req.user._id;
        promises.push(foundDiscount.save());
      }

      return promises;
    })
    .spread(() => {
      return [createdInvoice, createdPayment];
    })
    .catch(err => {
      if (createdInvoice && createdInvoice._id)
        Invoice.remove({_id: createdInvoice._id}).exec();

      throw err;
    });
}

function createApplication(req, releaseRequestId, templateId) {
  let foundReleaseRequest, foundTemplate, createdServer, createdClient;

  return Promise.all([
    ReleaseRequest.findOne({_id: releaseRequestId, owner: req.user._id}),
    Template.findOne({_id: templateId, status: "Published"})
  ])
    .spread((releaseRequest, template) => {
      if (!releaseRequest) throw new Error('ReleaseRequestNotFound');
      if (!template) throw new Error('TemplateNotFound');

      foundReleaseRequest = releaseRequest;
      foundTemplate = template;

      return ServerService.generateApplication(
        foundTemplate,
        {owner: req.user._id},
        {owner: req.user._id, name: foundTemplate.name});
    })
    .spread((template, server, client) => {
      foundReleaseRequest.client = client._id;
      return foundReleaseRequest.save();
    })
    .catch(function (err) {
      // Handling Error
      if (createdServer && createdServer._id) Server.remove({_id: createdServer._id}).exec();
      if (createdClient && createdClient._id) Client.remove({_id: createdClient._id}).exec();

      throw err;
    });
}


/**
 *
 * 사용자 전용 상태 체크
 *
 * @param previous
 * @param next
 * @returns {boolean}
 */
function validateStatus(previous, next) {
  if (previous === "Preparation") return false;
  if (previous === "Release Request" && next === "Release Request") return false;
  if (previous === "Release In Progress" && next === "Release Request") return false;
  if (previous === "Released" && next === "Release Request") return false;
  if (previous === "Refunded" && next === "Release Request") return false;
  return true;
}

