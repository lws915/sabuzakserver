'use strict';
/**
 * Created by JongIn Koo on 10/11/2016
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by JongIn Koo <jiku90@naver.com>, 10/11/2016
 *
 */

let Promise = require('bluebird');
Promise.config({cancellation: true});

let logger = AppZet.logger('StampService');

let BARCODE_TYPE = {
  'STAMP': 0,
  'STAMP_COMPLETE': 1,
  'COUPON': 3
}

module.exports = {
  issue: issue,
  getStamps: getStamps,
  claim: claim
};
/**
 *
 *  Errors
 *  - PreviewClient
 *  - DeviceNotFound
 *  - CollectingStampCollectionExist
 *  - StampCardNotFound
 *
 * @param serverId
 * @param userId
 * @param uniqueDeviceId
 */
function issue(serverId, userId, uniqueDeviceId) {
  let createdStampCollection, foundDevice, foundStampCard;

  return Promise.all([
    Client.findOne({server: serverId}),
    StampCollection.find({
      status: 'Collecting',
      server: serverId,
      owner: userId
    }),
    StampCard.findOne({server: serverId}).populate('server'), // 기본으로 최신순으로 찾음
    Device.findOne({server: serverId, deviceId: uniqueDeviceId})
  ])
    .spread((client, stampCollections, stampCard, device) => {
      if (client.isPreview) throw new Error('PreviewClient');
      if (!device) throw new Error('DeviceNotFound');
      if (stampCollections.length > 0) throw new Error('CollectingStampCollectionExist');
      if (!stampCard) throw new Error('StampCardNotFound');

      foundDevice = device;
      foundStampCard = stampCard;

      let snapshot = {};
      snapshot.maxStamps = foundStampCard.maxStamps;
      snapshot.policyDesc = foundStampCard.policyDesc;
      snapshot.policyTitle = foundStampCard.policyTitle;
      snapshot.rewardContent = foundStampCard.rewardContent;
      snapshot.name = foundStampCard.name;
      snapshot.server = serverId;

      let stampCollection = {
        server: serverId,
        owner: userId,
        createdBy: userId,
        updatedBy: userId,
      };

      stampCollection.stampCard = foundStampCard._id;
      stampCollection.stampCardSnapShot = snapshot;

      return StampCollection.create(stampCollection);
    })
    .then((stampCollection) => {
      createdStampCollection = stampCollection;

      createdStampCollection.barcode =
        ServerService.createBarcodeNumber(serverId, BARCODE_TYPE.STAMP, foundDevice._id, createdStampCollection._id);

      foundStampCard.issued++;
      return Promise.all([stampCollection.save(), foundStampCard.save()]);
    })
    .spread((stampCollection) => {
      return stampCollection.populate('stamps').execPopulate();
    })
    .catch(function (err) {
      if (createdStampCollection && createdStampCollection._id) StampCollection.remove({_id: createdStampCollection._id}).exec();
      throw err;
    });
}

/**
 *
 * stamp 세가지 일을 한다
 * 1. stamp를 stampCollection에 추가 한다 (모으는 중)
 * 2. stampCollection이 완성 되면 상태 수정 및 새로운 바코드 발급 (완료 상태)
 * 3. stampCollection의 개체수 보다 더 많은 수로 도장을 추가 할시
 *     1번 + 2번 그리고 새로운 stampCollection 만든 후 추가된 만큼 stamp 추가.
 *
 *  Errors
 *  - StampCollectionNotFound
 *  - StampCardSnapShotNotFound
 *
 *
 * @param query
 * @param quantity
 */
function getStamps(query, quantity) {
  //query는 StampCollection을 찾을수 있는 조건이어야 한다.

  if (!quantity) quantity = 1;
  if (quantity > 20) quantity = 20;

  query.status = 'Collecting';

  let foundStampCollection;

  return StampCollection.findOne(query)
    .populate('stamps')
    .populate('stampCard')
    .populate({path: 'owner', populate: {path: 'devices'}})
    .then((stampCollection) => {
      if (!stampCollection) throw new Error('StampCollectionNotFound');
      if (!stampCollection.stampCardSnapShot) throw new Error('StampCardSnapShotNotFound');

      foundStampCollection = stampCollection;

      // 남은 갯수
      let remainingStamps =
        stampCollection.stampCardSnapShot.maxStamps >= stampCollection.stamps.length ?
          stampCollection.stampCardSnapShot.maxStamps - stampCollection.stamps.length : 0;

      let promiseChain = Promise.resolve(foundStampCollection);

      // 1번 case + // 2번 case
      _.times(remainingStamps > quantity ? quantity : remainingStamps, () => {
        promiseChain = promiseChain.then((stampCollection) => {
          return stamp(stampCollection);
        });
      });

      if (remainingStamps < quantity) {
        // 3번 case
        quantity -= remainingStamps;

        let maxStamps = foundStampCollection.stampCard.maxStamps;
        let newCards = Math.floor(quantity / maxStamps);
        let remainder = quantity % maxStamps;
        let serverId = foundStampCollection.server;
        let userId = foundStampCollection.owner._id;
        let uniqueDeviceId = foundStampCollection.owner.devices[0].deviceId;


        // 받자 마자 완료 될 새카드
        _.times(newCards, () => {
          promiseChain = promiseChain.then((stampCollection) => {
            return issue(serverId, userId, uniqueDeviceId);
          });
          _.times(maxStamps, () => {
            promiseChain = promiseChain.then((stampCollection) => {
              return stamp(stampCollection);
            });
          });
        });

        // 마지막 새카드 발급
        promiseChain = promiseChain.then((stampCollection) => {
          return issue(serverId, userId, uniqueDeviceId);
        });

        // 남은 스탬프들
        _.times(remainder, () => {
          promiseChain = promiseChain.then((stampCollection) => {
            return stamp(stampCollection);
          });
        });
      }

      return promiseChain;
    })
    .then((stampCollection) => {

      if (foundStampCollection.owner
        && foundStampCollection.owner.devices
        && foundStampCollection.owner.devices.length > 0) {
        Client.findOne({server: foundStampCollection.server})
          .then((client) => {
            PushService
              .sendToDevices(client, foundStampCollection.owner.devices,
                "적립 완료", "스탬프가 적립 되었습니다.",
                {type: "STAMP"});
          })
          .catch((err) => {
            logger.log('error', err);
          });
      }

      return foundStampCollection;
    });
}

function claim(query) {
  query.status = 'Complete';
  let foundStampCollection;

  return StampCollection.findOne(query)
    .populate('stamps')
    .populate('stampCard')
    .populate({path: 'owner', populate: {path: 'devices'}})
    .then((stampCollection) => {
      if (!stampCollection) throw new Error('StampCollectionNotFound');

      foundStampCollection = stampCollection;
      foundStampCollection.status = 'Claimed';
      foundStampCollection.claimedAt = new Date();

      foundStampCollection.stampCard.claimed++;

      return [
        foundStampCollection.save(),
        foundStampCollection.stampCard.save()
      ];
    })
    .spread(() => {
      if (foundStampCollection.owner
        && foundStampCollection.owner.devices
        && foundStampCollection.owner.devices.length > 0) {
        Client.findOne({server: foundStampCollection.server})
          .then((client) => {
            PushService.sendToDevices(client, foundStampCollection.owner.devices,
              "적립 완료", "스탬프가 모두 적립되었습니다. 보관함을 확인해주세요.", {type: "STAMP_CLAIM"});
          })
          .catch((err) => {
            logger.log('error', err);
          });
      }

      return foundStampCollection;
    });
}


/******************************
 *
 *       PRIVATE METHOD
 *
 ******************************/


function stamp(stampCollection) {
  return Stamp.create({
    stampCollection: stampCollection._id,
    server: stampCollection.server,
    owner: stampCollection.owner._id || stampCollection.owner
  })
    .then(stamp => {

      if (stampCollection.stamps.length >= (stampCollection.stampCardSnapShot.maxStamps - 1)) {
        if (stampCollection.status && stampCollection.status == 'Collecting') {
          stampCollection.status = 'Complete';
          stampCollection.completedAt = new Date();

          // 나중에 바코드 상태 업데이트 API가 생길수도 있을거 같음
          stampCollection.barcode =
            stampCollection.barcode.substring(0, 6)
            + BARCODE_TYPE.STAMP_COMPLETE
            + stampCollection.barcode.substring(6 + 1);
        }
      }
      return stampCollection.save();
    })
    .then((stampCollection) => {
      return stampCollection.populate("stamps").execPopulate();
    });
}

