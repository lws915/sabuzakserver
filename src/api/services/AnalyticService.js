/**
 * Created by andy on 3/27/17
 * As part of appzetserver
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & andy - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by andy <>, 3/27/17
 *
 */


let logger = AppZet.logger('StampCollectionController');

module.exports = {
  visitTotal: visitTotal,
  visit: visit,
  firstVisit: firstVisit,
  rankFeature: rankFeature,

  stampCount: stampCount,
  stampRewardCount: stampRewardCount,

  couponUsedCount: couponUsedCount,
  getCouponRank: getCouponRank,

  eventAdmissionCount: eventAdmissionCount,
  getEventRank: getEventRank
};


/**
 *
 * 방문수는 아래와 같은 조건으로 count가 된다
 * - 앱이나 포스 기계를 통해 들어오는 요청을 기반으로 count
 * - 앱 같은 경우 스탬프 찍기, 사용, 쿠폰 사용 한에서만 count
 * - 같은 손님이 1시간안에 최대 1번 방문 가능
 *  case1 손님1이 1시간에 20번의 요청을 보내도 1번 방문으로 취급 한다.
 *  case2 손님1당 하루에 24번까지 방문 가능 하다. (정신병자만 가능 할듯)
 *
 */

/**
 *
 * 앱 방문자 수
 *
 * @param serverId - 서버 아이디
 * @param userId - 고객 아이디
 * @param deviceId -  고객 기기아이디 (X UUID)
 * @param fromTo - {}
 * @returns {Promise<TResult>|Promise|Promise.<TResult>}
 */
function visitTotal(serverId, query) {

  let matchingQuery = {
    "$match": {
      server: serverId,
      "$or": [
        {
          controller: {
            "$in": [
              "StampCollectionController",
              "CouponController"
            ]
          },
          api: {
            "$in": [
              "passcodeIssueAndUse",
              "stampPasscode",
              "claimPasscode",
            ]
          }
        },
        {
          controller: {
            "$in": [
              "StampCollectionController",
              "CouponController"
            ]
          },
          api: {
            "$in": [
              "barcodeIssueAndUse",
              "stampBarcode",
              "claimBarcode",
            ]
          }
        },
      ]
    }
  };


  if (query) matchingQuery.$match = _.assignIn(matchingQuery.$match, query);

  return RequestLog.aggregate([
    // 서버를 조건으로 작은 data 내에서 count
    matchingQuery,
    {
      "$group": {
        _id: {
          year: {$year: "$createdAt"},
          month: {$month: "$createdAt"},
          day: {$dayOfMonth: "$createdAt"},
          hour: {$hour: "$createdAt"},
          owner: "$owner"
        }
      }
    },
    {
      "$count": "visit"
    }
  ])
    .then(result => {
      if (result && result[0]) return result[0].visit;
      return 0;
    });
}

/**
 *
 * 방문수는 아래와 같은 조건으로 count가 된다
 * - 앱이나 포스 기계를 통해 들어오는 요청을 기반으로 count
 * - 앱 같은 경우 스탬프 찍기, 사용, 쿠폰 사용 한에서만 count
 * - 같은 손님이 1시간안에 최대 1번 방문 가능
 *  case1 손님1이 1시간에 20번의 요청을 보내도 1번 방문으로 취급 한다.
 *  case2 손님1당 하루에 24번까지 방문 가능 하다. (정신병자만 가능 할듯)
 *
 */

/**
 *
 * 앱 방문자 수
 *
 * @param serverId - 서버 아이디
 * @param userId - 고객 아이디
 * @param deviceId -  고객 기기아이디 (X UUID)
 * @param fromTo - {}
 * @returns {Promise<TResult>|Promise|Promise.<TResult>}
 */
function visit(serverId, userId, deviceId, query) {

  let barcodeRegex = new RegExp("^.*" + LPAD(deviceId + "", '0', 6));

  let matchingQuery = {
    "$match": {
      server: serverId,
      "$or": [
        {
          owner: userId,
          controller: {
            "$in": [
              "StampCollectionController",
              "CouponController"
            ]
          },
          api: {
            "$in": [
              "passcodeIssueAndUse",
              "stampPasscode",
              "claimPasscode",
            ]
          }
        },
        {
          "body.barcode": {$regex: barcodeRegex},
          controller: {
            "$in": [
              "StampCollectionController",
              "CouponController"
            ]
          },
          api: {
            "$in": [
              "barcodeIssueAndUse",
              "stampBarcode",
              "claimBarcode",
            ]
          }
        },
      ]
    }
  };


  if (query) matchingQuery.$match = _.assignIn(matchingQuery.$match, query);

  return RequestLog.aggregate([
    // 서버를 조건으로 작은 data 내에서 count
    matchingQuery,
    {
      "$group": {
        _id: {
          year: {$year: "$createdAt"},
          month: {$month: "$createdAt"},
          day: {$dayOfMonth: "$createdAt"},
          hour: {$hour: "$createdAt"},
          owner: "$owner"
        }
      }
    },
    {
      "$count": "visit"
    }
  ])
    .then(result => {
      if (result && result[0]) return result[0].visit;
      return 0;
    });
}

function firstVisit(serverId, ownerId) {
  return Promise.all([
    Stamp.findOne({
      server: serverId,
      owner: ownerId
    }),
    CouponIssued.findOne({
      server: serverId,
      owner: ownerId
    })
  ])
    .spread((stamp, couponIssued) => {

      if (stamp && couponIssued) return Moment(stamp.createdAt) > (Moment(couponIssued.createdAt)) ?
        couponIssued.createdAt : stamp.createdAt;
      if (stamp) return stamp.createdAt;
      if (couponIssued) return couponIssued.createdAt;

      return null;
    });
}

function rankFeature(serverId, owner) {


  let stampQuery = {
    server: serverId,
    owner: {$ne: owner},
    controller: {"$in": ["StampCardController", "StampCollectionController"]},
    api: {
      "$in": ["issue", "findMyStampCollection",
        "findOne", "stampPasscode", "claimPasscode", "stampBarcode", "claimBarcode"]
    }
  };
  let couponQuery = {
    server: serverId,
    owner: {$ne: owner},
    controller: {"$in": ["CouponController", "CouponIssuedController"]},
    api: {
      "$in": ["count", "find", "findOne", "findExpired", "passcodeIssueAndUse",
        "findUnused", "barcodeIssueAndUse", "findUsed", "countIssued"]
    }
  };
  let galleryQuery = {
    server: serverId,
    owner: {$ne: owner},
    controller: {"$in": ["GalleryController"]},
    api: {"$in": ["count", "find", "findOne"]}
  };
  let eventQuery = {
    server: serverId,
    owner: {$ne: owner},
    controller: {"$in": ["EventController"]},
    api: {
      "$in": ["count", "find", "findOne", "findForOrderStatus", "submit",
        "getMyEventAdmissions"]
    }
  };
  let productQuery = {
    server: serverId,
    owner: {$ne: owner},
    controller: {"$in": ["ProductController"]},
    api: {"$in": ["count", "find", "findOne"]}
  };
  let introQuery = {
    server: serverId,
    owner: {$ne: owner},
    controller: {"$in": ["IntroController"]},
    api: {"$in": ["findOne"]}
  };
  let postQuery = {
    server: serverId,
    owner: {$ne: owner},
    controller: {"$in": ["PostController"]},
    api: {"$in": ["count", "find", "findOne",]}
  };


  return Promise.all([
    RequestLog.count(stampQuery),
    RequestLog.count(couponQuery),
    RequestLog.count(galleryQuery),
    RequestLog.count(eventQuery),
    RequestLog.count(productQuery),
    RequestLog.count(introQuery),
    RequestLog.count(postQuery)
  ])
    .then(result => {
      let features = _.map(result, (v, i) => {
        switch (i) {
          case 0:
            return {label: "stamp", usedCount: v};
          case 1:
            return {label: "coupon", usedCount: v};
          case 2:
            return {label: "gallery", usedCount: v};
          case 3:
            return {label: "event", usedCount: v};
          case 4:
            return {label: "product", usedCount: v};
          case 5:
            return {label: "intro", usedCount: v};
          case 6:
            return {label: "post", usedCount: v};
        }
      });

      return _.sortBy(features, function (v) {
        return -v.usedCount;
      });
    });
}

function stampCount(serverId, query) {
  let matchingQuery = {
    server: serverId,
  };

  if (query) matchingQuery = _.assignIn(matchingQuery, query);

  return Stamp.count(matchingQuery)
    .then(result => {
      return result;
    });
}

function stampRewardCount(serverId, query) {
  let matchingQuery = {
    server: serverId,
  };

  if (query) matchingQuery = _.assignIn(matchingQuery, query);

  return StampCollection.count(matchingQuery)
    .then(result => {
      return result;
    });
}

function couponUsedCount(serverId, query) {
  let matchingQuery = {
    server: serverId,
  };

  if (query) matchingQuery = _.assignIn(matchingQuery, query);

  return CouponIssued.count(matchingQuery)
    .then(result => {
      return result;
    });
}

function getCouponRank(serverId, startDate, endDate) {
  // let couponRankIds;
  let ranks;
  let couponRankIds;
  return CouponIssued.aggregate([
    {
      "$match": {
        server: serverId,
        usedDate: {
          $gte: new Date(startDate),
          $lt: new Date(endDate)
        }
      }
    },
    {
      "$group": {
        _id: "$coupon",
        count: {$sum: 1}
      }
    },
    {$sort: {"count": -1}},
    {$limit: 6}
  ])
    .then(result => {
      ranks = result;
      couponRankIds = _.map(result, "_id");

      return Coupon.find({_id: {$in: couponRankIds}})
    })
    .then(coupons => {
      let rankedCoupon = _.compact(_.map(ranks, (rankItem) => {
        let foundCoupon = _.find(coupons, {_id: rankItem._id});
        foundCoupon = foundCoupon.toObject();
        foundCoupon.usedCount = rankItem.count;
        return foundCoupon;
      }));

      return rankedCoupon;
    });
}


function eventAdmissionCount(serverId, query) {
  let matchingQuery = {
    server: serverId,
  };

  if (query) matchingQuery = _.assignIn(matchingQuery, query);

  return EventAdmission.count(matchingQuery)
    .then(result => {
      return result;
    });
}

function getEventRank(serverId, startDate, endDate) {
  let ranks;
  let eventRankIds;
  return EventAdmission.aggregate([
    {
      "$match": {
        server: serverId,
        createdAt: {
          $gte: new Date(startDate),
          $lt: new Date(endDate)
        }
      }
    },
    {
      "$group": {
        _id: "$event",
        count: {$sum: 1}
      }
    },
    {$sort: {"count": -1}},
    {$limit: 6}
  ])
    .then(result => {
      ranks = result;
      eventRankIds = _.map(result, "_id");

      return Event.find({_id: {$in: eventRankIds}})
    })
    .then(events => {
      let rankedEvent = _.compact(_.map(ranks, (rankItem) => {
        let foundEvent = _.find(events, {_id: rankItem._id});
        foundEvent = foundEvent.toObject();
        foundEvent.admissionCount = rankItem.count;
        return foundEvent;
      }));

      return rankedEvent;
    });
}

function LPAD(s, c, n) {
  if (!s || !c || s.length >= n) {
    return s;
  }
  let max = (n - s.length) / c.length;
  for (let i = 0; i < max; i++) {
    s = c + s;
  }
  return s;
}
