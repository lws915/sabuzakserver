'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 * Doc URL: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#abortMultipartUpload-property
 *
 */

let Promise = require('bluebird');
let fs = require('fs');


let parseString = require('xml2js').parseString;
//e.g.
// var xml = "<root>Hello xml2js!</root>"
// parseString(xml, function (err, result) {
//   console.dir(result);
// });

module.exports = {
  create: create,
};

function create(mobile) {

  let agent_code = AppZet.config.connections.safeNumber.agent_code;
  let comm_code = AppZet.config.connections.safeNumber.comm_code;
  let url = AppZet.config.connections.safeNumber.url;


  let body =
    "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><api>" +
    "<cell_phone_no>" + mobile + "</cell_phone_no> " +
    "<auth_type>S</auth_type> " +
    "<auth_date>" + Moment().format("yyyyMMddHHmmss") + "</auth_date> " +
    "<agent_code>" + agent_code + "</agent_code> " +
    "<comm_code>" + comm_code + "</comm_code> " +
    "<chg_050_yn>Y</chg_050_yn></api>";

  let postOption = {
    url: url,
    body: body,
    headers: {'Content-Type': 'text/xml'}
  };

  return Promise.promisify(Request.post, {multiArgs: true})(postOption)
    .then((data) => {

      return Promise.promisify(parseString)(data[1]);
    })
    .then((result) => {

      if (!result || !result.response || !result.response.number_050)
        throw new Error("FailToCreateSafeNumber");

      if (Array.isArray(result.response.number_050))
        return result.response.number_050[0];

      return result.response.number_050;
    });
}

