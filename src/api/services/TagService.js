/**
 * Created by andy on 3/27/17
 * As part of appzetserver
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & andy - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by andy <>, 3/27/17
 *
 */


let logger = AppZet.logger('TagService');

module.exports = {
  findOrCreate: findOrCreate,
};

function findOrCreate(tags) {
  let promises = [];

  _.forEach(tags, tagName => {
    let tag = {_id: tagName};

    promises.push(
      new Promise((resolve, reject) => {
        Tag.findOrCreate(tag, tag, (err, resultTag) => {
          if (err && err.code == 11000)
            return resolve(tag);

          if (err)
            return reject(err);

          return resolve(resultTag);
        });
      }));
  });

  return Promise.all(promises);

}