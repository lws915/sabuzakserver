'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

let logger = AppZet.logger('TransactionService');

let Promise = require('bluebird');
let iamport;
const uuidV1 = require('uuid/v1');

module.exports = {
  init: init,

  transact: transact,
  payWithExistingCard: payWithExistingCard,

  registerCreditCard: registerCreditCard,
  removeCreditCard: removeCreditCard,

  refund: refund,

  /** Private Methods **/
  // confirm: confirm,
  // cancel: cancel,
  // getCreditCard: getCreditCard,
  // registerCreditCard: registerCreditCard,
  // pay: pay,
  // schedule: schedule,
  // unschedule: unschedule
};

function init() {
  iamport = require('iamport')({
    impKey: AppZet.config.connections.iamport.impKey,
    impSecret: AppZet.config.connections.iamport.impSecret
  });
}

/******************
 * Public Methods
 ******************/

function transact(invoice, creditCard, rememberCard) {

  let createdPayment;

  // TODO: move this to transaction
  let payment = {
    name: 'AppZet 구매 ' + Moment().format('YYYY-MM-DD HH:mm:ss SSS'),
    merchant_uid: uuidV1(),
    amount: invoice.totalPrice,
    invoice: invoice._id,
    owner: invoice.owner,
    server: invoice.server
  };

  let customer_uid = uuidV1();

  return Payment.create(payment)
    .then((payment) => {

      createdPayment = payment;

      let transactionData = {
        customer_uid: customer_uid,
        merchant_uid: createdPayment.merchant_uid,
        name: createdPayment.name,
        amount: createdPayment.amount,
        card_number: creditCard.card_number,
        expiry: creditCard.expiry,
        birth: creditCard.birth
      };

      if (creditCard.pwd_2digit)
        transactionData.pwd_2digit = creditCard.pwd_2digit;


      // TODO: remove later 100 for test
      transactionData.amount = 100;

      return iamport.subscribe.onetime(transactionData);
    })
    .then((data) => {
      createdPayment = _.assignIn(createdPayment, data);

      let promise = [createdPayment.save()];

      if (rememberCard)
        promise.push(CreditCard.create({
          customer_uid: customer_uid,
          card_name: data.card_name,
          owner: createdPayment.owner,
          server: createdPayment.server
        }));

      return promise;
    })
    .spread((payment, creditCard) => {
      if (creditCard) {
        payment.creditCard = creditCard._id;
        return payment.save();
      }

      return payment;
    })
    .catch(err => {
      // Iamport
      // IAMPORT_-1
      //
      // 카드정보 인증에 실패하였습니다. [F112]유효하지않은 카드번호를 입력하셨습니다. (card_bin 없음)
      // IAMPORT_-1
      //
      // 카드정보 인증에 실패하였습니다. [F113]유효기간 오류
      // IAMPORT_-1
      //
      // 카드정보 인증에 실패하였습니다. [F113]주민번호 또는 사업자번호 입력오류(숫자만 가능)
      // IAMPORT_-1
      //
      // 카드정보 인증에 실패하였습니다. [F113]거래거절 비밀번호오류입력
      // TODO: 카드정보 인증에 실패하였습니다 [F113]해당카드 사용불가 가맹점(타카드 사용요망)
      // Param missing: pwd_2digit
      if (err.message.indexOf('F112') > -1
        || err.message.indexOf('F113') > -1
        || err.message.indexOf('Param missing: pwd_2digit') > -1) {
        err = new Error('InvalidCard');
      }

      if (createdPayment && createdPayment._id) {
        createdPayment.status = 'failed';
        createdPayment.save();
      }

      throw err;
    });
}


function payWithExistingCard(invoice, creditCardId) {

  let foundCard, createdPayment;

  return CreditCard.findOne({_id: creditCardId})
    .then((card) => {
      if (!card) throw new Error('CardNotExistError');

      foundCard = card;

      let payment = {
        name: 'AppZet 구매 ' + Moment().format('YYYY-MM-DD HH:mm:ss SSS'),
        merchant_uid: uuidV1(),
        amount: invoice.totalPrice,
        invoice: invoice._id,
        creditCard: foundCard._id,

        owner: invoice.owner,
        server: invoice.server
      };

      return Payment.create(payment);
    })
    .then((payment) => {
      createdPayment = payment;

      let transactionData = {
        customer_uid: foundCard.customer_uid,
        merchant_uid: createdPayment.merchant_uid,
        amount: createdPayment.amount
      };

      // TODO: remove later 100 for test
      transactionData.amount = 100;

      return iamport.subscribe.again(transactionData);
    })
    .then((data) => {
      createdPayment = _.assignIn(createdPayment, data);

      return createdPayment.save();
    })
    .catch(err => {
      // Iamport
      // IAMPORT_-1
      //
      // 카드정보 인증에 실패하였습니다. [F112]유효하지않은 카드번호를 입력하셨습니다. (card_bin 없음)
      // IAMPORT_-1
      //
      // 카드정보 인증에 실패하였습니다. [F113]유효기간 오류
      // IAMPORT_-1
      //
      // 카드정보 인증에 실패하였습니다. [F113]주민번호 또는 사업자번호 입력오류(숫자만 가능)
      // IAMPORT_-1
      //
      // 카드정보 인증에 실패하였습니다. [F113]거래거절 비밀번호오류입력
      // Param missing: pwd_2digit
      if (err.message.indexOf('F112') > -1
        || err.message.indexOf('F113') > -1
        || err.message.indexOf('Param missing: pwd_2digit') > -1) {
        err = new Error('InvalidCard');
      }

      if (createdPayment && createdPayment._id) {
        createdPayment.status = 'failed';
        createdPayment.save();
      }

      throw err;
    });
}


function registerCreditCard(creditCard, owner, server) {
  return Promise.resolve()
    .bind({})
    .then(() => {
      creditCard.customer_uid = uuidV1();
      return iamport.subscribe_customer.create(creditCard)
    })
    .then((data) => {
      //   "card_name": "하나SK카드",
      //   "customer_addr": null,
      //   "customer_email": null,
      //   "customer_name": null,
      //   "customer_postcode": null,
      //   "customer_tel": null,
      //   "customer_uid": "customer_1234",
      //   "inserted": 1486468459,
      //   "updated": 1486468459

      data.owner = owner;
      data.server = server;
      return CreditCard.create(data);
    });
}

function removeCreditCard(customer_uid) {
  return Promise.resolve()
    .bind({})
    .then(() => {
      // { card_name: '하나SK카드',
      //   customer_addr: null,
      //   customer_email: null,
      //   customer_name: null,
      //   customer_postcode: null,
      //   customer_tel: null,
      //   customer_uid: '335ada91-29ae-11e7-bdc6-efd48c83a3ac',
      //   inserted: 1493121393,
      //   updated: 1493121393 }

      return iamport.subscribe_customer.delete({customer_uid: customer_uid});
    });
  // .then((data) => {
  //   console.log(data);
  // })
  // .catch((err) => {
  //   console.log(err);
  // });
}


/**
 *
 * 환불은 템플릿과 플랜 모두
 * 24시간 이내에만 가능
 *
 * @param invoiceId
 */
function refund(clientId, reason) {

  let foundInvoices;

  return Client.findOne({_id: clientId})
    .populate('domainRef')
    .populate({path: 'subscriptions', match: {status: 'Start'}})
    .populate('releaseRequestRef')
    .populate({
      path: 'invoices',
      options: {sort: {createdAt: 1}},
      populate: {path: 'payments'},
    })
    .then((client) => {
      if (!client) throw new Error("ClientNotFound");
      if (!client.releaseRequestRef) throw new Error("ReleaseRequestNotFound");
      if (!client.invoices
        || !client.invoices[0]) throw new Error("InvoiceNotFound");

      _.forEach(client.invoices, (invoice) => {
        if (!invoice.payments
          || !invoice.payments[0])
          throw new Error("PaymentsNotFound");
      });

      if (!client.subscriptions
        || !client.subscriptions[0]) throw new Error("SubscriptionNotFound");

      let diffHours = Moment().diff(client.releaseRequestRef.invoice.createdAt, 'hours');
      if (diffHours > 23) throw new Error("ExceedTimeLimit");

      client.invoices[0].refundReason = reason;
      client.releaseRequestRef.status = 'Refunded';
      client.subscriptions[0].status = 'Stop';

      let promises = [
        client.releaseRequestRef.save(),
        client.subscriptions[0].save(),
        client.domainRef.remove(),
        client.remove()
      ];

      foundInvoices = client.invoices;

      _.forEach(foundInvoices, (invoice) => {
        invoice.refundRequestedAt = new Date();
        promises.push(invoice.save());
        if (invoice.payments[0].status === 'paid') {
          invoice.payments[0].status = "cancel_requested";
          promises.push(invoice.payments[0].save());
        }
      });

      return promises;
    })
    .spread((releaseRequest) => {
      _.forEach(foundInvoices, (invoice) => {
        cancel(invoice.payments[0]._id);
      });

      return releaseRequest;
    });
}

/******************
 * Private Methods
 ******************/


//  요녀석 사용 해서 결제 밎 저장 동시에 해결
// subscribe/payments/onetime
//   Sent
//   merchant_uid           - 가맹점 거래 고유번호
//   amount                 - 결제금액
//   vat                    - 결제금액 중 부가세 금액(파라메터가 누락되면 10%로 자동 계산됨)
//   card_number            - 카드번호(dddd-dddd-dddd-dddd)
//   expiry                 - 카드 유효기간(YYYY-MM)
//   birth                  - 생년월일6자리(법인카드의 경우 사업자등록번호10자리)
//   pwd_2digit             - 카드비밀번호 앞 2자리(법인카드의 경우 생략가능)
//   customer_uid           - string 타입의 고객 고유번호 (저장 해야 다음번에 사용 가능)
//   name                   - 주문명
//   buyer_name             - 주문자명
//   buyer_email            - 주문자 E-mail주소
//   buyer_tel              - 주문자 전화번호
//   buyer_addr             - 주문자 주소
//   buyer_postcode         - 주문자 우편번호
//   card_quota             - 카드할부개월수. 2 이상의 integer 할부개월수 적용(결제금액 50,000원 이상 한정)


//   response
//   "amount": 500,
//   "apply_num": "05995745",
//   "buyer_addr": null,      --> customer_addr
//   "buyer_email": null,     --> customer_email
//   "buyer_name": null,      --> customer_name
//   "buyer_postcode": null,  --> customer_postcode
//   "buyer_tel": null,       --> customer_tel
//   "cancel_amount": 0,
//   "cancel_reason": null,
//   "cancel_receipt_urls": [],
//   "cancelled_at": 0,
//   "card_name": "하나SK카드",
//   "card_quota": 0,
//   "currency": "KRW",
//   "custom_data": null,
//   "escrow": false,
//   "fail_reason": null,
//   "failed_at": 0,
//   "imp_uid": "imps_660015896512",
//   "merchant_uid": "11111111",
//   "name": null,
//   "paid_at": 1486470015,
//   "pay_method": "card",
//   "pg_provider": "nice",
//   "pg_tid": "nictest04m01161702072120148542",
//   "receipt_url": "https://pg.nicepay.co.kr/issue/IssueLoader.jsp?TID=nictest04m01161702072120148542&type=0",
//   "status": "paid",
//   "user_agent": "sorry_not_supported_anymore",
//   "vbank_date": 0,
//   "vbank_holder": null,
//   "vbank_name": null,
//   "vbank_num": null


function confirm(merchantUid) {
  return Promise.all([
    Payment.findOne({_id: merchantUid}),
    iamport.payment.getByMerchant({merchant_uid: merchantUid})
  ])
    .bind({})
    .spread(function (payment, iamportPayment) {

      if (!payment) throw new Error('PaymentNotExistError');
      if (!iamportPayment) throw new Error('PGServiceDown');

      if (payment.amount != iamportPayment.amount) {
        return cancel(merchantUid);
      } else {
        payment = _.extend(payment, iamportPayment);
        return payment.save();
      }
    });
}


function cancel(paymentId) {

  let payment;

  return Payment.findOne({_id: paymentId})
    .then((foundPayment) => {
      if (!foundPayment) throw new Error('PaymentNotExistError');

      payment = foundPayment;

      return iamport.payment.cancel({merchant_uid: foundPayment.merchant_uid});
    })
    .then((iamportPayment) => {
      if (!iamportPayment) throw new Error('PGServiceDown');

      payment = _.extend(payment, iamportPayment);

      return payment.save();
    });
}


function getCreditCard(req) {
  return iamport.subscribe_customer.get({customer_uid: req.body.cardId});
}


function schedule(req) {

  return Promise.all([
    Payment.findOne({_id: req.body.paymentId}),
    CreditCard.findOne({_id: req.body.cardId})
  ])
    .spread((payment, card) => {

      if (!payment) throw new Error('PaymentNotExistError');
      if (!card) throw new Error('CardNotExistError');

      // let schedule_at = (new Date().getTime() + (24 * 60 * 60 * 1000)) / 1000;
      let schedule_at = Math.floor((new Date().getTime() + (5 * 60 * 1000)) / 1000);

      return iamport.subscribe.schedule({
        customer_uid: card.customer_uid,
        checking_amount: payment.amount,
        schedules: [
          {
            "merchant_uid": payment._id,
            "schedule_at": schedule_at,
            "amount": payment.amount,
            // "name": "주문명",
            // "buyer_name": "주문자명",
            // "buyer_email": "주문자 Email주소",
            // "buyer_tel": "주문자 전화번호",
            // "buyer_addr": "주문자 주소",
            // "buyer_postcode": "주문자 우편번호"
          }
        ]
      });
    });

}

function unschedule(req) {


  return Promise.all([
    Payment.findOne({_id: req.body.paymentId}),
    CreditCard.findOne({_id: req.body.cardId})
  ])
    .spread((payment, card) => {

      if (!payment) throw new Error('PaymentNotExistError');
      if (!card) throw new Error('CardNotExistError');

      return iamport.subscribe.unschedule({
        customer_uid: card.customer_uid,
        merchant_uid: payment._id
      })
    });

}
