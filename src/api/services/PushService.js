/**
 * Created by Andy on 7/6/2015
 * As part of ServerStarter
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 * Updater JongIn Koo 2017/01/20
 */

let FCM = require('fcm-push');

let logger = AppZet.logger('PushService');
let Promise = require('bluebird');
let async = require('async');


module.exports = {
  sendToDevices: sendToDevices,
  sendToTopic: sendToTopic,
};

function devicePushTask(sender, devices, message, callback) {
  message.registration_ids = _.compact(_.map(devices, data => {
    return data.pushId
  }));

  if(message.registration_ids.length <1)
    return callback(null, {});

  sender.send(message,
    (err, response) => {
      if (err)
        return callback(err);

      callback(null, response);
    }
  );
}

function sendToTopic(client, topic, title, message) {
  return new Promise(function (resolve, reject) {
    if (!client || !client.fcmKey)
      return resolve();

    let fcm = new FCM(client.fcmKey);

    var message = {
      condition: topic, // e.g) 'dogs' in topics || 'cats' in topics
      data: {
        message: title,
        title: title,
        body: message,
        sound: 'default',
        icon: "pushicon"
      },
      notification: {
        title: title,
        body: message,
        sound: 'default',
        icon: "pushicon"
      }
    };

    fcm.send(message, function (err, response) {
      if (err)
        reject(err);
      resolve(response);
    })
  });
}

function sendToDevices(client, devices, title, message, data, collapseKey) {
  return new Promise(function (resolve, reject) {
    if (!client || !client.fcmKey)
      return resolve();

    let fcm = new FCM(client.fcmKey);

    var collapseKey = collapseKey || 'NEWS_TO_USER';

    let pushMessage = {
      collapse_key: collapseKey,
      data: {
        title: title,
        body: message,
        sound: 'default',
        icon: "pushicon"
      },
      notification: {
        title: title,
        body: message,
        sound: 'default',
        icon: "pushicon"
      }
    };

    //TODO: 추후 UserAdmin에서 모든 사용자에게 푸시를 보낼 경우 ALL_NOTICE라는 타입을 사용
    pushMessage.data = _.assign(pushMessage.data, data);

    let deviceLists = _.groupBy(devices, function (element, index) {
      return Math.floor(index / 1000);
    });

    let pushTask = [];

    _.forEach(deviceLists, function (devices) {
      pushTask.push(devicePushTask.bind(null, fcm, devices, pushMessage));
    });

    async.parallel(pushTask,
      function (err, results) {
        logger.debug(err);
        logger.debug(results);
        if (err) {
          logger.log('error', err);
          return reject(err);
        }

        return resolve(results);
      });
  });
}
