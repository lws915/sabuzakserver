'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

  // Logger
let logger = AppZet.logger('ServerService');
let Promise = require('bluebird');
let REQUEST_MAX_COUNT = 5;
let REQUEST_LIMIT_TIME = 3 * 60;

const uuidV1 = require('uuid/v1');


const CONFIG_KEY = "Config";
const PRESET_MODEL_KEY = [
  "Intro",
  "StampCard",
  "Coupon",
  "Event",
  "Post",
  "Product",
  "Gallery",
  "Place",
  "File",
];

const PUBLISH_MODEL_KEY = [
  "Intro",
  "StampCard",
  "Coupon",
  "Event",
  "Post",
  "Product",
  "Gallery",
  "Place",
];

let BARCODE_TYPE = {
  'STAMP': 0,
  'STAMP_COMPLETE': 1,
  'COUPON': 3
}

module.exports = {

  publishSetting: publishSetting,

  // Release request managing services
  generateApplication: generateApplication,
  generatePosUser: generatePosUser,
  resetPosUserPassword: resetPosUserPassword,

  prepareApplication: prepareApplication,
  updateDomain: updateDomain,

  // Template Services
  addTemplate: addTemplate,
  changeTemplate: changeTemplate,

  clearTemplate: clearTemplate,
  clearClient: clearClient,

  createPreviewSpace: createPreviewSpace,
  createAppSpace: createAppSpace,

  // Setup preset data
  resetServerPresetData: resetServerPresetData,

  // Feature Services
  dropServerData: dropServerData,
  checkPasscode: checkPasscode,
  createBarcodeNumber: createBarcodeNumber,

  // Private Method
  presetDataCycle: presetDataCycle,
};

function publishSetting(client) {

  let serverId;

  if (typeof client.server === 'object')
    serverId = client.server._id;
  else
    serverId = client.server;

  return Server.findOne({_id: serverId})
    .then(server => {
      if (!server) throw new Error("ClientNotFound");

      let promieses = [];

      client.config = client.draftConfig;
      promieses.push(client.save());

      // _.forEach(PUBLISH_MODEL_KEY, (modelName) => {
      //   promieses.push(global[modelName].update({server: serverId}, {$set: {isPublished: true}}, {multi: true}));
      // });

      return promieses;
    });
}


// Change this to generating server clint and importing template files
function generateApplication(template, server, client, isPreview) {

  if (isPreview)
    server.isPreview = true;

  if (isPreview)
    client.isPreview = true;

  let updateTemplate, createdServer, createdClient;

  return Server.create(server)
    .then((server) => {
      createdServer = server;
      client.availableTemplates = [template._id];
      client.template = template._id;
      client.server = server._id;
      return Client.create(client);
    })
    .then((client) => {
      createdClient = client;
      if (!isPreview)
        return generatePosUser(createdServer._id);
      else
        return null;
    })
    .then(() => {
      if (isPreview)
        template.preview = createdClient._id;
      return [template.save(), createdServer, createdClient];
    });
}

function generatePosUser(serverId) {
  return new Promise((resolve, reject) => {

    let posId = LPAD(serverId + "", '0', 5) + "00";
    let randomPassword = LPAD(Math.ceil(Math.random() * 100000000) + "", '0', 8);

    Server.findOne({_id: serverId})
      .then((server) => {
        if (!server) return reject(new Error("ServerNotFound"));
        // Server ID + userId
        // 00002 + 00

        let posUser = {
          identifier: posId,
          password: randomPassword,
          server: server._id,
          isPosUser: true,
        };

        PassportService.protocols.pos.createUser(posUser)
          .then(user => resolve({
            identifier: posId,
            password: randomPassword
          }))
          .catch(err => reject(err));
      });
  });
}

function resetPosUserPassword(serverId, posUserId) {
  return new Promise((resolve, reject) => {
    let randomPassword = Math.ceil(Math.random() * 100000000) + "";
    User.findOne({
      identifier: posUserId,
      server: serverId
    })
      .then(user => {
        if (!user) return reject(new Error("UserNotFound"));
        return Passport.findOne({owner: user._id, protocol: 'pos'});
      })
      .then(passport => {
        if (!passport) return reject(new Error("PassportNotFound"));
        passport.password = randomPassword;
        return passport.save();
      })
      .then(() => {
        return resolve({
          identifier: posUserId,
          password: randomPassword
        })
      })
  });
}

function prepareApplication(client) {
  return ServerService.createPreviewSpace(client)
    .then(() => {
      let promises = [ServerService.resetServerPresetData(client)];

      if (client.domainRef)
        promises.push(updateDomain(client));

      return promises;
    })
    .spread((result) => {
      client.isPrepared = true;
      return client.save();
    });
}


function updateDomain(client) {
  return createAppSpace(client)
  // .then(() => {
  //   return tryCreateCDN(client);
  // })
    .then(result => {

      // if (result)
      return upsertBucketToDNS(client);
      // else
      // TODO need to create batch
      //   return upsertCDNToDNS(client.domainRef);
    });
}


// 첫날은 Bucket으로
function upsertBucketToDNS(client) {
  let params = {
    ChangeBatch: {
      Changes: [
        {
          Action: "UPSERT",
          ResourceRecordSet: {
            AliasTarget: {
              DNSName: AppZet.config.connections.s3.clients.s3WebDomainDNS,
              HostedZoneId: AppZet.config.connections.s3.clients.HostedZoneId,
              EvaluateTargetHealth: false,
            },
            Name: client.domainRef.name + AppZet.config.connections.s3.clients.domain,
            Type: "A"
          }
        }
      ],
      Comment: "Web server for example.com"
    },
    HostedZoneId: AppZet.config.connections.route53.HostedZoneId
  };

  return Route53Service.changeResourceRecordSets(params);
}

// 다음날은 CDN으로 < 성능 향상및 HTTPS
function upsertCDNToDNS(domain) {

  let params = {
    ChangeBatch: {
      Changes: [
        {
          Action: "UPSERT",
          ResourceRecordSet: {
            AliasTarget: {
              DNSName: domain.cdnDomainName,
              HostedZoneId: AppZet.config.connections.cloudFront.HostedZoneId,
              EvaluateTargetHealth: false,
            },
            Name: client.domainRef.name + AppZet.config.connections.s3.clients.domain,
            Type: "A"
          }
        }
      ],
      Comment: "Web server for example.com"
    },
    HostedZoneId: AppZet.config.connections.route53.HostedZoneId
  };

  return Route53Service.changeResourceRecordSets(params);
}

// TODO: enable later for HTTPS
function tryCreateCDN(client) {

  let params = _.clone(AppZet.config.cloudFrontConfig);
  params.DistributionConfig.CallerReference = uuidV1();
  params.DistributionConfig.Aliases.Items[0] = client.domainRef.name + AppZet.config.connections.s3.clients.domain;
  params.DistributionConfig.Origins.Items[0].Id = "S3-" + client.domainRef.name + AppZet.config.connections.s3.clients.domain;
  params.DistributionConfig.Origins.Items[0].DomainName = client.domainRef.name + AppZet.config.connections.s3.clients.s3WebDomain;
  params.DistributionConfig.Origins.Items[0].CustomOriginConfig = {
    OriginProtocolPolicy: "https-only",
    HTTPPort: 80,
    HTTPSPort: 443,
    OriginSslProtocols: {
      Quantity: 3,
      Items: ["TLSv1.2", "TLSv1.1", "TLSv1"]
    }
  };
  params.DistributionConfig.DefaultCacheBehavior.TargetOriginId = "S3-" + client.domainRef.name + AppZet.config.connections.s3.clients.domain;
  params.DistributionConfig.Logging.Prefix = client.domainRef.name;

  return new Promise((resolve, reject) => {
    CloudFrontService.createDistribution(params)
      .then((data) => {
        client.domainRef.cdnDomainName = data.DomainName;
        return client.domainRef.save();
      })
      .then((data) => {
        resolve(true);
      })
      .catch((err) => {
        if (err.name == "CNAMEAlreadyExists") {
          client.domainRef.isCdnApplied = true;
          client.domainRef.save();
          return resolve(false);
        }

        return reject(err);

      });
  });
}

function addTemplate(client, template, creditCard) {

  let invoice = {
    type: 'Normal',

    // 상품정보
    totalPrice: template.salePrice || template.price,
    originalPrice: template.price,
    discountPrice: template.salePrice || 0,
    templates: [template._id],

    itemList: {
      templates: [template.toObject()],
    },

    notes: "템플릿 추가",

    // 개인 정보
    email: client.owner.identifier,
    fullName: client.owner.fullName,
    phone: client.owner.phone,
    address: client.owner.address,
    addressDetail: client.owner.addressDetail,
    companyName: client.owner.companyName,

    client: client._id,

    owner: client.owner,
    server: client.server
  };

  let createdInvoice;

  return Invoice.create(invoice)
    .then((invoice) => {
      createdInvoice = invoice;

      if (!creditCard)
        return TransactionService.payWithExistingCard(invoice, client.subscriptions[0].creditCard);
      else
        return TransactionService.transact(invoice, creditCard, true);
    })
    .then((payment) => {
      client.availableTemplates.push(template._id);

      return client.save();
    });
}

function changeTemplate(client, template) {
  return new Promise((resolve, reject) => {
    if (client.availableTemplates.indexOf(template._id) == -1)
      throw new Error("TemplateNotBought");

    client.template = template._id;

    client.save()
      .then((data) => {
        return [
          createPreviewSpace(client),
          createAppSpace(client)
        ];
      })
      .spread(() => {
        resolve();
      });
  });
}

function clearTemplate(templateId) {
  return S3Service.deleteFolder({
    Bucket: AppZet.config.connections.s3.templates.bucket,
    Prefix: templateId.toString()
  });
}

function clearClient(clientId) {
  return S3Service.deleteFolder({
    Bucket: AppZet.config.connections.s3.previews.bucket,
    Prefix: clientId.toString()
  });
}


function createPreviewSpace(client) {
  return setupPreviewTemplate(client)
    .then(() => {
      return setupPreviewConnection(client);
    })
}

function setupPreviewTemplate(client) {

  let templateId;

  if (typeof client.template === 'object')
    templateId = client.template._id;
  else
    templateId = client.template;

  let params = {
    Bucket: AppZet.config.connections.s3.templates.bucket,
    Key: templateId + '/preview/index.html'
  };

  return S3Service.getObject(params)
    .then((data) => {
      return clearClient(client._id);
    })
    .then((data) => {
      return S3Service.copyFolder(
        {ACL: 'public-read'},
        AppZet.config.connections.s3.templates.bucket,
        templateId + "/preview",
        AppZet.config.connections.s3.previews.bucket,
        client._id);
    })
    .catch(err => {
      if (err.name == "NoSuchKey")
        throw new Error("TemplateNotFound");
    });
}

function setupPreviewConnection(client) {
  let serverId;

  if (typeof client.server === 'object')
    serverId = client.server._id;
  else
    serverId = client.server;

  let config = {locale: client.locale};
  config.serverUrl = AppZet.config.connections.childServerUrl + serverId;
  config.clientId = client._id;
  config = JSON.stringify(config) + '\n';

  return S3Service.upload({
    Bucket: AppZet.config.connections.s3.previews.bucket,
    Key: client._id + "/appzet-config.json",
    ACL: 'public-read',
    Body: config,
    ContentType: "application/json"
  });
}


/**
 *
 * 1. create bucket
 * 2. copy template
 * 3. setup connection
 *
 * @param client
 */
function createAppSpace(client) {
  return createDomainSpace(client.domainRef.name)
    .then(() => {
      return setupClientTemplate(client);
    })
    .then(() => {
      return setupClientConnection(client);
    });
}

function createDomainSpace(name) {
  let Bucket = name + AppZet.config.connections.s3.clients.domain;

  return S3Service.headBucket({Bucket: Bucket})
    .then((bucketInfo) => {
      let params = {
        ACL: 'public-read',
        Bucket: Bucket,
        CreateBucketConfiguration: {
          LocationConstraint: "ap-northeast-2"
        }
      };

      if (!bucketInfo)
      // TODO: need to find way to get an notification when bucket gets created
        return S3Service.createBucket(params);
      else
        return bucketInfo;
    })
    .then(() => {
      let params = {
        Bucket: Bucket,
        WebsiteConfiguration: {
          ErrorDocument: {Key: 'index.html'},
          IndexDocument: {Suffix: 'index.html'},
        }
      };
      return S3Service.putBucketWebsite(params)
    })
    .catch((err) => {
      console.log(err);
      throw err;
    });
}


function setupClientTemplate(client) {
  return S3Service.deleteFolder({
    Bucket: client.domainRef.name + AppZet.config.connections.s3.clients.domain,
    Prefix: ''
  })
    .then((data) => {
      return S3Service.copyFolder(
        {ACL: 'public-read'},
        AppZet.config.connections.s3.templates.bucket,
        client.template + "/preview/",
        client.domainRef.name + AppZet.config.connections.s3.clients.domain,
        '');
    });
}

function setupClientConnection(client) {
  let serverId;

  if (typeof client.server === 'object')
    serverId = client.server._id;
  else
    serverId = client.server;

  let config = {locale: client.locale};
  config.serverUrl = AppZet.config.connections.childServerUrl + serverId;
  config.clientId = client._id;
  config = JSON.stringify(config) + '\n';

  return S3Service.upload({
    Bucket: client.domainRef.name + AppZet.config.connections.s3.clients.domain,
    Key: "appzet-config.json",
    ACL: 'public-read',
    Body: config,
    ContentType: "application/json"
  });
}


function resetServerPresetData(client) {
  let serverId, templateId;

  if (typeof client.server === 'object')
    serverId = client.server._id;
  else
    serverId = client.server;

  if (typeof client.template === 'object')
    templateId = client.template._id;
  else
    templateId = client.template;

  return validatePresetData(templateId, serverId)
    .spread((presetData, files) => {
      return presetDataCycle(client, presetData, files);
    });
}

function validatePresetData(templateId, serverId) {
  let imagesToCreate = [];

  return Promise.all([
    Template.findOne({_id: templateId}),
    Server.findOne({_id: serverId})
  ])
    .spread((template, server) => {
      if (!template) throw new Error("TemplateNotFound");
      if (!server) throw new Error("ServerNotFound");


      let params = {
        Bucket: AppZet.config.connections.s3.templates.bucket,
        Prefix: templateId + '/preset-data/',
        Delimiter: '/'
      };

      return S3Service.listObjectsV2(params)
    })
    .then((data) => {
      // "appzet-preset.json"
      if (data.Contents.length == 0) throw new Error("PresetDataNotFound");

      let promises = [];

      // to file server
      data.Contents.forEach(function (content) {

        if (content.Key.indexOf("appzet-preset.json") > -1) return;

        let customKey = uuidV1() + content.Key.substring(content.Key.lastIndexOf("."));

        imagesToCreate.push({
          url: "http://" + AppZet.config.connections.s3.images.bucket + "/" + customKey,
          secure_url: "https://" + AppZet.config.connections.s3.images.bucket + "/" + customKey,
          server: serverId,
          fileName: content.Key.substring(content.Key.lastIndexOf("/") + 1)
        });

        let params = {
          CopySource: AppZet.config.connections.s3.templates.bucket + '/' + content.Key,
          Bucket: AppZet.config.connections.s3.images.bucket,
          ACL: 'public-read',
          Key: customKey
        };

        promises.push(S3Service.copyObject(params));
      });

      return promises;
    })
    .spread((data) => {
      let params = {
        Bucket: AppZet.config.connections.s3.templates.bucket,
        Key: templateId + '/preset-data/appzet-preset.json',
      };
      return S3Service.getObject(params);
    })
    .then((presetData) => {

      try {
        presetData = JSON.parse(presetData.Body.toString());
      } catch (e) {
        throw new Error("InvalidPresetData");
      }

      return Promise.all([presetData, File.create(imagesToCreate)]);
    });
}

function presetDataCycle(client, data, files) {

  let serverId;

  if (typeof client.server === 'object')
    serverId = client.server._id;
  else
    serverId = client.server;

  let presetData = data;
  return Promise.resolve()

  // Clear App Data
    .then(() => {
      let clearPromise = [];

      _.forEach(PRESET_MODEL_KEY, (modelName) => {
        if (modelName === 'File') return;
        clearPromise.push(global[modelName].remove({server: serverId}));
      });

      return clearPromise;
    })

    // Create Data
    .spread(() => {

      let presetPromise = Promise.resolve();

      _.forEach(PRESET_MODEL_KEY, (modelName) => {

        if (!presetData[modelName]) return;

        presetPromise = presetPromise
          .then(() => {
            return createData(serverId, modelName, presetData[modelName], files)
          })
      });

      return presetPromise;
    })
    .then(() => {
      return linkData(presetData);
    })
    .then(() => {
      client.config = presetData[CONFIG_KEY];
      client.draftConfig = presetData[CONFIG_KEY];

      let promises = [client.save()];

      if (client.productTags)
        promises.push(TagService.findOrCreate(client.productTags));

      return promises;
    })
    .spread((client) => {
      return client;
    });
}


function createData(serverId, modelName, data, files) {
  return new Promise((resolve, reject) => {

    let createPromise = [];

    _.forEach(data, (datum) => {
      if (modelName === 'File') {
        let photo = _.find(files, (file) => {
          let item = file.toObject();
          return item.fileName === datum.fileName && item.server === serverId;
        });

        datum.document = photo;

      } else if (modelName === 'Coupon') {
        let dataToCreate = _.assignIn({server: serverId}, datum.document);

        createPromise.push(global[modelName].create(dataToCreate)
          .then((createdDatum) => {
            createdDatum.barcode = createBarcodeNumber(serverId, BARCODE_TYPE.COUPON, null, createdDatum._id);
            createdDatum.save();
            datum.document = createdDatum;
          }));

      } else {
        let dataToCreate = _.assignIn({server: serverId}, datum.document);

        if (datum.document.tags && Array.isArray(datum.document.tags))
          createPromise.push(TagService.findOrCreate(datum.document.tags));

        createPromise.push(global[modelName].create(dataToCreate)
          .then((createdDatum) => {
            datum.document = createdDatum;
          }));
      }
    });

    Promise.all(createPromise)
      .spread(() => {
        resolve(data);
      })
      .catch(err => {
        reject(err);
      });
  });
}

function linkData(data) {
  return new Promise((resolve, reject) => {

    Promise.resolve()
      .then(() => {


        let updateLinkPromises = Promise.resolve();

        _.forEach(PRESET_MODEL_KEY, (modelName) => {
          if (!data[modelName]) return;

          _.forEach(data[modelName], (datum) => {

            _.forEach(datum.vRef, (vRefItem) => {
              if (!data[vRefItem.modelName])
                throw new Error("In Model: " + modelName + " , Item:" + datum.document + " of RefModel: " + vRefItem.modelName + " does not exist in Server: " + datum.document.server);

              let realId;

              if (Array.isArray(vRefItem.vRefValue)) {
                let foundItems = [];

                _.forEach(vRefItem.vRefValue, (vRefValue) => {
                  let item = _.find(data[vRefItem.modelName], {vId: vRefValue});
                  if (!item)
                    throw new Error("In Model: " + modelName + " , Item: " + datum.document + " of RefModel: " + vRefItem.modelName + " with vId: " + vRefValue + " does not exist in Server: " + datum.document.server);
                  foundItems.push(item);
                  realId = _.map(foundItems, (foundItem) => {
                    if (!foundItem.document)
                      throw new Error("In Model: " + modelName + " , Item: " + datum.document + " of RefModel: " + vRefItem.modelName + " with vId: " + vRefValue + " does not exist in Server: " + datum.document.server);
                    return foundItem.document._id;
                  });
                });

              } else {
                let foundItem = _.find(data[vRefItem.modelName], {vId: vRefItem.vRefValue});
                if (!foundItem)
                  throw new Error("In Model: " + modelName + " , Item: " + datum.document + " of RefModel: " + vRefItem.modelName + " with vId: " + vRefItem.vRefValue + " does not exist in Server: " + datum.document.server);
                realId = foundItem.document._id;
              }

              logger.log('silly', "Linking Model:", modelName, "of _id:", datum.document._id, "with RefModel:", vRefItem.modelName, "_id:", realId, "in Server:", datum.document.server);

              datum.document[vRefItem.propName] = realId;
              updateLinkPromises = updateLinkPromises.then(datum.document.save);
            });
          });
        });

        return updateLinkPromises;
      })
      .then(() => {
        resolve();
      })
      .catch(err => {
        reject(err);
      });
  });
}


/**
 *  drop db
 **/
function dropServerData() {
  // Drop Server
  let dropPromises = [];

  _.forEach(AppZet.models, (model, modelKey) => {
    if (['Server', 'Model'].indexOf(modelKey) > -1) return;
    dropPromises.push(model.remove({server: serverConfig._id}))
  });

  dropPromises.push(Server.remove({_id: serverConfig._id}));

  return Promise.all(dropPromises);
}

function checkPasscode(serverId, passcode, userId) {
  let error;

  function isLocked(user) {
    let requestedAt = new Date(user.passcodeTryRequestedAt);
    let now = new Date();
    let limit = user.passcodeTryLimit * 1000;
    if (requestedAt.getTime() + limit - now.getTime() >= 0 && user.passcodeTryCount >= REQUEST_MAX_COUNT) {
      return true;
    } else {
      return false;
    }
  }

  return new Promise((resolve, reject) => {

    //Error Message Case
    //1. RequestedLocked
    //2. NotFound
    //3. NotMatchPasscode
    User.findOne({_id: userId})
      .then(user => {
        if (isLocked(user)) {
          let err = new Error('RequestLocked');
          err.cData = {
            user: user
          }
          throw err;
        } else {
          return Promise.all([user, Server.findOne({_id: serverId})]);
        }
      })
      .spread((user, server) => {
        if (!server) throw new Error("ServerNotFound");

        if (server.passcode == passcode) {
          user.passcodeTryRequestedAt = new Date();
          user.passcodeTryCount = 0;
          user.passcodeTryLimit = REQUEST_LIMIT_TIME;
          return Promise.all([user.save(), server, true]);
        } else {

          user.passcodeTryRequestedAt = new Date();
          if (user.passcodeTryCount >= REQUEST_MAX_COUNT) {
            user.passcodeTryLimit = (user.passcodeTryLimit / 60 + user.passcodeTryLimit / 60) * 60;
            if (user.passcodeTryLimit >= 60 * 30) {
              user.passcodeTryLimit = 60 * 30;
            }
            user.passcodeTryCount = 1;
          } else {
            user.passcodeTryCount++;
          }

          user.passcodeTryRequestedAt = new Date();
          return Promise.all([user.save(), error]);
        }
      })
      .spread((user, data, isSuccess) => {
        if (isSuccess) {
          let server = data;
          resolve(server);
        }
        else {
          let err = new Error("NotMatchPasscode");

          if (data) {
            if (data.message) {
              err.message = data.message;
              delete data.message;
            }
            err.cData = data;
          } else {
            err.cData = {user: user};
          }

          throw err;
        }
      })
      .catch(err => {
        reject(err);
      })
  })
}

function createBarcodeNumber(serverIndex, type, deviceIndex, modelIndex) {
  let serverCode = LPAD(serverIndex + "", '0', 5);
  let modelCode = LPAD(modelIndex + "", '0', 8);
  let deviceCode;

  let barcode = serverCode + " " + type + " " + modelCode;

  if (!deviceIndex)
    return barcode;

  if (deviceIndex == -1) {
    //Preview일 경우
    deviceCode = '000000'
  } else {
    deviceCode = LPAD(deviceIndex + "", '0', 6);
  }

  barcode += " " + deviceCode;
  return barcode;
}

function LPAD(s, c, n) {
  if (!s || !c || s.length >= n) {
    return s;
  }
  let max = (n - s.length) / c.length;
  for (let i = 0; i < max; i++) {
    s = c + s;
  }
  return s;
}