'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

// Logger
let logger = AppZet.logger('NotificationService');

module.exports = {
  send: send,
};

function send(server, query, message) {
  query.server = server;

  return User.find(query)
    .then(users => {

      let promises = _.map(users, (user) => {
        user.notificationCounts++;
        user.newNotification = true;
        user.save();

        // let sequence = Math.floor(user.notificationCounts / 50);
        let sequence = Math.floor(user.notificationCounts / 5);

        return NotificationInbox
          .update(
            {owner: user._id, sequence: sequence},
            {
              $push: {
                "notifications": {
                  $each: [message],
                  $position: 0
                }
              },
              sequence: sequence,
              isDeleted: false,
              server: server
            },
            {new: true, upsert: true});
      });

      return promises;
    });
}
