'use strict';

/**
 * Created by wonseok Lee on 25/08/2017
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & wonseok Lee - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 25/08/2017
 *
 */

let logger = AppZet.logger('BallWalletController');

module.exports = {
  // public (서버)
  count: count,
  find: find,
  findOne: findOne,

  // 사용자 주인
  create: create,
  update: update,
  remove: remove,
};

function count(req, res) {
  req.buildQuery()
    .then((params) => {
      return BallWallet.count(params.query);
    })
    .then(function (count) {
      return res.ok({count: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function find(req, res) {
  let params, promise;

  req.buildQuery()
    .then(( _params) => {
      params = _params;

      let queryPromise = BallWallet.find(params.query);

      // Limit
      if (!params.limit || params.limit > 50)
        params.limit = 50;
      params.limit++;
      queryPromise = queryPromise.limit(params.limit);

      // Skip
      if (params.skip)
        queryPromise = queryPromise.skip(params.skip);

      // Sort
      if (params.sort)
        queryPromise = queryPromise.sort(params.sort);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      // Count
      let countPromise = BallWallet.count(params.query);

      return [queryPromise, countPromise];
    })
    .spread(function (ballWallets, count) {

      // See if there's more
      let more = (ballWallets[params.limit - 1]) ? true : false;
      // Remove item over 20 (only for check purpose)
      if (more) ballWallets.splice(params.limit - 1, 1);

      return res.ok({ballWallets: ballWallets, more: more, total: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function findOne(req, res) {
  req.buildQuery()
    .then((params) => {
      let queryPromise = BallWallet.findOne(params.query);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      return queryPromise;
    })
    .then(function (ballWallet) {
      if (!ballWallet) throw new Error("BallWalletNotFound");
      res.ok({ballWallet: ballWallet});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'BallWalletNotFound')
        return res.badRequest();

      if (err.message === 'NoPermission')
        return res.forbidden();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}


function create(req, res) {
  let ballWallet;
  req.getParams(["title", "contentHtml", "category", "campus"])
    .then((_ballWallet) => {
      ballWallet = _ballWallet;

      ballWallet.createdBy = req.user._id;
      ballWallet.updatedBy = req.user._id;
      ballWallet.owner = req.user._id;

      return BallWallet.create(ballWallet);
    })
    .then(function (ballWallet) {
      res.ok({ballWallet: ballWallet});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function update(req, res) {
  req.getParams("_id")
    .then((query) => {
      return BallWallet.findOne(query);
    })
    .then(ballWallet => {
      if (!ballWallet) throw new Error("BallWalletNotFound");

      ballWallet.lastUpdatedAt = new Date();
      ballWallet.updatedBy = req.user._id;

      ballWallet.balance = req.body.balance;

      ballWallet.files = req.body.files;

      return ballWallet.save();
    })
    .then((ballWallet) => {
      return res.ok({ballWallet: ballWallet});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'BallWalletNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}


function remove(req, res) {
  req.getParams("_id")
    .then((query) => {
      return BallWallet.findOne(query);
    })
    .then(ballWallet => {
      if (!ballWallet) throw new Error("BallWalletNotFound");
      ballWallet.isDeleted = true;
      return ballWallet.save();
    })
    .then((ballWallet) => {
      return res.ok({ballWallet: ballWallet});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'BallWalletNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}
