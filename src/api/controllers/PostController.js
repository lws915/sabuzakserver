'use strict';

/**
 * Created by Andy on 7/8/2015
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/8/2015
 *
 */

let logger = AppZet.logger('PostController');

module.exports = {
  // public (서버)
  count: count,
  find: find,
  findOne: findOne,

  // 사용자 주인
  create: create,
  update: update,
  remove: remove,
};

function count(req, res) {
  req.buildQuery()
    .then((params) => {
      return Post.count(params.query);
    })
    .then(function (count) {
      return res.ok({count: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function find(req, res) {
  let params, promise, owner;

  req.buildQuery()
    .then((_params) => {
      if(_params.query.nickName)
        return [User.findOne({nickName: _params.query.nickName}), _params];
      else
        return [undefined, _params];
    })
    .spread((owner, _params) => {
      params = _params;

      if (owner) {
        delete params.query.nickName;
        params.query.owner = {$in: owner._id};
      }

      let queryPromise = Post.find(params.query);

      // Limit
      if (!params.limit || params.limit > 50)
        params.limit = 50;
      params.limit++;
      queryPromise = queryPromise.limit(params.limit);

      // Skip
      if (params.skip)
        queryPromise = queryPromise.skip(params.skip);

      // Sort
      if (params.sort)
        queryPromise = queryPromise.sort(params.sort);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      // Count
      let countPromise = Post.count(params.query);

      return [queryPromise, countPromise];
    })
    .spread(function (posts, count) {

      // See if there's more
      let more = (posts[params.limit - 1]) ? true : false;
      // Remove item over 20 (only for check purpose)
      if (more) posts.splice(params.limit - 1, 1);

      posts = _.map(posts, (post) => {
        if (post.isSecret) {
          post.title = "비밀글로 작성 된 게시물입니다.";
          post.contentHtml = "비밀글로 작성 된 게시물입니다.";
        }

        post.replies = _.map(post.replies, (reply) => {
          if (reply.isSecret) {
            reply.title = "비밀글로 작성 된 답글입니다.";
            reply.contentHtml = "비밀글로 작성 된 답글입니다.";
          }
          return reply;
        })

        return post;
      });

      return res.ok({posts: posts, more: more, total: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function findOne(req, res) {
  req.buildQuery()
    .then((params) => {
      let queryPromise = Post.findOne(params.query);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      return queryPromise;
    })
    .then(function (post) {

      post.views++;
      let promises = [post.save()];

      if (post.isSecret) {
        if (!req.user) throw new Error("NoPermission");
        promises.push(SecretPostService.checkPostAvailability(post, req));
      }
      return promises;
    })
    .spread(function (post) {
      if (!post) throw new Error("PostNotFound");
      res.ok({post: post});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'PostNotFound')
        return res.badRequest();

      if (err.message === 'NoPermission')
        return res.forbidden();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}


function create(req, res) {
  let post;
  req.getParams(["title", "contentHtml", "category", "campus"])
    .then((_post) => {
      post = _post;

      post.createdBy = req.user._id;
      post.updatedBy = req.user._id;
      post.owner = req.user._id;
      post.isSecret = req.body.isSecret;
      post.files = req.body.files;

      return Post.create(post);
    })
    .then(function (post) {
      res.ok({post: post});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function update(req, res) {
  req.getParams("_id")
    .then((query) => {
      return Post.findOne(query);
    })
    .then(post => {
      if (!post) throw new Error("PostNotFound");

      post.lastUpdatedAt = new Date();
      post.updatedBy = req.user._id;

      post.title = req.body.title;
      post.contentHtml = req.body.contentHtml;
      post.category = req.body.category;
      post.campus = req.body.campus;
      post.isSecret = req.body.isSecret;

      post.files = req.body.files;

      return post.save();
    })
    .then((post) => {
      return res.ok({post: post});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'PostNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}


function remove(req, res) {
  req.getParams("_id")
    .then((query) => {
      return Post.findOne(query);
    })
    .then(post => {
      if (!post) throw new Error("PostNotFound");
      post.isDeleted = true;
      return post.save();
    })
    .then((post) => {
      return res.ok({post: post});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'PostNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}
