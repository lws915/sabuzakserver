'use strict';

/**
 * Created by Andy on 7/8/2015
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/8/2015
 *
 */

let logger = AppZet.logger('LikeController');

module.exports = {
  // TODO: need to apply proper security api structure
  // 앱젯 사용자 주인
  find: find,
  likeTemplate: likeTemplate,
  unlikeTemplate: unlikeTemplate
};


function find(req, res) {
  let params = QueryService.buildQuery(req);

  params.query.server = req.server;
  params.query.owner = req.user._id;

  // Find
  let queryPromise = Like.find(params.query);

  // Limit
  if (!params.limit || params.limit > 50)
    params.limit = 50;
  params.limit++;
  queryPromise = queryPromise.limit(params.limit);

  // Skip
  if (params.skip)
    queryPromise = queryPromise.skip(params.skip);

  // Sort
  if (params.sort)
    queryPromise = queryPromise.sort(params.sort);

  // Populate
  if (params.populate) {
    if (Array.isArray(params.populate))
      _.forEach(params.populate, function (populate) {
        queryPromise = queryPromise.populate(populate);
      });
    else {
      queryPromise = queryPromise.populate(params.populate);
    }
  }


  // Count
  let countPromise = Like.count(params.query);

  Promise.all([queryPromise, countPromise])
    .spread(function (likes, count) {

      // See if there's more
      let more = (likes[params.limit - 1]) ? true : false;
      // Remove item over 20 (only for check purpose)
      if (more) likes.splice(params.limit - 1, 1);

      return res.ok({likes: likes, more: more, total: count});
    })
    .catch(function (err) {
      logger.log('error', err);
      return res.internalServer();
    });
}

function likeTemplate(req, res) {
  let templateId = req.param("templateId");
  if (!templateId) res.badRequest();

  let foundTemplate, likeToCreate, previousLikeCount;

  Promise.all([
    Template.findOne({_id: templateId}),
    Like.findOne({owner: req.user._id, template: templateId})
  ])
    .spread(function (template, like) {
      if (!template) throw new Error("TemplateNotFound");

      foundTemplate = template;
      previousLikeCount = foundTemplate.likes;
      foundTemplate.likes++;

      if (like) throw new Error("AlreadyLiked");

      likeToCreate = {
        template: templateId,
        owner: req.user._id,
        server: req.server
      };

      return Promise.all([
        foundTemplate.save(),
        Like.create(likeToCreate)
      ]);
    })
    .spread(function (template) {
      res.ok({template: template});
    })
    .catch(function (err) {
      logger.log('debug', err);

      if (likeToCreate) Like.remove(likeToCreate).exec();
      if (previousLikeCount != foundTemplate.likes) {
        foundTemplate.likes--;
        foundTemplate.save();
      }

      if (err.message === 'TemplateNotFound'
        || err.message === 'AlreadyLiked') return res.unprocessableEntity();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function unlikeTemplate(req, res) {
  let templateId = req.param("templateId");
  if (!templateId) res.badRequest();

  let foundTemplate, likeToRemove, previousLikeCount;

  Promise.all([
    Template.findOne({_id: templateId}),
    Like.findOne({owner: req.user._id, template: templateId})
  ])
    .spread(function (template, like) {
      if (!template) throw new Error("TemplateNotFound");

      foundTemplate = template;
      previousLikeCount = foundTemplate.likes;
      foundTemplate.likes--;

      if (!like) throw new Error("LikeNotFound");

      likeToRemove = {
        template: templateId,
        owner: req.user._id,
        server: req.server
      };

      return Promise.all([
        foundTemplate.save(),
        Like.remove(likeToRemove)
      ]);
    })
    .spread(function (template) {
      res.ok({template: template});
    })
    .catch(function (err) {
      logger.log('debug', err);

      if (likeToRemove) Like.findOrCreate(likeToRemove, likeToRemove, () => {
      });
      if (previousLikeCount != foundTemplate.likes) {
        foundTemplate.likes++;
        foundTemplate.save();
      }

      if (err.message === 'TemplateNotFound'
        || err.message === 'LikeNotFound') return res.unprocessableEntity();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}