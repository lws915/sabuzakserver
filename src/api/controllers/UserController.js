'use strict';

/**
 * Created by Andy on 7/7/2015
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/7/2015
 *
 */

let logger = AppZet.logger('UserController');

module.exports = {
  // 앱젯 사용자 (서버)
  getCustomerStat: getCustomerStat,

  // 앱젯 사용자 (서버) or 앱젯 관리자
  count: count,
  find: find,
  findOne: findOne,
  findRandom: findRandom,
  profileOpen: profileOpen,
  getUsersByPop: getUsersByPop,

  // 앱젯 관리자
  resetLoginLimit: resetLoginLimit,
  resetEmailConfirmLimit: resetEmailConfirmLimit,
  resetPasswordResetLimit: resetPasswordResetLimit,
  update: update,
	remove: remove
	
};

function getCustomerStat(req, res) {
  req.getParams("_id")
    .then((query) => {
      return User.findOne(query)
        .populate('devices');
    })
    .then((user) => {
      if (!user) throw new Error("UserNotFound");

      let lastMonth = Moment().subtract(1, 'months');
      let lastMonthQuery = {
        createdAt: {
          "$gte": lastMonth.startOf('month').toDate(),
          "$lte": lastMonth.endOf('month').toDate()
        }
      };

      let thisMonthQuery = {
        createdAt: {
          "$gte": Moment().startOf('month').toDate(),
          "$lte": Moment().endOf('month').toDate()
        }
      };

      let totalQuery = {};

      let deviceId = '';

      if (user.devices && user.devices[0])
        deviceId = user.devices[0]._id;

      return [
        AnalyticService.firstVisit(req.server, user._id),
        AnalyticService.visit(req.server, user._id, deviceId, lastMonthQuery),
        AnalyticService.visit(req.server, user._id, deviceId, thisMonthQuery),
        AnalyticService.visit(req.server, user._id, deviceId, totalQuery),
        Stamp.count({
          server: req.server,
          owner: user._id,
        }),
        CouponIssued.count({
          server: req.server,
          owner: user._id,
        })
      ]
    })
    .spread((firstVisit, lastMonthVisits, thisMonthVisits, totalVisits, totalStamps, couponUsed) => {

      let resData = {};

      resData.firstVisit = firstVisit;
      resData.lastMonthVisits = lastMonthVisits;
      resData.thisMonthVisits = thisMonthVisits;
      resData.totalVisits = totalVisits;
      resData.totalStamps = totalStamps;
      resData.couponUsed = couponUsed;

      return res.ok(resData);
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'UserNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });

}

function count(req, res) {
  req.buildQuery()
    .then((params) => {
      return User.count(params.query);
    })
    .then(function (count) {
      return res.ok({count: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function find(req, res) {
  let params;

  req.buildQuery()
    .then((_params) => {
      params = _params;

      // Find
      let queryPromise = User.find(params.query);

      // Limit
      if (!params.limit || params.limit > 50)
        params.limit = 50;
      params.limit++;
      queryPromise = queryPromise.limit(params.limit);

      // Skip
      if (params.skip)
        queryPromise = queryPromise.skip(params.skip);

      // Sort
      if (params.sort)
        queryPromise = queryPromise.sort(params.sort);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, (populate) => {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      // Count
      let countPromise = User.count(params.query);

      return Promise.all([queryPromise, countPromise]);
    })
    .spread(function (users, count) {

      // See if there's more
      let more = (users[params.limit - 1]) ? true : false;
      // Remove item over 20 (only for check purpose)
      if (more) users.splice(params.limit - 1, 1);

      return res.ok({users: users, more: more, total: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}


function findOne(req, res) {
  req.buildQuery()
    .then((params) => {
      let queryPromise = User.findOne(params.query);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      return queryPromise;
    })
    .then(function (user) {
      if (!user) throw new Error("UserNotFound");
      return res.ok({user: user});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
      ||err.message === 'UserNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function findRandom(req, res){
  let params;
  let user;
  let userList;

  req.buildQuery()
    .then( _params => {
      params = _params.query;

      return User.findOne({_id : req.user._id});
    }) 
    .then((_user) => {
      user = _user;
      let randomPromise;
      let otherGender = user.gender == "male" ? "female" : "male";
      if(user.todayRecommend.length == 0){
        randomPromise = User.aggregate(
          [
            {
              $match: {
                gender:otherGender, 
                isDeleted: false,
                _id: {$nin:user.opened}
              }
            },
            {
              $sample: {size:2}
            }
          ]
        );
      }else {
        randomPromise = User.aggregate(
          [
            {
              $match: {
                gender:otherGender, 
                isDeleted: false,
                _id: {$in:user.todayRecommend}
              }
            }
          ]
        );
      }
      return randomPromise;
    })
    .then((users) => {
      if(user.todayRecommend.length == 0){
        users.map(data => {
          user.todayRecommend.push(data._id);
        })
      }
      userList = users;
      return user.save();
    })
    .then((user) => {
      let idx = user.opened.length;
      let openIdx;
      if(user.opened.length == 0){
        openIdx = -1;
      }else{
        openIdx = user.opened[idx-1];
      }
      return res.ok({users: userList, lastOpened: openIdx});
    }) 
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });

}

function getUsersByPop(req, res){
  let typeArr, curNum, more;
  req.buildQuery()
    .then(params => {
      typeArr = params.query.typeArr;

      let getUser = User.findOne({_id: req.user._id})
                        .populate(['likeMe','upMe','likeU','upU','eachU']);
      return getUser;
    })
    .then(users => {
      let outObj = {};
      typeArr.map(data => {
        let temp = users[data.type];
        
        let tempOut = temp.slice(data.curNum*6, (data.curNum+1)*6);
        let tempNext = temp.slice((data.curNum+1)*6, (data.curNum+2)*6);
        outObj[data.type] = {list : tempOut, curNum : data.curNum + 1};
        
        if(tempNext.length > 0){
          outObj[data.type]['more'] = true;
        }else{
          outObj[data.type]['more'] = false;
        }
      })
      return res.ok({users: outObj});
    })
}

function profileOpen(req, res){

  let otherID;

  req.getParams(["otherID"])
    .then(_otherID => {
      otherID = _otherID.otherID;

      let getPromise = User.findOne({_id : req.user._id});

      return getPromise;
    })
    .then((me) => {
      let isPush = true;
      me._id = req.user._id;
      me.email = req.user.email;
      me.nickName = req.user.nickName;
      me.age = req.user.age;
      me.location = req.user.location;
      me.bodyType = req.user.bodyType;
      me.tall = req.user.tall;
      me.likeMe = req.user.likeMe;

      me.opened.map(data => {
        if(data == otherID){
          isPush = false;
        }
      });

      if(isPush){
        me.opened.push(otherID);
      }
      
      return me.save();
    })
    .then(updatedMe => {
      return res.ok(updatedMe);
    })
    .catch(err => {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    })
}

function resetLoginLimit(req, res) {
  req.getParams("_id")
    .then((query) => {
      return User.findOne(query);
    })
    .then((user) => {
      if (!user) throw new Error("UserNotFound");

      user.lastUpdatedAt = new Date();
      user.accesscount = 0;

      return user.save();
    })
    .then(function (updatedUser) {
      return res.ok({user: updatedUser});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'UserNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function resetEmailConfirmLimit(req, res) {
  req.getParams("_id")
    .then((query) => {
      return User.findOne(query);
    })
    .then((user) => {
      if (!user) throw new Error("UserNotFound");

      user.lastUpdatedAt = new Date();
      user.activationCounter = 0;

      return user.save();
    })
    .then(function (updatedUser) {
      return res.ok({user: updatedUser});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'UserNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function resetPasswordResetLimit(req, res) {
  req.getParams("_id")
    .then((query) => {
      return User.findOne(query);
    })
    .then((user) => {
      if (!user) throw new Error("UserNotFound");

      user.lastUpdatedAt = new Date();
      user.passwordResetCounter = 0;

      return user.save();
    })
    .then(function (updatedUser) {
      return res.ok({user: updatedUser});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'UserNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function update(req, res) {
  req.getParams("_id")
    .then((query) => {
      return User.findOne(query);
    })
    .then((user) => {
      if (!user) throw new Error("UserNotFound");

      user.lastUpdatedAt = new Date();
      user.updatedBy = req.user._id;

      user.introduce = req.body.introduce;      
      user.location = req.body.location;
      user.job = req.body.job;
      user.bodyType = req.body.bodyType;
      user.tall = req.body.tall;
      user.faith = req.body.faith;
      user.school = req.body.school;
      user.drinking = req.body.drinking;
      user.smoking = req.body.smoking;
      user.interest = req.body.interest;

      user.isAlert = req.body.isAlert;
      user.isPush = req.body.isPush;
      user.isTodayMatch = req.body.isTodayMatch;

      // user.receiveEmail = req.body.receiveEmail;
      // user.receiveSms = req.body.receiveSms;
      // user.receivePush = req.body.receivePush;

      return user.save();
    })
    .then(function (updatedUser) {
      return res.ok({user: updatedUser});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'UserNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function remove(req, res) {
  req.getParams("_id")
    .then((query) => {
      return User.findOne(query);
    })
    .then((user) => {
      if (!user) throw new Error("UserNotFound");
      user.isDeleted = true;
      return user.save();
    })
    .then(function (removedUser) {
      return res.ok(removedUser);
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'UserNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}