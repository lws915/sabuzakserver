'use strict';

/**
 * Created by Andy on 7/13/2015
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/13/2015
 *
 */

let dot = require('dot-object');
let logger = AppZet.logger('FileController');

module.exports = {
  // Public
  create: create,
  createOnlyUrl: createOnlyUrl,
  update: update,
  remove: remove
};

function create(req, res) {
  let data = {
    server: req.server
  }, files = [];

  new Promise((resolve, reject) => {
    let form = new Multiparty.Form({uploadDir: 'uploads'});

    form.on('field', function (name, value) {
      data[name] = value;
    });

    // all uploads are completed
    form.on('file', function (name, file) {
      files.push(file);
    });

    // all uploads are completed
    form.on('close', function () {
      resolve(files);
    });

    form.on('error', function (err) {
      reject(err);
    });
    form.parse(req);
  })
    .then(() => {
      let filePromises = [];
      _.forEach(files, (file) => {
        let promise = FileService.upload(file, data);
        filePromises.push(promise);
      });

      return filePromises;
    })
    .spread((file) => {
      return res.ok({file: file});
    })
    .catch((err) => {
      logger.error(err);
      return res.internalServer();
    })
    .finally(() => _.forEach(files, (file) => {
      if (FileSystem.existsSync(file.path))
        FileSystem.unlink(file.path)
    }));
}

function createOnlyUrl(req, res) {
  if (!req.body.secure_url) return res.badRequest();

  let file = req.body;
  file.server = req.server;

  file.createdBy = req.user._id;
  file.updatedBy = req.user._id;

  if (!file.owner)
    file.owner = req.user._id;

  // cannot update scores and soft delete
  delete file.views;
  delete file.isDeleted;
  delete file.lastUpdatedAt;

  File.create(file)
    .then(function (file) {
      return res.ok({file: file});
    })
    .catch(function (err) {
      logger.error(err);
      return res.internalServer();
    });
}

function update(req, res) {
  if (!req.body._id)
    return res.badRequest();

  req.body.lastUpdatedAt = new Date();

  req.body.updatedBy = req.user._id;

  delete req.body.owner;
  delete req.body.createdBy;
  delete req.body.server;

  File.update({_id: req.body._id}, req.body, {multi: false})
    .then(function (result) {
      return res.ok(result);
    })
    .catch(function (err) {
      logger.log('error', err);
      return res.internalServer();
    });

}

function remove(req, res) {
  if (!req.query._id) return res.badRequest();

  File.update({_id: req.query._id}, {isDeleted: true})
    .then(function (removedItem) {
      return res.ok(removedItem);
    })
    .catch(function (err) {
      logger.log('error', err);
      return res.internalServer();
    });
}