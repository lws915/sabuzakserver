'use strict';

/**
 * Created by wonseok Lee on 30/08/2017
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 30/08/2017
 *
 */

let logger = AppZet.logger('LikeController');

module.exports = {
  // public (서버)
  count: count,
  find: find,
  findOne: findOne,

  // 사용자 주인
  create: create,
  remove: remove,
};

function count(req, res) {
  req.buildQuery()
    .then((params) => {
      return Like.count(params.query);
    })
    .then(function (count) {
      return res.ok({count: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function find(req, res) {
  let params, promise, owner;

  req.buildQuery()
    .then((_params) => {
      if(_params.query.nickName)
        return [User.findOne({nickName: _params.query.nickName}), _params];
      else
        return [undefined, _params];
    })
    .spread((owner, _params) => {
      params = _params;

      if (owner) {
        delete params.query.nickName;
        params.query.owner = {$in: owner._id};
      }

      let queryPromise = Like.find(params.query);

      // Limit
      if (!params.limit || params.limit > 50)
        params.limit = 50;
      params.limit++;
      queryPromise = queryPromise.limit(params.limit);

      // Skip
      if (params.skip)
        queryPromise = queryPromise.skip(params.skip);

      // Sort
      if (params.sort)
        queryPromise = queryPromise.sort(params.sort);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      // Count
      let countPromise = Like.count(params.query);

      return [queryPromise, countPromise];
    })
    .spread(function (likes, count) {

      // See if there's more
      let more = (likes[params.limit - 1]) ? true : false;
      // Remove item over 20 (only for check purpose)
      if (more) likes.splice(params.limit - 1, 1);

      return res.ok({likes: likes, more: more, total: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function findOne(req, res) {
  req.buildQuery()
    .then((params) => {
      let queryPromise = Like.findOne(params.query);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      return queryPromise;
    })
    .then(function (like) {

      like.views++;
      let promises = [like.save()];

      return promises;
    })
    .spread(function (like) {
      if (!like) throw new Error("LikeNotFound");
      res.ok({like: like});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'LikeNotFound')
        return res.badRequest();

      if (err.message === 'NoPermission')
        return res.forbidden();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}


function create(req, res) {
  let like;
  req.getParams(["user"])
    .then((_like) => {
      like = _like;

      like.createdBy = req.user._id;
      like.updatedBy = req.user._id;
      like.owner = req.user._id;

      return Like.find({owner: req.user._id, user:like.user})
    })
    .then(isLike => {
      if(isLike.length){
        return isLike;
      }else{
        return Like.create(like);
      }
    })
    .then(function (resultLike) {
      res.ok({like: resultLike});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function remove(req, res) {
  req.getParams("_id")
    .then((query) => {
      return Like.findOne(query);
    })
    .then(like => {
      if (!like) throw new Error("LikeNotFound");
      like.isDeleted = true;
      return like.save();
    })
    .then((like) => {
      return res.ok({like: like});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'LikeNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}
