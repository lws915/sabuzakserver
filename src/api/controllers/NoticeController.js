'use strict';

/**
 * Created by wonseok Lee on 24/08/2017
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & wonseok Lee - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 24/08/2017
 *
 */

let logger = AppZet.logger('NoticeController');

module.exports = {
  // public (서버)
  count: count,
  find: find,
  findOne: findOne,

  // 사용자 주인
  create: create,
  update: update,
  remove: remove,
};

function count(req, res) {
  req.buildQuery()
    .then((params) => {
      return Notice.count(params.query);
    })
    .then(function (count) {
      return res.ok({count: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function find(req, res) {
  let params, promise ;

  req.buildQuery()
    .then((_params) => {
      params = _params;

      let queryPromise = Notice.find(params.query);

      // Limit
      if (!params.limit || params.limit > 50)
        params.limit = 50;
      params.limit++;
      queryPromise = queryPromise.limit(params.limit);

      // Skip
      if (params.skip)
        queryPromise = queryPromise.skip(params.skip);

      // Sort
      if (params.sort)
        queryPromise = queryPromise.sort(params.sort);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      // Count
      let countPromise = Notice.count(params.query);

      return [queryPromise, countPromise];
    })
    .spread(function (notices, count) {

      // See if there's more
      let more = (notices[params.limit - 1]) ? true : false;
      // Remove item over 20 (only for check purpose)
      if (more) notices.splice(params.limit - 1, 1);

      return res.ok({notices: notices, more: more, total: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function findOne(req, res) {
  req.buildQuery()
    .then((params) => {
      let queryPromise = Notice.findOne(params.query);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      return queryPromise;
    })
    .then(function (notice) {

      notice.views++;
      let promises = [notice.save()];

      if (notice.isSecret) {
        if (!req.user) throw new Error("NoPermission");
        promises.push(SecretNoticeService.checkNoticeAvailability(notice, req));
      }
      return promises;
    })
    .spread(function (notice) {
      if (!notice) throw new Error("NoticeNotFound");
      res.ok({notice: notice});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'NoticeNotFound')
        return res.badRequest();

      if (err.message === 'NoPermission')
        return res.forbidden();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}


function create(req, res) {
  let notice;
  req.getParams(["title", "contentHtml", "category", "campus"])
    .then((_notice) => {
      notice = _notice;

      notice.createdBy = req.user._id;
      notice.updatedBy = req.user._id;
      notice.owner = req.user._id;
      notice.isSecret = req.body.isSecret;
      notice.files = req.body.files;

      return Notice.create(notice);
    })
    .then(function (notice) {
      res.ok({notice: notice});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function update(req, res) {
  req.getParams("_id")
    .then((query) => {
      return Notice.findOne(query);
    })
    .then(notice => {
      if (!notice) throw new Error("NoticeNotFound");

      notice.lastUpdatedAt = new Date();
      notice.updatedBy = req.user._id;

      notice.title = req.body.title;
      notice.contentHtml = req.body.contentHtml;
      notice.category = req.body.category;
      notice.campus = req.body.campus;
      notice.isSecret = req.body.isSecret;

      notice.files = req.body.files;

      return notice.save();
    })
    .then((notice) => {
      return res.ok({notice: notice});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'NoticeNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}


function remove(req, res) {
  req.getParams("_id")
    .then((query) => {
      return Notice.findOne(query);
    })
    .then(notice => {
      if (!notice) throw new Error("NoticeNotFound");
      notice.isDeleted = true;
      return notice.save();
    })
    .then((notice) => {
      return res.ok({notice: notice});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'NoticeNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}
