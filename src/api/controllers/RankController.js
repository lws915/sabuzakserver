'use strict';

/**
 * Created by wonseok Lee on 30/08/2017
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 30/08/2017
 *
 */

let logger = AppZet.logger('RankController');

module.exports = {
  // public (서버)
  count: count,
  find: find,
  findOne: findOne,
  recommend: recommend,

  // 사용자 주인
  create: create,
  remove: remove,
};

function count(req, res) {
  req.buildQuery()
    .then((params) => {
      return Rank.count(params.query);
    })
    .then(function (count) {
      return res.ok({count: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function find(req, res) {
  let params;

  req.buildQuery()
    .then((_params) => {
      params = _params;
      
      if(params.query.owner){
        return Like.find({owner:params.query.owner})
      }else{
        return Like.find({user:params.query.userID})
      }
    })
    .then((likeList) => {
      let exceptionList = [];
      if(params.query.owner){
        likeList.map(data => {
          exceptionList.push(data.user);
        })
        params.query.user={
          $nin: exceptionList
        }
      }else{
        likeList.map(data => {
          exceptionList.push(data.owner);
        })
        params.query.owner={
          $nin: exceptionList
        }
        params.query.user = params.query.userID;
        delete params.query.userID;
      }
      
      let queryPromise = Rank.find(params.query);

      // Limit
      if (!params.limit || params.limit > 50)
        params.limit = 50;
      params.limit++;
      queryPromise = queryPromise.limit(params.limit);

      // Skip
      if (params.skip)
        queryPromise = queryPromise.skip(params.skip);

      // Sort
      if (params.sort)
        queryPromise = queryPromise.sort(params.sort);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      // Count
      let countPromise = Rank.count(params.query);

      return [queryPromise, countPromise];
    })
    .spread(function (ranks, count) {

      // See if there's more
      let more = (ranks[params.limit - 1]) ? true : false;
      // Remove item over 20 (only for check purpose)
      if (more) ranks.splice(params.limit - 1, 1);

      return res.ok({ranks: ranks, more: more, total: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function findOne(req, res) {
  req.buildQuery()
    .then((params) => {
      let queryPromise = Rank.findOne(params.query);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      return queryPromise;
    })
    .then(function (rank) {

      rank.views++;
      let promises = [rank.save()];

      return promises;
    })
    .spread(function (rank) {
      if (!rank) throw new Error("RankNotFound");
      res.ok({rank: rank});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'RankNotFound')
        return res.badRequest();

      if (err.message === 'NoPermission')
        return res.forbidden();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}


function create(req, res) {
  let rank;
  req.getParams(["user", "type"])
    .then((_rank) => {
      rank = _rank;
      
      rank.createdBy = req.user._id;
      rank.updatedBy = req.user._id;
      rank.owner = req.user._id;

      return Rank.findOne({
        owner:rank.user, 
        user:req.user._id,
        type: 'up'
      });

    })
    .then(resultRank => {
      if(resultRank){
        resultRank.eachUp = true;
        rank.eachUp = true;
        return resultRank.save();
      }else{
        return true;
      }
    })
    .then(tempRank => {
      return Rank.create(rank);
    })
    .then(function (rank) {
      res.ok({rank: rank, upCount: req.user.upCount });
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function recommend(req, res){
  let params;
  let user;

  req.buildQuery()
    .then(_params => {
      params = _params.query;

      return Rank.find({owner: req.user._id})
    })
    .then(ranks => {

      let otherGender = req.user.gender == "male" ? "female" : "male";
      let exceptionList = [];

      ranks.map(data => {
        exceptionList.push(data.user);
      })

      return User.aggregate(
        [
          {
            $match: {
              gender:otherGender, 
              isDeleted: false,
              _id: {$nin:exceptionList}
            }
          },
          {
            $sample: {size:1}
          }
        ]
      );
    })
    .then(function(users) {
      return res.ok({users: users});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });

}

function remove(req, res) {
  req.getParams("_id")
    .then((query) => {
      return Rank.findOne(query);
    })
    .then(rank => {
      if (!rank) throw new Error("RankNotFound");
      rank.isDeleted = true;
      return rank.save();
    })
    .then((rank) => {
      return res.ok({rank: rank});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'RankNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}
