'use strict';

/**
 * Created by Andy on 7/8/2015
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/8/2015
 *
 */

let logger = AppZet.logger('VersionController');

module.exports = {
  // 앱젯 관리자
  count: count,
  find: find,
  findOne: findOne,

  create: create,
  update: update,
  remove: remove,
};

function count(req, res) {
  req.buildQuery()
    .then((params) => {
      return Version.count(params.query);
    })
    .then(function (count) {
      return res.ok({count: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function find(req, res) {
  let params;

  req.buildQuery()
    .then((_params) => {
      params = _params;

      let queryPromise = Version.find(params.query);

      // Limit
      if (!params.limit || params.limit > 50)
        params.limit = 50;
      params.limit++;
      queryPromise = queryPromise.limit(params.limit);

      // Skip
      if (params.skip)
        queryPromise = queryPromise.skip(params.skip);

      // Sort
      if (params.sort)
        queryPromise = queryPromise.sort(params.sort);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      // Count
      let countPromise = Version.count(params.query);

      return Promise.all([queryPromise, countPromise]);
    })
    .spread(function (versions, count) {

      // See if there's more
      let more = (versions[params.limit - 1]) ? true : false;
      // Remove item over 20 (only for check purpose)
      if (more) versions.splice(params.limit - 1, 1);

      return res.ok({versions: versions, more: more, total: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function findOne(req, res) {
  req.buildQuery()
    .then((params) => {
      let queryPromise = Version.findOne(params.query);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      return queryPromise;
    })
    .then(function (version) {
      if (!version) throw new Error("VersionNotFound");
      res.ok({version: version});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'VersionNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}


function create(req, res) {
  let version;
  req.getParams(["requiredVersion", "optionalVersion",
    "buildNumber", "platform", "client"])
    .then((query) => {

      version = {};
      version.createdBy = req.user._id;
      version.updatedBy = req.user._id;
      version.owner = req.user._id;

      version.requiredVersion = req.body.requiredVersion;
      version.optionalVersion = req.body.optionalVersion;
      version.buildNumber = req.body.buildNumber;
      version.platform = req.body.platform;
      if (req.body.active)
        version.active = req.body.active;

      version.client = req.body.client;

      return Version.create(version);
    })
    .then((version) => {
      res.ok({version: version});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}


function update(req, res) {
  req.getParams("_id")
    .then((query) => {
      return Version.findOne(query);
    })
    .then(version => {
      if (!version) throw new Error("VersionNotFound");

      version.lastUpdatedAt = new Date();
      version.updatedBy = req.user._id;

      version.requiredVersion = req.body.requiredVersion;
      version.optionalVersion = req.body.optionalVersion;
      version.buildNumber = req.body.buildNumber;

      let promises = [version.save()];

      if (req.body.active) {
        version.active = req.body.active;
        promises.push(Version.update({_id: {$ne: version._id}, client: version.client, platform: version.platform},
          {active: false}, {multi: true}));
      }

      return promises;
    })
    .spread((version) => {
      return res.ok({version: version});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'VersionNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}


function remove(req, res) {
  req.getParams("_id")
    .then((query) => {
      return Version.findOne(query);
    })
    .then(version => {
      if (!version) throw new Error("VersionNotFound");
      version.isDeleted = true;
      return version.save();
    })
    .then((version) => {
      return res.ok({version: version});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'VersionNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}
