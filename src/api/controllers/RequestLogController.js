'use strict';

/**
 * Created by MaengKwan Seo on 16/03/03
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by MaengKwan Seo <hbnb7894@gmail.com>, 16/03/03
 *
 */

let moment = require('moment');
let async = require('async');
let logger = AppZet.logger('RequestLogController');

module.exports = {
  // 앱젯 관리자
  count: count,
  find: find,
  findOne: findOne,
  countIp: countIp,
  countMonth: countMonth
};

function count(req, res) {
  let params = QueryService.buildQuery(req);
  params.query.server = req.server;

  logger.debug('params: ', params);

  RequestLog.count(params.query)
    .then(function (count) {
      return res.ok({count: count});
    })
    .catch(function (err) {
      logger.log('error', err);
      return res.internalServer();
    });
}

function find(req, res) {
  let params = QueryService.buildQuery(req);
  params.query.server = req.server;

  logger.debug('params: ', params);

  // Find
  let queryPromise = RequestLog.find(params.query);

  // Limit
  if (!params.limit || params.limit > 50)
    params.limit = 50;
  params.limit++;
  queryPromise = queryPromise.limit(params.limit);

  // Skip
  if (params.skip)
    queryPromise = queryPromise.skip(params.skip);

  // Sort
  if (params.sort)
    queryPromise = queryPromise.sort(params.sort);

  // Populate
  if (params.populate) {
    if (Array.isArray(params.populate))
      _.forEach(params.populate, function (populate) {
        queryPromise = queryPromise.populate(populate);
      });
    else
      queryPromise = queryPromise.populate(params.populate);
  }

  // Count
  let countPromise = RequestLog.count(params.query);

  Promise.all([queryPromise, countPromise])
    .spread(function (RequestLogs, count) {

      // See if there's more
      let more = (RequestLogs[params.limit - 1]) ? true : false;
      // Remove item over 20 (only for check purpose)
      if (more) RequestLogs.splice(params.limit - 1, 1);

      return res.ok({RequestLogs: RequestLogs, more: more, total: count});
    })
    .catch(function (err) {
      logger.log('error', err);
      return res.internalServer();
    });
}

function findOne(req, res) {
  let params = QueryService.buildQuery(req);
  params.query.server = req.server;

  let queryPromise = RequestLog.findOne(params.query);

  // Populate
  if (params.populate) {
    if (Array.isArray(params.populate))
      _.forEach(params.populate, function (populate) {
        queryPromise = queryPromise.populate(populate);
      });
    else
      queryPromise = queryPromise.populate(params.populate);
  }

  queryPromise
    .then(function (requestLog) {
      return res.ok({requestLog: requestLog});
    })
    .catch(function (err) {
      logger.log('error', err);
      return res.internalServer();
    });
}

function countIp(req, res) {
  let params = QueryService.buildQuery(req);
  params.query.server = req.server;

  let query = params.query;

  let ModelNative = Promise.promisify(RequestLog.native);

  ModelNative()
    .then(function (collection) {
      let todayStart = moment().startOf('day').toDate();
      let count = {};
      query = {
        createdAt: {
          '$gt': todayStart
        }
      };
      collection.distinct('ipAddress', query, null, function (err, docs) {
        if (err) {
          logger.log('error', err);
          return res.internalServer();
        }
        else return res.ok({count: docs.length});
      });
    })
    .catch(function (err) {
      logger.log('error', err);
      return res.internalServer();
    });
}

function countMonth(req, res) {
  let params = QueryService.buildQuery(req);
  params.query.server = req.server;

  let query = params.query;

  RequestLog.native(function (err, collection) {
    if (err) {
      logger.log('error', err);
      return res.internalServer;
    }

    let endDay = moment().endOf('month');
    let endDate = endDay.date();
    let promises = [];
    let countQuery = [];
    let amount = new Array(endDate);

    _.each(amount, function (item, i) {
      let startOfDay = moment().startOf('month').date(i + 1).toDate();
      let endOfDay = moment().startOf('month').date(i + 2).toDate();
      let query = {
        createdAt: {
          '$gt': startOfDay,
          '$lt': endOfDay
        }
      };
      promises.push(function (callback) {
        collection.distinct('ipAddress', query, null, function (err, docs) {
          if (err)
            return callback(400);
          return callback(null, docs.length);
        });
      });
    });

    async.parallel(promises,
      function (error, results) {
        if (error) {
          logger.log('error', error);
          return res.internalServer();
        }

        let sum = 0;
        _.each(results, function (length) {
          sum += length;
        });

        return res.ok({count: sum});
      });
  });
}