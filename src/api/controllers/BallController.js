'use strict';

/**
 * Created by wonseok Lee on 25/08/2017
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & wonseok Lee - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 25/08/2017
 *
 */

let logger = AppZet.logger('BallController');

module.exports = {
  // public (서버)
  count: count,
  find: find,
  findOne: findOne,

  // 사용자 주인
  create: create,
  update: update,
  remove: remove,
};

function count(req, res) {
  req.buildQuery()
    .then((params) => {
      return Ball.count(params.query);
    })
    .then(function (count) {
      return res.ok({count: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function find(req, res) {
  let params, promise;

  req.buildQuery()
    .then(( _params) => {
      params = _params;

      let queryPromise = Ball.find(params.query);

      // Limit
      if (!params.limit || params.limit > 50)
        params.limit = 50;
      params.limit++;
      queryPromise = queryPromise.limit(params.limit);

      // Skip
      if (params.skip)
        queryPromise = queryPromise.skip(params.skip);

      // Sort
      if (params.sort)
        queryPromise = queryPromise.sort(params.sort);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      // Count
      let countPromise = Ball.count(params.query);

      return [queryPromise, countPromise];
    })
    .spread(function (balls, count) {

      // See if there's more
      let more = (balls[params.limit - 1]) ? true : false;
      // Remove item over 20 (only for check purpose)
      if (more) balls.splice(params.limit - 1, 1);

      return res.ok({balls: balls, more: more, total: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function findOne(req, res) {
  req.buildQuery()
    .then((params) => {
      let queryPromise = Ball.findOne(params.query);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      return queryPromise;
    })
    .then(function (ball) {
      if (!ball) throw new Error("BallNotFound");
      res.ok({ball: ball});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'BallNotFound')
        return res.badRequest();

      if (err.message === 'NoPermission')
        return res.forbidden();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}


function create(req, res) {
  let ball;
  req.getParams(["amount", "note"])
    .then((_ball) => {
      ball = _ball;

      ball.createdBy = req.user._id;
      ball.updatedBy = req.user._id;
      ball.owner = req.user._id;
      ball.amount = req.body.amount;
      ball.note = req.body.note;

      return Ball.create(ball);
    })
    .then(function (ball) {
      res.ok({ball: ball});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}

function update(req, res) {
  req.getParams("_id")
    .then((query) => {
      return Ball.findOne(query);
    })
    .then(ball => {
      if (!ball) throw new Error("BallNotFound");

      ball.lastUpdatedAt = new Date();
      ball.updatedBy = req.user._id;

      ball.amount = req.body.amount;
      ball.note = req.body.note;

      return ball.save();
    })
    .then((ball) => {
      return res.ok({ball: ball});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'BallNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}


function remove(req, res) {
  req.getParams("_id")
    .then((query) => {
      return Ball.findOne(query);
    })
    .then(ball => {
      if (!ball) throw new Error("BallNotFound");
      ball.isDeleted = true;
      return ball.save();
    })
    .then((ball) => {
      return res.ok({ball: ball});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'BallNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}
