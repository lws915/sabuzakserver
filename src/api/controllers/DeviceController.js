'use strict';
/**
 * Created by JongIn Koo on 10/11/2016
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by JongIn Koo <jiku90@naver.com>, 10/11/2016
 *
 */

let logger = AppZet.logger('DeviceController');

module.exports = {
  // 앱 사용자 (주인)
  mine: mine,
  pushOn: pushOn,
  pushOff: pushOff,
  update: update,

  // 앱젯 사용자 (서버)
  count: count,
  find: find,
  findOne: findOne,

  // 앱젯 관리자
  pushToDevices: pushToDevices,
  // pushToTopicSomething: pushToTopicSomething,
  remove: remove,
};

function mine(req, res) {
  req.user.populate('devices').execPopulate()
    .then(user => {
      res.ok({devices: user.devices});
    });
}

function pushOn() {
  req.getParams(["deviceId"])
    .then((query) => {
      return Device.findOne(query);
    })
    .then(function (device) {
      if (!device) throw new Error("DeviceNotFound");

      device.lastUpdatedAt = new Date();
      device.updatedBy = req.user._id;

      device.active = true;

      return device.save();
    })
    .then(function (device) {
      return res.ok({device: device});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'DeviceNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function pushOff() {
  req.getParams(["deviceId"])
    .then((query) => {
      return Device.findOne(query);
    })
    .then(function (device) {
      if (!device) throw new Error("DeviceNotFound");

      device.lastUpdatedAt = new Date();
      device.updatedBy = req.user._id;

      device.active = false;

      return device.save();
    })
    .then(function (device) {
      return res.ok({device: device});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'DeviceNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function update(req, res) {
  req.getParams(["deviceId"])
    .then((query) => {
      return Device.findOne(query);
    })
    .then(function (device) {
      if (!device) throw new Error("DeviceNotFound");

      device.lastUpdatedAt = new Date();
      device.updatedBy = req.user._id;

      if (req.body.pushId)
        device.pushId = req.body.pushId;

      return device.save();
    })
    .then(function (device) {
      return res.ok({device: device});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'DeviceNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function count(req, res) {
  req.buildQuery()
    .then((params) => {
      return Device.count(params.query);
    })
    .then(function (count) {
      return res.ok({count: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}


function find(req, res) {
  let params;

  req.buildQuery()
    .then((_params) => {
      params = _params;

      // Find
      let queryPromise = Device.find(params.query);

      // Limit
      if (!params.limit || params.limit > 50)
        params.limit = 50;
      params.limit++;
      queryPromise = queryPromise.limit(params.limit);

      // Skip
      if (params.skip)
        queryPromise = queryPromise.skip(params.skip);

      // Sort
      if (params.sort)
        queryPromise = queryPromise.sort(params.sort);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      // Count
      let countPromise = Device.count(params.query);

      return Promise.all([queryPromise, countPromise])
    })
    .spread(function (devices, count) {

      // See if there's more
      let more = (devices[params.limit - 1]) ? true : false;
      // Remove item over 20 (only for check purpose)
      if (more) devices.splice(params.limit - 1, 1);

      return res.ok({devices: devices, more: more, total: count});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      res.internalServer();
    });
}


function findOne(req, res) {
  req.buildQuery()
    .then((params) => {

      let queryPromise = Device.findOne(params.query);

      // Populate
      if (params.populate) {
        if (Array.isArray(params.populate))
          _.forEach(params.populate, function (populate) {
            queryPromise = queryPromise.populate(populate);
          });
        else
          queryPromise = queryPromise.populate(params.populate);
      }

      return queryPromise;
    })
    .then(function (device) {
      if (!device) throw new Error("DeviceNotFound");
      res.ok({device: device});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'DeviceNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}


// function pushToTopicSomething(req, res) {
//   let title = req.param("title");
//   let message = req.param("message");
//
//   //TODO : 나중에 TOPIC LIST 정의 되면 이름 바꿔서 써야함.
//   let topic = 'SOMETHING';
//
//   if (!title || !message) return res.badRequest();
//
//   PushService.sendToTopic(topic, title, message);
//   return res.ok({message: "Message sent."});
// }

function pushToDevices(req, res) {
  let clientId = req.param("clientId");
  let title = req.param("title");
  let message = req.param("message");

  if (!clientId || !title || !message) return res.badRequest();

  let params = QueryService.buildQuery(req);
  params.query.server = parseInt(req.server);
  params.query.active = true;

  Promise.all([
    Client.findOne({_id: clientId}),
    Device.find(params.query)
  ])
    .spread(function (client, devices) {
      if (!client) throw new Error("ClientNotFound");

      PushService.sendToDevices(client, devices, title, message);
      return res.ok({message: "Message sent."});
    })
    .catch(function (err) {
      logger.log('error', err);
      return res.internalServer();
    });
}

function remove(req, res) {
  req.getParams("_id")
    .then((query) => {
      return Device.findOne(query)
    })
    .then(device => {
      if (!device) throw new Error("DeviceNotFound");

      device.isDeleted = true;

      return device.save();
    })
    .then(function (device) {
      return res.ok({device: device});
    })
    .catch(function (err) {
      if (err.message === 'InvalidParameter'
        || err.message === 'DeviceNotFound')
        return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

