'use strict';

/**
 * Created by Andy on 7/7/2015
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/7/2015
 *
 */

let logger = AppZet.logger('AuthController');
let regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
let regNumExp = /^\d+$/;
let Promise = require('bluebird');

module.exports = {
  // Public
  checkEmail: checkEmail,
  register: register,
  login: login,
  socialLogin: socialLogin,
  confirmActivation: confirmActivation,
  forgotPasswordStart: forgotPasswordStart,
  forgotPasswordCheck: forgotPasswordCheck,
  forgotPasswordComplete: forgotPasswordComplete,

  forgotPasswordStartSMS: forgotPasswordStartSMS,
  forgotPasswordCheckSMS: forgotPasswordCheckSMS,
  forgotPasswordCompleteSMS: forgotPasswordCompleteSMS,

  support: support,
  reportError: reportError,
  logout: logout,

  checkMobileNumber: checkMobileNumber,
  completeMobileNumber: completeMobileNumber,

  // 사용자
  changePassword: changePassword,
  getMyUserInfo: getMyUserInfo,
  updateMyInfo: updateMyInfo,
  changeSafeNumber: changeSafeNumber,
  sendActivationEmail: sendActivationEmail,

};

function checkEmail(req, res) {
  let serverId = req.server;
  let email = req.param('email');

  if (!email || !regExp.test(email)) return res.ok({isAvailable: false});

  User.findOne({server: serverId, email: email})
    .then(function (user) {
      if (user) {
        return res.ok({isAvailable: false});
      } else {
        return res.ok({isAvailable: true});
      }
    })
    .catch(function (err) {
      logger.log('error', err);
      return res.internalServer();
    });
}

function register(req, res) {
  PassportService.protocols.local.register(req.body)
    .then((user) => {
      return res.ok({user: user});
    })
    .catch(err => {
      if (err.message === "NoPassword"
        || err.message === "InvalidPassword") return res.badRequest();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function login(req, res) {
  PassportService.authenticate('local', handler)(req, res, req.next);

  function handler(err, user, message) {
    if (err) {
      logger.log('error', err);
      return res.internalServer();
    }

    if (!user) return res.unauthorized();

    req.logIn = Promise.promisify(req.logIn);
    req.logIn(user)
      .then(function () {
        return SafeNumberService.create(user.mobile);
      })
      .then(function (safeNumber) {
        // Get populated fields
        user.safeNumber = safeNumber;
        user.save();
        let userData = user.toJSON();

        let token = AuthService.getToken({_id: userData._id});
        req.session.authenticated = true;
        return res.ok({user: userData, token: token});
      })
      .catch(function (err) {
        logger.log('error', err);
        return res.internalServer();
      });
  }
}

function socialLogin(req, res){
  PassportService.authenticate('oauth2', handler)(req, res, req.next);
  
    function handler(err, user, message) {
      if (err) {
        logger.log('error', err);
        return res.internalServer();
      }
  
      if (!user) return res.unauthorized();
  
      req.logIn = Promise.promisify(req.logIn);
      req.logIn(user)
        .then(function () {
          return SafeNumberService.create(user.mobile);
        })
        .then(function (safeNumber) {
          // Get populated fields
          user.safeNumber = safeNumber;
          user.save();
          let userData = user.toJSON();
  
          let token = AuthService.getToken({_id: userData._id});
          req.session.authenticated = true;
          return res.ok({user: userData, token: token});
        })
        .catch(function (err) {
          logger.log('error', err);
          return res.internalServer();
        });
    }
}

function confirmActivation(req, res) {
  AuthService.activationComplete(req)
    .then((data) => {
      return res.ok({message: 'done'});
    })
    .catch((err) => {
      if (err.message === 'UserNotFound'
        || err.message === "BadRequest") return res.badRequest();                     // 400
      if (err.message === 'AlreadyActivated') return res.conflict();                  // 409
      if (err.message === 'ActivationNotRequested') return res.preconditionFailed();  // 412
      if (err.message === 'InvalidCode') return res.unprocessableEntity();                   // 422
      if (err.message === 'ActivationExpired') return res.unprocessableEntity();      // 422

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function forgotPasswordStart(req, res) {
  AuthService.passwordResetStart(req)
    .then(function (data) {
      return res.ok({message: "Password reset email sent."});
    })
    .catch(function (err) {
      if (err.message === 'UserNotFound'
        || err.message === "BadRequest") return res.badRequest();                 //400

      // 하루 요청량 초과
      if (err.message === 'ExceedDailyRequestLimit') return res.paymentRequired();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function forgotPasswordCheck(req, res) {
  AuthService.passwordResetCheck(req)
    .then(function () {
      return res.ok({message: 'done'});
    })
    .catch(function (err) {
      if (err.message === 'UserNotFound'
        || err.message === "BadRequest") return res.badRequest();

      if (err.message === 'PasswordResetNotRequested') return res.preconditionFailed();
      if (err.message === 'InvalidCode') return res.unprocessableEntity();
      if (err.message === 'PasswordResetExpired') return res.unprocessableEntity();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function forgotPasswordComplete(req, res) {
  AuthService.passwordResetComplete(req)
    .then(function () {
      return res.ok({message: "Password reset complete"});
    })
    .catch(function (err) {
      if (err.message === 'UserNotFound'
        || err.message === "BadRequest") return res.badRequest();

      if (err.message === 'PasswordResetNotRequested') return res.preconditionFailed();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}


function forgotPasswordStartSMS(req, res) {
  AuthService.passwordResetStartSMS(req)
    .then(function (data) {
      return res.ok({message: "Password reset email sent."});
    })
    .catch(function (err) {
      if (err.message === 'UserNotFound'
        || err.message === "BadRequest") return res.badRequest();                 //400

      // 하루 요청량 초과
      if (err.message === 'ExceedDailyRequestLimit') return res.paymentRequired();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function forgotPasswordCheckSMS(req, res) {
  AuthService.passwordResetCheckSMS(req)
    .then(function () {
      return res.ok({message: 'done'});
    })
    .catch(function (err) {
      if (err.message === 'UserNotFound'
        || err.message === "BadRequest") return res.badRequest();

      if (err.message === 'PasswordResetNotRequested') return res.preconditionFailed();
      if (err.message === 'InvalidCode') return res.unprocessableEntity();
      if (err.message === 'PasswordResetExpired') return res.unprocessableEntity();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function forgotPasswordCompleteSMS(req, res) {
  AuthService.passwordResetCompleteSMS(req)
    .then(function () {
      return res.ok({message: "Password reset complete"});
    })
    .catch(function (err) {
      if (err.message === 'UserNotFound'
        || err.message === "BadRequest") return res.badRequest();

      if (err.message === 'PasswordResetNotRequested') return res.preconditionFailed();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function support(req, res) {
  let data = {}, files = [];

  new Promise((resolve, reject) => {
    let form = new Multiparty.Form({uploadDir: 'uploads'});

    form.on('field', function (name, value) {
      data[name] = value;
    });

    // all uploads are completed
    form.on('file', function (name, file) {
      files.push(file);
    });

    // all uploads are completed
    form.on('close', function () {
      resolve(files);
    });

    form.on('error', function (err) {
      reject(err);
    });
    form.parse(req);
  })
    .then(() => {

      if (!data.email
        || !regExp.test(data.email)
        || !data.name
        || !data.category
        || !data.content) throw new Error("BadRequest");

      let attachments = [];
      _.forEach(files, function (file, index) {

        if (file.size > 15 * 1000 * 1000) throw new Error("ExceedFileSize");

        attachments.push({
          filename: "attachment" + (index + 1) + "_" + file.originalFilename,
          path: file.path
        });
      });

      return MailService.sendEmail(
        "support",
        "kr",
        {
          email: data.email,
          name: data.name,
          category: data.category,
          content: data.content,
          date: Moment().format("YYYY년 MM월 DD일 HH시 mm분")
        },
        {email: data.email, name: data.name},
        ["support@appzet.com"],
        attachments
      );
    })
    .then(() => {
      res.ok();
    })
    .catch((err) => {
      if (err.message === "BadRequest")
        return res.badRequest();

      if (err.message === "ExceedFileSize")
        return res.payloadTooLarge();

      logger.error(err);
      return res.internalServer();
    })
    .finally(() => _.forEach(files, (file) => {
      if (FileSystem.existsSync(file.path))
        FileSystem.unlink(file.path)
    }));
}


function logout(req, res) {
  req.session.destroy(() => {
    res.ok({});
  });
}

function checkMobileNumber(req, res) {

  let mobile, mobileActiveNumber, message;

  if (req.session.mobileActiveNumber
    && req.session.mobileActiveRequested
    && Moment(req.session.mobileActiveRequested) > Moment().minutes(Moment().minute() - 1)) {
    return res.unprocessableEntity();
  }

  mobile = req.param('mobile');

  if (!mobile || !regNumExp.test(mobile))
    return res.badRequest();

  mobile = (mobile + "").replace("-", "");

  User.findOne({mobile: mobile})
    .then((user) => {
      if (user)throw new Error("UserAlreadyExist");

      mobileActiveNumber = LPAD(randomIntInc(0, 999999) + "", '0', 6);

      message = "인증번호는 [" + mobileActiveNumber + "]입니다. 바톤";

      return SmsService.send(message, mobile);
    })
    .then(() => {
      req.session.mobileActiveNumber = mobileActiveNumber;
      req.session.mobileActiveRequested = new Date();
      res.ok({});
    })
    .catch((err) => {
      if (err.message === "BadRequest") return res.badRequest();

      if (err.message === "UserAlreadyExist") return res.gone();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function completeMobileNumber(req, res) {
  let code = req.param('code');

  if (!code)
    return res.badRequest();

  if (req.session.mobileActiveNumber == code) {
    req.session.mobileActiveNumber = null;
    res.ok({});
  } else {
    res.preconditionFailed();
  }
}


function changePassword(req, res) {
  AuthService.changePassword(req)
    .then(function () {
      return res.ok({message: "Password change complete"});
    })
    .catch(function (err) {
      if (err.message === 'PassportNotFound'
        || err.message === "BadRequest") return res.badRequest();

      if (err.message === 'InvalidPassword') return res.unprocessableEntity();

      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function getMyUserInfo(req, res) {
  let params = QueryService.buildQuery(req);

  params.query._id = req.user._id;

  let queryPromise = User.findOne(params.query);

  // Populate
  if (params.populate) {
    if (Array.isArray(params.populate))
      _.forEach(params.populate, function (populate) {
        queryPromise = queryPromise.populate(populate);
      });
    else
      queryPromise = queryPromise.populate(params.populate);
  }

  queryPromise
    .then(function (user) {
      return res.ok({user: user});
    })
    .catch(function (err) {
      logger.log('error', err);
      return res.internalServer();
    });
}

function updateMyInfo(req, res) {
  req.user.lastUpdatedAt = new Date();
  req.user.updatedBy = req.user._id;

  if (req.body.name)
    req.user.name = req.body.name;

  if (req.body.mobile)
    req.user.mobile = req.body.mobile;

  if (req.body.birth)
    req.user.birth = req.body.birth;

  if (req.body.identifier && req.user.identifier != req.body.identifier) {
    req.user.identifier = req.body.identifier;
    req.user.emailConfirmed = false;
  }

  req.user.save()
    .then(function (user) {
      return res.ok({user: user});
    })
    .catch(function (err) {
      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function changeSafeNumber(req, res) {

  if (!req.user.mobile)
    return res.conflict();

  SafeNumberService.create(req.user.mobile)
    .then(function (safeNumber) {
      req.user.safeNumber = safeNumber;

      req.user.lastUpdatedAt = new Date();
      req.user.updatedBy = req.user._id;

      return req.user.save();
    })
    .then(function (user) {
      return res.ok({safeNumber: req.user.safeNumber});
    })
    .catch(function (err) {
      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function sendActivationEmail(req, res) {
  AuthService.activationStart(req)
    .then((data) => {
      res.ok({message: 'Activation email sent.'})
    })
    .catch((err) => {
      if (err.message === 'UserNotFound'
        || err.message === "BadRequest") return res.badRequest();

      // 이미 이메일 인증 완료
      if (err.message === 'AlreadyActivated') return res.conflict();

      // 하루 요청량 초과
      if (err.message === 'ExceedDailyRequestLimit') return res.paymentRequired();


      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}


function reportError(req, res) {
  let errorReport = {
    ipAddress: req.headers['x-forwarded-for'] || (req.connection && req.connection.remoteAddress),
    logs: req.body.logs,
    owner: req.user && req.user._id ? req.user._id : null
  };

  ErrorReport.create(errorReport)
    .then(() => {
      return res.ok();
    })
    .catch(function (err) {
      if (err.name === 'MongoError' || err instanceof Mongoose.Error)
        return res.mongooseError(err);

      logger.log('error', err);
      return res.internalServer();
    });
}

function randomIntInc(low, high) {
  return Math.floor(Math.random() * (high - low + 1) + low);
}

function LPAD(s, c, n) {
  if (!s || !c || s.length >= n) {
    return s;
  }
  let max = (n - s.length) / c.length;
  for (let i = 0; i < max; i++) {
    s = c + s;
  }
  return s;
}