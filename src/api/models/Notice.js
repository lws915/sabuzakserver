'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

let logger = AppZet.logger('Notice');

module.exports = {
  schema: {
    title: {type: String},
    content: {type: String},
    
    isDeleted: {type: Boolean, index: true, default: false},
    // 수정순으로 가져올때 사용됨
    lastUpdatedAt: {
      type: Date, default: function () {
        return new Date();
      }
    },

    //Associations
  },
  beforeUpdate: function (next) {
    next();
  }
};