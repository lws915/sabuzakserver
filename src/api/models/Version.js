'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

module.exports = {
  disableGlobalSchema: true,
  schema: {

    requiredVersion: {type: String, required: true},
    optionalVersion: {type: String, required: true},
    buildNumber: {type: String, required: true},
    platform: {type: String, enum: ['iOS', 'Android'], required: true},
    active: {type: Boolean, default: false},

    isDeleted: {type: Boolean, index: true, default: false},

    // 수정순으로 가져올때 사용됨
    lastUpdatedAt: {
      type: Date, default: function () {
        return new Date();
      }
    },

    client: {type: Number, ref: 'Client', required: true},

    owner: {type: Number, ref: 'User'},
    createdBy: {type: Number, ref: 'User'},
    updatedBy: {type: Number, ref: 'User'}
  }
};