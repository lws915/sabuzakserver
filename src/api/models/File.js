'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

module.exports = {
  schema: {
    tags: [{type: String, ref: 'Tag'}],
    url: {type: String},
    note: {type: String},
    secure_url: {type: String},
    index: {type: Number},
    views: {type: Number, default: 0},
    fileType: {type: String},
    fileName: {type: String},
    fileSize: {type: Number},

    // track: {type: String},

    isDeleted: {type: Boolean, index: true, default: false},
    // 수정순으로 가져올때 사용됨
    lastUpdatedAt: {
      type: Date, default: function () {
        return new Date();
      }
    },

    //Associations
  },
  virtuals: {
    announcementRef: {ref: 'Coupon', localField: '_id', foreignField: 'photos', justOne: true},
    couponRef: {ref: 'Coupon', localField: '_id', foreignField: 'photos', justOne: true},
    postRef: {ref: 'Post', localField: '_id', foreignField: 'photos', justOne: true},
    eventRef: {ref: 'Event', localField: '_id', foreignField: 'photos', justOne: true},
    eventAdmissionRef: {ref: 'EventAdmission', localField: '_id', foreignField: 'photos', justOne: true},
    galleryRef: {ref: 'Gallery', localField: '_id', foreignField: 'photos', justOne: true},
    stampCardRef: {ref: 'StampCard', localField: '_id', foreignField: 'photos', justOne: true},
    userRef: {ref: 'User', localField: '_id', foreignField: 'photos', justOne: true},
    templateRef: {ref: 'Template', localField: '_id', foreignField: 'photos', justOne: true},
  },
  beforeUpdate: function (next) {
    next();
  }
};