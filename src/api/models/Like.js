'use strict';

/**
 * Created by sungwooKIM on 7/25/2016
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & sungwooKIM - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy sungwooKIM <ksw1652@gmail.com>, 7/25/2016
 *
 */

let logger = AppZet.logger('Like');

module.exports = {

  options: {
    timestamps: {createdAt: 'createdAt'}
  },
  schema: {

    //sabuzak association
    user: {type: Number, ref: 'User'},

    isDeleted: {type: Boolean, index: true, default: false},
    // 수정순으로 가져올때 사용됨
    lastUpdatedAt: {
      type: Date, default: function () {
        return new Date();
      }
    },

  },
  virtuals: {
  },

  cycles: {
    beforeSave: function (next) {
      next();
    },
    beforeUpdate: function (next) {
      next();
    },
    afterSave: [],
  }
};