'use strict';

/**
 * Created by wonseok Lee on 24/08/2017
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & wonseok Lee - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy wonseok Lee <whereiswonny@gmail.com>, 24/08/2017
 *
 */

module.exports = {
  schema: {

    // Properties
    title: {type: String, required: true},
    category: {type: String},
    contentHtml: {type: String},
    videoLink: {type: String},
    isSecret: {type: Boolean, default: false},

    commentsCount: {type: Number, default: 0},
    views: {type: Number, default: 0},
    likes: {type: Number, default: 0},

    isDeleted: {type: Boolean, index: true, default: false},
    // 수정순으로 가져올때 사용됨
    lastUpdatedAt: {
      type: Date, default: function () {
        return new Date();
      }
    },

    // Associations
    files: [{type: Number, ref: 'File'}],
    campus: {type: Number, ref: 'Campus'},
    owner: {type: Number, ref:'User'},
  },
  virtuals: {
    comments: {ref: 'Comment', localField: '_id', foreignField: 'post'},
    replies: {ref: 'Reply', localField: '_id', foreignField: 'post'}
  },
  beforeUpdate: function (next) {
    next();
  }
};