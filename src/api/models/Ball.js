'use strict';

/**
 * Created by Andy on 25/08/2017
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & wonseok Lee - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 25/08/2017
 *
 */

let logger = AppZet.logger('Ball');

module.exports = {
  schema: {
    amount: {type: Number},
    note: {type: String},
    
    isDeleted: {type: Boolean, index: true, default: false},
    // 수정순으로 가져올때 사용됨
    lastUpdatedAt: {
      type: Date, default: function () {
        return new Date();
      }
    },

    //Associations
    ballWallet: {type: Number, ref: 'BallWallet'},
  },
  beforeUpdate: function (next) {
    next();
  }
};