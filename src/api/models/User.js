'use strict';

/**
 * Created by sungwooKIM on 7/25/2016
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & sungwooKIM - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy sungwooKIM <ksw1652@gmail.com>, 7/25/2016
 *
 */

let logger = AppZet.logger('User');

module.exports = {
  // indexes: [
  //   {field: {identifier: 1}, option: {unique: true}},
  //   {field: {mobile: 1}, option: {unique: true}}
  // ],
  options: {
    timestamps: {createdAt: 'createdAt'},
    toObject: {
      virtuals: true,
      transform: function (doc, ret) {
        delete ret.emailConfirmed;
        delete ret.activationCode;
        delete ret.activationRequestTime;

        delete ret.passwordResetCode;
        delete ret.passwordResetRequestTime;
      }
    },
    toJSON: {
      virtuals: true,
      transform: function (doc, ret) {
        delete ret.activationCode;
        delete ret.activationRequestTime;

        delete ret.passwordResetCode;
        delete ret.passwordResetRequestTime;

        delete ret.transactionState;
        delete ret.transactionLastModified;


        _.forEach(doc.$$populatedVirtuals, (val, key) => {
          ret[key] = val;
        });
      }
    }
  },
  schema: {
    // 사용자 정보
    // identifier: {                                       //아이디
    //   type: String, required: true,
    //   validate: {
    //     validator: function (v) {
    //       return v !== 'null';
    //     },
    //     message: '{VALUE} is should not be null'
    //   }
    // },

    //sabuzak properties
    email: {type: String, required: true},              // 이메일
    nickName: {type: String, required: true},           // 닉네임
    gender: {type: String},                             // 성별
    introduce: {type: String},                          // 소개글
    age: {type: Number, required: true},                // 나이 : 18 - 45
    location: {type: String, required: true},           // 지역 : 서울, 경기, 인천, 대전, 충북, 충남, 강원, 부산, 경북, 경남, 대구, 울산, 광주, 전북, 전남, 제주
    job: {type: String},                                // 직업
    bodyType: {type: String, required: true},           // 체형 : 여성 - 마른, 슬림탄탄, 보통, 글래머, 포근 / 남성 - 마른, 슬림탄탄, 보통, 건장한, 포근
    tall: {type: Number, required: true},               // 키 : 150 - 190
    faith: {type: String},                              // 종교 : 무교, 기독교, 천주교, 불교, 기타
    school: {type: String},                             // 학교
    drinking: {type: String},                           // 음주 : 마시지 않음, 가끔 즐김, 술자리를 즐기는 편
    smoking: {type: String},                            // 흡연 : 흡연, 비흡연
    interest: {type: String},                           // 관심사
    rank: {type:String, default: '심사중'},              // 등급
    likeCount: {type: Number, default: 0},              // 등급을 매기기 위한 점수, 업 할때마다 1점씩 오른다.
    noticeChk: {type:Object},                           // 공지사항 확인여부
    totalBall: {type:Number, default: 0},               // 보유중인 ball
    upCount: {type:Number, default: 20},                  // up 추천가능 횟수

    isAlert: {type: Boolean, default: true},            // 환경설정 - 알림받기
    isPush: {type: Boolean, default: true},             // 환경설정 - 푸시알림
    isTodayMatch: {type: Boolean, default: true},       // 환경설정 - 오늘의 카드 받기


    // BallWallet < totalBalance BallWallet.owner
    // BallTransaction < Ball history
    // BallTransaction.ballWallet
    // BallTransaction.owner
    //
    // RankReport - it is a weekly report for rank
    // Rank - 
    // 


    //sabuzak association
    // balls: [{type: Number, ref: 'Ball'}],   
    // upMe: [{type: Number, ref: 'User'}],
    // upU: [{type: Number, ref: 'User'}],
    // down: [{type: Number, ref: 'User'}],
    todayRecommend: [{type: Number, ref: 'User'}],

    // eachU: [{type: Number, ref: 'User'}],
    passed: [{type: Number, ref: 'User'}],
    opened: [{type: Number, ref: 'User'}],

    // Auth properties
    // Email Activation
    emailConfirmed: {type: Boolean, default: false},    // 이메일 인증여부
    activationCode: {type: String},                     // 이메일 인증코드
    activationCounter: {type: Number, default: 0},      // 이메일 인증 요청 횟수 max = 5
    activationRequestTime: {type: Number, default: 0},  // 마지막 이메일 인증 요청 시간

    passwordResetCode: {type: String},                    // 비밀번호 찾기 코드
    passwordResetCounter: {type: Number, default: 0},     // 비밀번호 찾기 요청 횟수 max = 5
    passwordResetRequestTime: {type: Number, default: 0}, // 마지막 비밀번호 찾기 요청 시간

    accesscount: {type: Number, default: 0},

    // Application passcode
    passcodeTryCount: {type: Number, default: 0},
    passcodeTryRequestedAt: {type: Date, default: Date.now},
    passcodeTryLimit: {type: Number, default: 3 * 60},//second

    isDeleted: {type: Boolean, index: true, default: false},
    // 수정순으로 가져올때 사용됨
    lastUpdatedAt: {
      type: Date, default: function () {
        return new Date();
      }
    },

    role: {type: String, ref: 'Role'},
  },
  virtuals: {
    devices: {ref: 'Device', localField: '_id', foreignField: 'owner'},
    passports: {ref: 'Passport', localField: '_id', foreignField: 'owner'},
    requestLogs: {ref: 'RequestLog', localField: '_id', foreignField: 'owner'},
  },

  cycles: {
    beforeSave: function (next) {
      next();
    },
    beforeUpdate: function (next) {
      next();
    },
    afterSave: [],
  }
};