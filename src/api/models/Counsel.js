'use strict';

/**
 * Created by sungwooKIM on 7/25/2016
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & sungwooKIM - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy sungwooKIM <ksw1652@gmail.com>, 7/25/2016
 *
 */

module.exports = {
  schema: {

    // Properties
    content: {type:String, required: true},

    isDeleted: {type: Boolean, index: true, default: false},
    // 수정순으로 가져올때 사용됨
    lastUpdatedAt: {
      type: Date, default: function () {
        return new Date();
      }
    },

    // Associations
  },
  virtuals: {},
  beforeUpdate: function (next) {
    next();
  }
};