'use strict';

/**
 * Created by Andy on 7/6/2015
 * As part of Appzet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Andy Yoon Yong Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <andy.shin@applicat.co.kr>, 7/6/2015
 *
 */

module.exports = {
  autoPK: false,
  autoCreatedBy: false,
  autoUpdatedAt: false,
  disableGlobalSchema: true,
  schema: {
    ipAddress: {type: String},
    method: {type: String},
    url: {type: String},
    body: {type: Object},
    query: {type: Object},
    params: {type: Object},
    type: {type: String},
    duration: {type: Schema.Types.Double}, // in milliseconds

    statusCode: {type: Number},

    server: {type: Number, ref: 'Server', index: true},
    owner: {type: Number, ref: 'User'},

    controller: {type: String},
    api: {type: String},

    isDeleted: {type: Boolean, index: true, default: false},
    // 수정순으로 가져올때 사용됨
    lastUpdatedAt: {
      type: Date, default: function () {
        return new Date();
      }
    },
  }
};